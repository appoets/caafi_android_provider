package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.common.AddDoctorResponse;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;
import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddDoctorModel extends BaseModel<BaseResponse> {

    public AddDoctorModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);

    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void postAdd(HashMap<String, RequestBody> params, MultipartBody.Part filePart) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).addDoctor(params,filePart));
    }

}
