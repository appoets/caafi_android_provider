package com.caafisom.android.provider.view.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CalendarView;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.response.ScheduledListResponse;
import com.caafisom.android.provider.presenter.ScheduledListPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IScheduledListPresenter;
import com.caafisom.android.provider.view.adapter.ScheduledListAdapter;
import com.caafisom.android.provider.view.adapter.listener.IScheduledListListener;
import com.caafisom.android.provider.view.iview.IScheduledListView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class ScheduledListActivity extends BaseActivity<IScheduledListPresenter> implements IScheduledListView {

    @BindView(R.id.ibBack)
    ImageView ibBack;
    @BindView(R.id.rcvScheduledList)
    RecyclerView rcvScheduledList;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.noData)
    TextView noData;

    private String selectedDate;
    //private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd", Locale.getDefault());
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private ScheduledListAdapter adapter;
    private List<ScheduledListResponse> wholeresponse = new ArrayList<>();
    private List<ScheduledListResponse> selectedDateList;
    private String tempDate;
    private IScheduledListListener iScheduledListListener;

    @Override
    int attachLayout() {
        return R.layout.activity_schedule;
    }

    @Override
    IScheduledListPresenter initialize() {
        selectedDate = dateFormat.format(Calendar.getInstance().getTime());
        return new ScheduledListPresenter(this);
    }



/*    @Override
    public void setAdapter(ScheduledListAdapter adapter, IScheduledListListener iScheduleListener) {
        if (adapter.getItemCount() > 0) {
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
            rcvScheduledList.setAdapter(adapter);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }*/

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void setAdapter(List<ScheduledListResponse> list, IScheduledListListener iScheduledListListener) {
        if (list.size() > 0) {
            wholeresponse = list;
            this.iScheduledListListener = iScheduledListListener;
            searchForSelectedDate();
            if (selectedDateList.size() > 0) {
                noData.setVisibility(View.GONE);
                rcvScheduledList.setVisibility(View.VISIBLE);
                adapter = new ScheduledListAdapter(selectedDateList, iScheduledListListener);
                rcvScheduledList.setAdapter(adapter);
            } else {
                rcvScheduledList.setVisibility(View.GONE);
                noData.setVisibility(View.VISIBLE);
            }
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void moveToDetail(ScheduledListResponse data) {
        if (data.getStatus().equalsIgnoreCase("CANCELLED")) {
            showToast("Request was Cancelled");
        } else {
            startActivity(new Intent(ScheduledListActivity.this, ScheduleDetailActivity.class)
                    .putExtra("data" , data).putExtra("from","list"));
                    /*.putExtra("id", data.getId())
                    .putExtra("name", data.getUser().getFirstName() + " " + data.getUser().getLastName())
                    .putExtra("image", data.getUser().getPicture())
                    .putExtra("date", data.getScheduleAt() != null && !data.getScheduleAt().equalsIgnoreCase(null) ? data.getScheduleAt().split(" ")[0] : " - ")
                    .putExtra("time", data.getScheduleAt() != null && !data.getScheduleAt().equalsIgnoreCase(null) ? data.getScheduleAt().split(" ")[1] : " - ")
                    .putExtra("status", data.getStatus()));*/
        }
    }

    @Override
    public void initSetUp() {
        rcvScheduledList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvScheduledList.setItemAnimator(new DefaultItemAnimator());


    }

    @Override
    protected void onResume() {
        super.onResume();

        calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    //selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth;
                    Date enddateObj = dateFormat.parse(year + "-" + (month + 1) + "-" + dayOfMonth);
                    selectedDate = dateFormat.format(enddateObj);
                    //selectedDate = dateFormat.format(selectedDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (wholeresponse.size() > 0)
                    getSelectedDateList();
            }
        });


        new ScheduledListPresenter(this);
    }

    private void getSelectedDateList() {
        searchForSelectedDate();
        if (selectedDateList.size() > 0) {
            adapter = new ScheduledListAdapter(selectedDateList, iScheduledListListener);
            rcvScheduledList.setAdapter(adapter);
            noData.setVisibility(View.GONE);
            rcvScheduledList.setVisibility(View.VISIBLE);
        } else {
            rcvScheduledList.setVisibility(View.GONE);
            noData.setVisibility(View.VISIBLE);
        }
    }

    private void searchForSelectedDate() {
        tempDate = "";
        selectedDateList = new ArrayList<>();
        try {
            for (int i = 0; i < wholeresponse.size(); i++) {
                if (wholeresponse.get(i).getScheduleAt() != null && !wholeresponse.get(i).getScheduleAt().equalsIgnoreCase("null")) {
                    Date enddateObj = dateFormat.parse(wholeresponse.get(i).getScheduleAt().split(" ")[0]);
                    tempDate = dateFormat.format(enddateObj);
                    if (tempDate.equalsIgnoreCase(selectedDate)) {
                        selectedDateList.add(wholeresponse.get(i));
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
