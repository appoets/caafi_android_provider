package com.caafisom.android.provider.view.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.caafisom.android.provider.util.MultiSelectionSpinner;
import com.jaredrummler.materialspinner.MaterialSpinner;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.Constants;
import com.caafisom.android.provider.countrypicker.Country;
import com.caafisom.android.provider.countrypicker.CountryPicker;
import com.caafisom.android.provider.countrypicker.CountryPickerListener;
import com.caafisom.android.provider.model.dto.common.ServiceItem;
import com.caafisom.android.provider.model.dto.request.RegisterRequest;
import com.caafisom.android.provider.presenter.RegisterPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IRegisterPresenter;
import com.caafisom.android.provider.view.iview.IRegisterView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.RequestBody;

public class RegisterActivity extends BaseActivity<IRegisterPresenter> implements IRegisterView, MultiSelectionSpinner.OnMultipleItemsSelectedListener {

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etPassword)
    EditText etPassword;
    @BindView(R.id.etExperience)
    EditText etExperience;
    @BindView(R.id.etPrice)
    EditText etPrice;
    @BindView(R.id.etCountryCode)
    EditText etCountryCode;
    @BindView(R.id.etCity)
    EditText etCity;
    @BindView(R.id.spinnerSpecialities)
    MultiSelectionSpinner multiSelectionListSpinner;

    private CountryPicker mCountryPicker;
    private int selectedCityID = 0;
    private String selectedCityName;
    final int GET_CITY = 420;

    String specialities = "";

    ArrayList<String> specialist = new ArrayList<>();
    ArrayList<Integer> specialistID = new ArrayList<>();
    ArrayList<Integer> specialistnameID = new ArrayList<>();
    RegisterRequest registerRequest;

    @Override
    int attachLayout() {
        return R.layout.activity_register;
    }

    @Override
    IRegisterPresenter initialize() {
        mCountryPicker = CountryPicker.newInstance("Select Country");

        // You can limit the displayed countries
        ArrayList<Country> nc = new ArrayList<>();
        for (Country c : Country.getAllCountries()) {
            nc.add(c);
        }
        // and decide, in which order they will be displayed
        Collections.reverse(nc);
        mCountryPicker.setCountriesList(nc);
        setListener();
        registerRequest = new RegisterRequest();

        return new RegisterPresenter(this);

    }

    @OnClick({R.id.btnSignUp, R.id.tvSignIN, R.id.etCountryCode, R.id.etCity})
    public void OnViewClick(View view) {

        switch (view.getId()) {
            case R.id.btnSignUp:
                validateSignUp();
                break;

            case R.id.tvSignIN:
                iPresenter.goToLogin();
                break;
            case R.id.etCity:
                Intent intent = new Intent(getActivity(), CityListActivity.class);
                startActivityForResult(intent, GET_CITY);
                break;
            case R.id.etCountryCode:
                mCountryPicker.show(getSupportFragmentManager(), "COUNTRY_PICKER");
                break;
        }

    }

    private void setListener() {
        mCountryPicker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode,
                                        int flagDrawableResID) {
                etCountryCode.setText(dialCode);
                mCountryPicker.dismiss();
            }
        });

        getUserCountryInfo();
    }

    private void getUserCountryInfo() {
        Country country = Country.getCountryFromSIM(getApplicationContext());
        if (country != null) {
            etCountryCode.setText(country.getDialCode());
        }
    }

    private void validateSignUp() {
        String email = etEmail.getText().toString().trim();
        String first_name = etName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String mobile_number = etMobileNumber.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String experience = etExperience.getText().toString().trim();
        String price_min = etPrice.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            showSnackBar(getString(R.string.please_enter_email));
        } else if (!getCodeSnippet().isEmailValid(email)) {
            showSnackBar(getString(R.string.please_enter_valid_email));
        } else if (first_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_name));
        } else if (last_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_last_name));
        } else if (etCountryCode.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.please_enter_dial_code));
        } else if (mobile_number.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_mobile_number));
        } else if (password.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_password));
        } else if (password.length() < 6) {
            showSnackBar(getString(R.string.invalid_password));
        } else if (specialistID.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_specialities));
        } else if (experience.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_experience));
        } else if (price_min.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_price_min));
        } else if (etCity.getText().toString().isEmpty()) {
            showSnackBar(getString(R.string.please_enter_city));
        } else {
            registerRequest.setFirst_name(first_name);
            registerRequest.setLast_name(last_name);
            registerRequest.setEmail(email);
            registerRequest.setMobile(etCountryCode.getText().toString());
            registerRequest.setPassword(password);
            registerRequest.setPassword_confirmation(password);
            registerRequest.setCity_id(String.valueOf(selectedCityID));
            registerRequest.setDevice_id(Constants.WebConstants.DEVICE_ID);
            registerRequest.setDevice_token(Constants.WebConstants.DEVICE_TOKEN);
            registerRequest.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
            registerRequest.setExperience(experience);
            registerRequest.setProvider_price(price_min);
            registerRequest.setService_type("service_type[" + specialistID.size() + "]", specialistID);

          /*  HashMap<String,Object> map = new HashMap<>();
            map.put("first_name", first_name);
            map.put("last_name", last_name);
            map.put("email", email);
            map.put("mobile", etCountryCode.getText().toString() + mobile_number);
            map.put("password", password);
            map.put("password_confirmation", password);
            map.put("city_id", String.valueOf(selectedCityID));
            map.put("device_id", Constants.WebConstants.DEVICE_ID);
            map.put("device_type", Constants.WebConstants.DEVICE_TYPE);
            map.put("device_token", Constants.WebConstants.DEVICE_TOKEN);
            map.put("provider_price", price_min);
            map.put("experience", experience);
            for (int i = 0; i < specialistID.size(); i++) {
                map.put("service_type[" + i + "]", specialistID.get(i));
            }*/


            iPresenter.postRegister(registerRequest);
        }
    }


    @Override
    public void setUpData(List<ServiceItem> itemList) {

        /*Typeface font = etEmail.getTypeface();
        spinnerSpecialities.setTypeface(font);

        if (itemList.size()>0){
            specialities = itemList.get(0).getId()+"";
            spinnerSpecialities.setItems(itemList);
            spinnerSpecialities.setOnItemSelectedListener(new MaterialSpinner.OnItemSelectedListener() {
                @Override
                public void onItemSelected(MaterialSpinner view, int position, long id, Object item) {
                    specialities = itemList.get(position).getId()+"";
                }
            });
        }*/


        if (itemList.size() > 0) {
            //specialities = itemList.get(0).getId() + "";
            specialist.clear();
            specialistID.clear();
            specialistnameID.clear();

            for (int i = 0; i < itemList.size(); i++) {
                specialist.add(itemList.get(i).getName());
                specialistnameID.add(itemList.get(i).getId());
                multiSelectionListSpinner.setItems(specialist);
            }
//            multiSelectionListSpinner.setSelection(new int[]{1, 3});
            multiSelectionListSpinner.setListener(this);
        }

    }

    @Override
    public void goToLogin() {
        navigateTo(LoginActivity.class, false, new Bundle());
        finishAffinity();
    }

    @Override
    public void goToHome() {
        navigateTo(HomeActivity.class, true, new Bundle());
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK && requestCode == GET_CITY) {
            selectedCityID = data.getIntExtra("selectedCityID", 0);
            selectedCityName = data.getStringExtra("selectedCityName");
            //Toast.makeText(this, "selected City " + selectedCityID + " " + selectedCityName, Toast.LENGTH_SHORT).show();
            etCity.setText(selectedCityName);
        }
    }

    @Override
    public void selectedIndices(List<Integer> indices, MultiSelectionSpinner spinner) {
        for (int i = 0; i < indices.size(); i++) {
            specialistID.add(specialistnameID.get(i));

        }
        Log.e("Selected ID", "" + specialistID);

    }

    @Override
    public void selectedStrings(List<String> strings, MultiSelectionSpinner spinner) {
        switch (spinner.getId()) {
            case R.id.spinnerSpecialities:
                //Toast.makeText(this, "Selected : " + strings.toString(), Toast.LENGTH_LONG).show();
                break;

        }
    }
}
