package com.caafisom.android.provider.view.adapter.listener;

import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.dto.common.HistoryItem;

public interface IDoctorAvailabilityRecyclerListener extends BaseRecyclerListener<GetDoctorAvailablity> {
}
