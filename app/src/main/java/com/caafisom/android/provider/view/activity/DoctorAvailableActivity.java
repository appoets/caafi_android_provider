package com.caafisom.android.provider.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.presenter.DoctorAvailabilityPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.adapter.DoctorAvailabilityAdapter;
import com.caafisom.android.provider.view.iview.IDoctorAvailabilityView;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;

public class DoctorAvailableActivity extends BaseActivity<IDoctorAvailabilityPresenter> implements IDoctorAvailabilityView {

    private static final String TAG = "DoctorAvailableActivity";
    @BindView(R.id.rvDoctor_Availability)
    RecyclerView rvDoctorAvailability;
    @BindView(R.id.llNoHistory)
    LinearLayout llNoHistory;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.civProfile)
    ImageView civProfile;
    @BindView(R.id.tv_name)
    TextView tvname;
    @BindView(R.id.btn_createAvailability)
    Button btncreateAvailability;


    private String name, avatar;
    private Integer doctorID;

    @Override
    int attachLayout() {
        return R.layout.activity_doctor_available;
    }

    @Override
    IDoctorAvailabilityPresenter initialize() {

        Intent intent = getIntent();
        if (intent != null) {
            name = intent.getStringExtra("doctor_name");
            doctorID = intent.getIntExtra("doctor_id", 0);
            avatar = intent.getStringExtra("doctor_image");

            tvname.setText(name);
            Glide.with(DoctorAvailableActivity.this)
                    .load(avatar)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user).dontAnimate())
                    .into(civProfile);
        }

        return new DoctorAvailabilityPresenter(this);
    }

    @OnClick({R.id.ibBack,R.id.btn_createAvailability})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.btn_createAvailability:
                Intent intent = new Intent(DoctorAvailableActivity.this, CreateDoctorAvailabilityActivity.class);
                intent.putExtra("doctor_id",doctorID);
                intent.putExtra("doctor_name",name);
                intent.putExtra("doctor_image",avatar);
                startActivity(intent);
                finish();
                break;

        }
    }


    @Override
    public void setAdapter(DoctorAvailabilityAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            llNoHistory.setVisibility(View.GONE);
            rvDoctorAvailability.setVisibility(View.VISIBLE);
            rvDoctorAvailability.setAdapter(adapter);
        } else {
            rvDoctorAvailability.setVisibility(View.GONE);
            llNoHistory.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void initSetUp() {
        iPresenter.getDoctorAvailability(doctorID);

        rvDoctorAvailability.setLayoutManager(new LinearLayoutManager(getActivity()));
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void removeDoctorAvaailability(Integer Id) {
        Log.d("ID:", "" + Id);
        iPresenter.getDoctorAvailability(doctorID);
        iPresenter.removeDoctorAvailability(Id);

    }

    @Override
    public void getRemoveResponse() {
        Toast.makeText(this, "Deleted Successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(DoctorAvailableActivity.this, DoctorEditActivity.class);
        startActivity(intent);
        finish();
    }


}