package com.caafisom.android.provider.model.dto.common;

import com.google.gson.annotations.SerializedName;

public class RegisterService {

	@SerializedName("service_type_id")
	private int serviceTypeId;

	@SerializedName("service_model")
	private Object serviceModel;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("service_number")
	private Object serviceNumber;

	@SerializedName("provider_price")
	private int providerPrice;

	@SerializedName("status")
	private String status;

	public void setServiceTypeId(int serviceTypeId){
		this.serviceTypeId = serviceTypeId;
	}

	public int getServiceTypeId(){
		return serviceTypeId;
	}

	public void setServiceModel(Object serviceModel){
		this.serviceModel = serviceModel;
	}

	public Object getServiceModel(){
		return serviceModel;
	}

	public void setProviderId(int providerId){
		this.providerId = providerId;
	}

	public int getProviderId(){
		return providerId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setServiceNumber(Object serviceNumber){
		this.serviceNumber = serviceNumber;
	}

	public Object getServiceNumber(){
		return serviceNumber;
	}

	public void setProviderPrice(int providerPrice){
		this.providerPrice = providerPrice;
	}

	public int getProviderPrice(){
		return providerPrice;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}
}