package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.caafisom.android.provider.view.adapter.HistoryAdapter;

public interface IHistoryView extends IView<IHistoryPresenter> {
    void setAdapter(HistoryAdapter adapter);
    void initSetUp();
    void moveToChat(HistoryItem data);
}
