package com.caafisom.android.provider.view.iview;

import android.os.Bundle;
import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.view.View;
import android.widget.EditText;

import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.common.utils.PermissionUtils;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.view.activity.BaseActivity;


public interface IView<P> {

    void showToast(String message);

    void showToast(int resId);

    void showToast(CustomException e);

    void showSnackBar(String message);

    void showSnackBar(@NonNull View view, String message);

    void showSnackBarWithDelayExit(String message,long delay);

    void showSnackBarWithDelayExit(String message);

    void showAlertDialog(String message);

    void showAlertDialog(String message, BaseActivity.whenClicked clicked);

    void showAlertDialog(int resId);

    void showProgressbar();

    void dismissProgressbar();

    void showTextInputLayoutError(TextInputLayout til, EditText et, String msg);

    String getTextRes(int resId);

    boolean isNetworkEnabled();

    void showNetworkMessage();

    void navigateTo(Class<?> cls, boolean isFinishActivity, Bundle bundle);

    void changeFragment(Bundle bundle, @IdRes int containerViewId, @NonNull Fragment fragment, @NonNull String fragmentTag);

    CodeSnippet getCodeSnippet();

    PermissionUtils getPermissionUtils();

    FragmentActivity getActivity();

    void bindPresenter(P iPresenter);

    void makeLogout();

    boolean checkLocationStatus();
}