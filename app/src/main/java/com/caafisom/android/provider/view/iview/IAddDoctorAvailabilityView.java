package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.adapter.DoctorAvailabilityAdapter;

public interface IAddDoctorAvailabilityView extends IView<IAddDoctorAvailabilityPresenter> {
    void initSetUp();
    void getSuccessResponse();
}
