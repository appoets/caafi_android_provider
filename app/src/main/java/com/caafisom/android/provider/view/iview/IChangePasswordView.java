package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IChangePasswordPresenter;

public interface IChangePasswordView extends IView<IChangePasswordPresenter> {
                void goToLogin();
}
