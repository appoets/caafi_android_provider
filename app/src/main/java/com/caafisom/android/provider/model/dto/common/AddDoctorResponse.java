package com.caafisom.android.provider.model.dto.common;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class AddDoctorResponse extends BaseResponse {

    @SerializedName("message")
    @Expose
    private String mess_age;

   /* @SerializedName("data")
    @Expose
    private DoctorAddData data;*/

    public String getMess_age() {
        return mess_age;
    }

    public void setMess_age(String mess_age) {
        this.mess_age = mess_age;
    }

  /*public DoctorAddData getData() {
        return data;
    }

    public void setData(DoctorAddData data) {
        this.data = data;
    }*/
}
