package com.caafisom.android.provider.view.iview;

public interface IScheduleView {
    void goToNotificationScreen(String message);
    void showCalendarDialog();
    void showTimeDialog();
    void showProgressbar();
    void dismissProgressbar();
    void showSnackBar(String message);
    void showToast(String message);
    void showNetworkMessage();
}
