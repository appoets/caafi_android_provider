package com.caafisom.android.provider.model.listener;



import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.dto.response.BaseResponse;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public interface IModelListener<ML> {

    void onSuccessfulApi(@NotNull List<BaseResponse> response);

    void onSuccessfulApi(@NotNull ML response);
    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();
}
