package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.ILoginPresenter;

public interface ILoginView extends IView<ILoginPresenter> {
    void goToRegistration();
    void goToForgotPassword();
    void goToHome();
}
