package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class GetDoctorAvailablity extends BaseResponse {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("doctor_id")
    @Expose
    private String doctorId;
    @SerializedName("morning")
    @Expose
    private Integer morning;
    @SerializedName("afternoon")
    @Expose
    private Integer afternoon;
    @SerializedName("night")
    @Expose
    private Integer night;
    @SerializedName("onsite")
    @Expose
    private Integer onsite;
    @SerializedName("video_consultancy")
    @Expose
    private Integer videoConsultancy;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("available_date")
    @Expose
    private String availableDate;
    @SerializedName("morning_start_time")
    @Expose
    private Object morningStartTime;
    @SerializedName("morning_end_time")
    @Expose
    private Object morningEndTime;
    @SerializedName("afternoon_start_time")
    @Expose
    private Object afternoonStartTime;
    @SerializedName("afternoon_end_time")
    @Expose
    private Object afternoonEndTime;
    @SerializedName("night_start_time")
    @Expose
    private Object nightStartTime;
    @SerializedName("night_end_time")
    @Expose
    private Object nightEndTime;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public Integer getMorning() {
        return morning;
    }

    public void setMorning(Integer morning) {
        this.morning = morning;
    }

    public Integer getAfternoon() {
        return afternoon;
    }

    public void setAfternoon(Integer afternoon) {
        this.afternoon = afternoon;
    }

    public Integer getNight() {
        return night;
    }

    public void setNight(Integer night) {
        this.night = night;
    }

    public Integer getOnsite() {
        return onsite;
    }

    public void setOnsite(Integer onsite) {
        this.onsite = onsite;
    }

    public Integer getVideoConsultancy() {
        return videoConsultancy;
    }

    public void setVideoConsultancy(Integer videoConsultancy) {
        this.videoConsultancy = videoConsultancy;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getAvailableDate() {
        return availableDate;
    }

    public void setAvailableDate(String availableDate) {
        this.availableDate = availableDate;
    }

    public Object getMorningStartTime() {
        return morningStartTime;
    }

    public void setMorningStartTime(Object morningStartTime) {
        this.morningStartTime = morningStartTime;
    }

    public Object getMorningEndTime() {
        return morningEndTime;
    }

    public void setMorningEndTime(Object morningEndTime) {
        this.morningEndTime = morningEndTime;
    }

    public Object getAfternoonStartTime() {
        return afternoonStartTime;
    }

    public void setAfternoonStartTime(Object afternoonStartTime) {
        this.afternoonStartTime = afternoonStartTime;
    }

    public Object getAfternoonEndTime() {
        return afternoonEndTime;
    }

    public void setAfternoonEndTime(Object afternoonEndTime) {
        this.afternoonEndTime = afternoonEndTime;
    }

    public Object getNightStartTime() {
        return nightStartTime;
    }

    public void setNightStartTime(Object nightStartTime) {
        this.nightStartTime = nightStartTime;
    }

    public Object getNightEndTime() {
        return nightEndTime;
    }

    public void setNightEndTime(Object nightEndTime) {
        this.nightEndTime = nightEndTime;
    }

}
