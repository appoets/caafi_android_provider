package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.common.EditDoctorResponse;
import com.caafisom.android.provider.model.dto.request.RegisterRequest;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.RegisterResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class EditDoctorModel extends BaseModel<BaseResponse> {

    public EditDoctorModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    public void updateDoctor(Integer id, HashMap<String, RequestBody> map) {
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void updateDoctor(Integer doctorID, HashMap<String, RequestBody> map, MultipartBody.Part filePart1) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).updateDoctor(doctorID,map,filePart1));

    }
}
