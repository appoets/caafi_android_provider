package com.caafisom.android.provider.view.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.request.ResetPasswordRequest;
import com.caafisom.android.provider.presenter.ForgotChangePasswordPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;
import com.caafisom.android.provider.view.iview.IForgotChangePasswordView;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotChangePasswordActivity extends BaseActivity<IForgotChangePasswordPresenter> implements IForgotChangePasswordView {

    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Override
    int attachLayout() {
        return R.layout.activity_forgot_change_password;
    }

    @Override
    IForgotChangePasswordPresenter initialize() {
        return new ForgotChangePasswordPresenter(this);
    }



    @OnClick({R.id.btnChangePassword})
    public void OnClickView(View view){
        switch (view.getId()){

            case R.id.btnChangePassword:
                String password = etNewPassword.getText().toString().trim();
                String confirm_password = etConfirmPassword.getText().toString().trim();
                if (TextUtils.isEmpty(password)){
                    showSnackBar(getString(R.string.error_enter_new_password));
                }else if(TextUtils.isEmpty(confirm_password)){
                    showSnackBar(getString(R.string.error_please_enter_confirm_password));
                }else{
                    ResetPasswordRequest resetPasswordRequest = new ResetPasswordRequest();
                    resetPasswordRequest.setId("");
                    resetPasswordRequest.setPassword(password);
                    resetPasswordRequest.setPassword_confirmation(confirm_password);
                    iPresenter.resetPassword(resetPasswordRequest);
                }
                break;

        }
    }




    @Override
    public void goToLogin() {
        startActivity(new Intent(this,LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


}
