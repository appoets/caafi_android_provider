package com.caafisom.android.provider.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.provider.view.adapter.listener.IDoctorListRecyclerAdapter;
import com.caafisom.android.provider.view.adapter.viewholder.CityListViewHolder;
import com.caafisom.android.provider.view.adapter.viewholder.DoctorListViewHolder;

import java.util.List;


public class DoctorListRecyclerAdater extends BaseRecyclerAdapter<IDoctorListRecyclerAdapter, DoctorList, DoctorListViewHolder> {

    List<DoctorList> doctorListResponses;
    IDoctorListRecyclerAdapter iDoctorListRecyclerAdapter;

    public DoctorListRecyclerAdater(List<DoctorList> doctorListResponses, IDoctorListRecyclerAdapter iCityListRecyclerAdapter) {
        super(doctorListResponses, iCityListRecyclerAdapter);
        this.doctorListResponses = doctorListResponses;
        this.iDoctorListRecyclerAdapter = iCityListRecyclerAdapter;

    }

    @NonNull
    @Override
    public DoctorListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new DoctorListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.doctors_list_items,parent,false),iDoctorListRecyclerAdapter);
    }
}
