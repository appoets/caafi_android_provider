package com.caafisom.android.provider.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.model.dto.common.User;
import com.caafisom.android.provider.view.adapter.listener.IDoctorAvailabilityRecyclerListener;
import com.caafisom.android.provider.view.adapter.listener.IHistoryRecyclerListener;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class DoctorAvailabilityViewHolder extends BaseViewHolder<GetDoctorAvailablity, IDoctorAvailabilityRecyclerListener> {
    @BindView(R.id.date)
    TextView date;
    @BindView(R.id.morning_avail)
    TextView morning_avail;
    @BindView(R.id.afn_avail)
    TextView afn_avail;
    @BindView(R.id.night_avail)
    TextView night_avail;
    @BindView(R.id.iv_delete)
    ImageView iv_delete;

    public DoctorAvailabilityViewHolder(View itemView, IDoctorAvailabilityRecyclerListener listener) {
        super(itemView, listener);
    }


    @Override
    void populateData(GetDoctorAvailablity data) {

        date.setText(data.getAvailableDate());

        iv_delete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                listener.onClickItem(getAdapterPosition(), data);
            }
        });

    }

   /* @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }*/

}
