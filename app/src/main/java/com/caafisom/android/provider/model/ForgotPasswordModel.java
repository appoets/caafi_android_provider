package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.response.ForgotPasswordResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class ForgotPasswordModel extends BaseModel<ForgotPasswordResponse> {

    public ForgotPasswordModel(IModelListener<ForgotPasswordResponse> listener) {
        super(listener);
    }

    public void getOTPDetails(String email) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).forgotPassword(email));
    }

    @Override
    public void onSuccessfulApi(ForgotPasswordResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
