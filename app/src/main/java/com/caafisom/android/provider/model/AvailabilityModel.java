package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.request.AvailabilityRequest;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class AvailabilityModel extends BaseModel<ProfileResponse> {

    public AvailabilityModel(IModelListener<ProfileResponse> listener) {
        super(listener);
    }

    public void postAvailability(AvailabilityRequest request) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postAvailability(request));
    }

    @Override
    public void onSuccessfulApi(ProfileResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
