package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.common.ProviderSpeciality;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class DoctorSpecialityModel extends BaseModel<DoctorSpecialityResponse> {


    public DoctorSpecialityModel(IModelListener<DoctorSpecialityResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(DoctorSpecialityResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getDoctorSpecialityList() {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getDoctorSpecialityList());
    }


}
