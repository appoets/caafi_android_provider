package com.caafisom.android.provider.model.dto.common.videocalldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FcmNotification {

    @SerializedName("body")
    @Expose
    private String body;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("content_available")
    @Expose
    private Boolean contentAvailable;
    @SerializedName("priority")
    @Expose
    private String priority;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Boolean getContentAvailable() {
        return contentAvailable;
    }

    public void setContentAvailable(Boolean contentAvailable) {
        this.contentAvailable = contentAvailable;
    }

    public String getPriority() {
        return priority;
    }

    public void setPriority(String priority) {
        this.priority = priority;
    }
}
