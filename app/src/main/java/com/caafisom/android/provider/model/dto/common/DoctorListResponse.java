package com.caafisom.android.provider.model.dto.common;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorListResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private List<DoctorList> data = null;

    public List<DoctorList> getData() {
        return data;
    }

    public void setData(List<DoctorList> data) {
        this.data = data;
    }
}
