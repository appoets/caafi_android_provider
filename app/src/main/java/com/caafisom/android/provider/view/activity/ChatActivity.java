package com.caafisom.android.provider.view.activity;

import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.chat.Chat;
import com.caafisom.android.provider.chat.ChatMessageAdapter;
import com.caafisom.android.provider.presenter.ChatPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IChatPresenter;
import com.caafisom.android.provider.view.iview.IChatView;

import java.util.ArrayList;
import java.util.Date;

import butterknife.BindView;
import butterknife.OnClick;

public class ChatActivity extends BaseActivity<IChatPresenter> implements IChatView {

    ChatMessageAdapter mAdapter;
    @BindView(R.id.back)
    ImageView back;
    @BindView(R.id.title)
    TextView title;
    @BindView(R.id.chat_lv)
    ListView mChatView;
    @BindView(R.id.et_message)
    EditText etMessage;
    @BindView(R.id.send)
    ImageView send;
    FirebaseDatabase database;
    DatabaseReference myRef;
    Integer sender = -1, userid = -1;
    String senderName="", senderAvatar="", chatPath = "";


    StorageReference storageRef;


    @Override
    int attachLayout() {
        return R.layout.activity_chat_view;
    }

    @Override
    IChatPresenter initialize() {
        return new ChatPresenter(this);
    }

    @Override
    public void setUp() {

         Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        userid = extras.getInt("ct_user_id");
        sender = extras.getInt("sender_id");
        senderName = extras.getString("sender_name");


        //title.setText(senderName);
        title.setText("Chat");

       /* if (userid < sender){
            chatPath = userid+"_user_chat_"+sender+"_provider";
        }else{
            chatPath = sender+"_provider_chat_"+userid+"_user";
        }*/

        chatPath = userid+"_user_chat_"+sender+"_provider";


        System.out.println(chatPath);
        storageRef = FirebaseStorage.getInstance().getReference();

        mAdapter = new ChatMessageAdapter(this, new ArrayList<>(), senderAvatar);
        mChatView.setAdapter(mAdapter);

        database = FirebaseDatabase.getInstance();
        myRef = database.getReference().child(chatPath);
        myRef.addChildEventListener(new ChildEventListener() {
            @Override
            public void onChildAdded(DataSnapshot dataSnapshot, String prevChildKey) {
                Chat chat = dataSnapshot.getValue(Chat.class);
                if (chat!=null)
                    mAdapter.add(chat);
            }

            @Override
            public void onChildChanged(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onChildRemoved(DataSnapshot dataSnapshot) {
            }

            @Override
            public void onChildMoved(DataSnapshot dataSnapshot, String prevChildKey) {
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
            }
        });
    }


    @OnClick({R.id.back, R.id.send, R.id.image, R.id.title})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back:
                onBackPressed();
                break;
            case R.id.send:
                String myText = etMessage.getText().toString().trim();
                if (myText.length() > 0) {
                    sendMessage(myText);
                }
                break;
            case R.id.title:
                break;
        }
    }

    private void sendMessage(String message) {

        Chat chat = new Chat();
        chat.setRead(0);
        chat.setReciever(sender);
        chat.setTimestamp(new Date().getTime());
        chat.setType("text");
        chat.setUsertype(getString(R.string.type_of_user));
        chat.setText(message);

        myRef.push().setValue(chat);

        etMessage.setText("");
    }




}
