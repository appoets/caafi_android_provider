package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.request.PasswordRequest;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class ChangePasswordModel extends BaseModel<BaseResponse> {

    public ChangePasswordModel(IModelListener<BaseResponse> listener) {
        super(listener);
    }

    public void updatePassword(PasswordRequest request) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).changePassword(request));
    }

    @Override
    public void onSuccessfulApi(BaseResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
