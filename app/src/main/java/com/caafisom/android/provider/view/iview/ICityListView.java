package com.caafisom.android.provider.view.iview;


import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.presenter.ipresenter.ICityListPresenter;
import com.caafisom.android.provider.view.adapter.CityListRecyclerAdater;

/**
 * Created by Tranxit Technologies.
 */

public interface ICityListView extends IView<ICityListPresenter> {
    void setAdapter(CityListRecyclerAdater adapter);
    void getCity(CityList data);
    void initSetUp();
}
