package com.caafisom.android.provider.model.dto.common.videocalldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by CSS08 on 20-03-2018.
 */

public class Data {



    @SerializedName("device_type")
    @Expose
    private String device_type="";
    @SerializedName("device_token")
    @Expose
    private String device_token="";
    @SerializedName("callType")
    @Expose
    private String callType="";
    @SerializedName("id")
    @Expose
    private String id="";
    @SerializedName("name")
    @Expose
    private String name="";
    @SerializedName("patientName")
    @Expose
    private String patientName="";
    @SerializedName("call_duration")
    @Expose
    private String call_duration="";
    @SerializedName("avatar")
    @Expose
    private String avatar="";
    @SerializedName("mobile")
    @Expose
    private String mobile="";

    @SerializedName("request_id")
    @Expose
    private String request_id="";

    public String getRequest_id() {
        return request_id;
    }

    public void setRequest_id(String request_id) {
        this.request_id = request_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getCallType() {
        return callType;
    }

    public void setCallType(String callType) {
        this.callType = callType;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPatientName() {
        return patientName;
    }

    public void setPatientName(String patientName) {
        this.patientName = patientName;
    }

    public String getCall_duration() {
        return call_duration;
    }

    public void setCall_duration(String call_duration) {
        this.call_duration = call_duration;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }
}
