package com.caafisom.android.provider.model.dto.response;

import com.google.gson.annotations.SerializedName;

public class HelpResponse extends BaseResponse{

	@SerializedName("contact_text")
	private String contactText;

	@SerializedName("contact_title")
	private String contactTitle;

	@SerializedName("contact_number")
	private String contactNumber;

	@SerializedName("contact_email")
	private String contactEmail;

	public void setContactText(String contactText){
		this.contactText = contactText;
	}

	public String getContactText(){
		return contactText;
	}

	public void setContactTitle(String contactTitle){
		this.contactTitle = contactTitle;
	}

	public String getContactTitle(){
		return contactTitle;
	}

	public void setContactNumber(String contactNumber){
		this.contactNumber = contactNumber;
	}

	public String getContactNumber(){
		return contactNumber;
	}

	public void setContactEmail(String contactEmail){
		this.contactEmail = contactEmail;
	}

	public String getContactEmail(){
		return contactEmail;
	}
}