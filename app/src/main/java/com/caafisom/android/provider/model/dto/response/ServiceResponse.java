package com.caafisom.android.provider.model.dto.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.ServiceItem;

public class ServiceResponse extends BaseResponse {

	@SerializedName("service")
	private List<ServiceItem> service;

	public void setService(List<ServiceItem> service){
		this.service = service;
	}

	public List<ServiceItem> getService(){
		return service;
	}
}