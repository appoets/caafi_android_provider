package com.caafisom.android.provider.presenter.ipresenter;

public interface IAddDoctorAvailabilityPresenter extends IPresenter {
        void addDoctorAvailability(Integer id, String selectDate);
}