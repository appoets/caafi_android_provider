package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.response.InvoiceResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class InvoiceModel extends BaseModel<InvoiceResponse> {

    public InvoiceModel(IModelListener<InvoiceResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(InvoiceResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getInvoice(String id) {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getInvoice(id));
    }
}
