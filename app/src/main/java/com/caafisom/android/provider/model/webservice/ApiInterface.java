package com.caafisom.android.provider.model.webservice;


import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.dto.common.AddDoctorResponse;
import com.caafisom.android.provider.model.dto.common.City;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.common.EditDoctorResponse;
import com.caafisom.android.provider.model.dto.common.ProviderSpeciality;
import com.caafisom.android.provider.model.dto.common.RemoveDoctor;
import com.caafisom.android.provider.model.dto.common.videocalldata.Fcm;
import com.caafisom.android.provider.model.dto.request.AvailabilityRequest;
import com.caafisom.android.provider.model.dto.request.LocationRequest;
import com.caafisom.android.provider.model.dto.request.LoginRequest;
import com.caafisom.android.provider.model.dto.request.PasswordRequest;
import com.caafisom.android.provider.model.dto.request.RatingRequest;
import com.caafisom.android.provider.model.dto.request.RegisterRequest;
import com.caafisom.android.provider.model.dto.request.ResetPasswordRequest;
import com.caafisom.android.provider.model.dto.request.ScheduleRequest;
import com.caafisom.android.provider.model.dto.response.AccessToken;
import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.CheckStatusResponse;
import com.caafisom.android.provider.model.dto.response.ForgotPasswordResponse;
import com.caafisom.android.provider.model.dto.response.HelpResponse;
import com.caafisom.android.provider.model.dto.response.HistoryResponse;
import com.caafisom.android.provider.model.dto.response.InvoiceResponse;
import com.caafisom.android.provider.model.dto.response.LoginResponse;
import com.caafisom.android.provider.model.dto.response.NotificationResponse;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.model.dto.response.RegisterResponse;
import com.caafisom.android.provider.model.dto.response.ScheduleResponse;
import com.caafisom.android.provider.model.dto.response.ScheduledListResponse;
import com.caafisom.android.provider.model.dto.response.ServiceResponse;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.PartMap;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface ApiInterface {

    @POST("api/provider/oauth/token")
    Call<LoginResponse> postLogin(@Body LoginRequest request);

    @GET("api/provider/profile")
    Call<ProfileResponse> getUserDetails();

    @GET("api/provider/services")
    Call<ServiceResponse> getServiceDetails();

    @GET("api/provider/help")
    Call<HelpResponse> getHelpDetails();

    @POST("api/provider/register")
    Call<RegisterResponse> postRegister(@Body RegisterRequest request);

    @POST("api/provider/profile/available")
    Call<ProfileResponse> postAvailability(@Body AvailabilityRequest request);

    @Multipart
    @POST("api/provider/profile")
    Call<ProfileResponse> postProfileUpdate(@PartMap HashMap<String, RequestBody> params, @Part MultipartBody.Part filePart, @Part MultipartBody.Part filePart2);

    @GET("api/provider/requests/history")
    Call<HistoryResponse> getHistoryDetails();

    @FormUrlEncoded
    @POST("api/provider/forgot/password")
    Call<ForgotPasswordResponse> forgotPassword(@Field("email") String email);

    @POST("api/provider/profile/password")
    Call<BaseResponse> changePassword(@Body PasswordRequest passwordRequest);

    @POST("api/user/reset/password")
    Call<BaseResponse> resetPassword(@Body ResetPasswordRequest resetPasswordRequest);

    @FormUrlEncoded
    @POST("api/provider/devicetoken")
    Call<BaseResponse> updateToken(@Field("device_token") String token);

    @Headers({"Content-Type: application/json", "Authorization: key=AIzaSyAm1fQ5lcaMfa8QyV9DNoapTXfBSajCQQw"})
    @POST("fcm/send")
    Call<BaseResponse> sendFCM(@Body Fcm fcm);

    @POST("api/provider/profile/location")
    Call<BaseResponse> updateLocation(@Body LocationRequest request);

    @POST("api/provider/trip/invoice/{id}")
    Call<InvoiceResponse> getInvoice(@Path("id") String id);

    @POST("api/provider/trip/{id}/rate")
    Call<BaseResponse> updateRating(@Path("id") String id, @Body RatingRequest request);

    @GET("api/provider/missed")
    Call<NotificationResponse> getMissed();

    @POST("api/provider/trip/schedule/call")
    Call<ScheduleResponse> schedule(@Body ScheduleRequest request);

    @POST("api/provider/appointment/history")
    Call<List<ScheduledListResponse>> getScheduledList();

    @GET("api/provider/search")
    Call<CheckStatusResponse> getRequest();

    @FormUrlEncoded
    @POST("api/provider/requests/{id}")
    Call<BaseResponse> changeStatus(@Path("id") Integer id, @Field("status") String status);

    @POST("/api/provider/trip/{id}/{status}")
    Call<Object> acceptORreject(@Path("id") Integer id, @Path("status") String status);

    @GET("api/provider/video/access/token")
    Call<AccessToken> getTwilloToken(@Query("room_id") Object obj);

    @GET("/api/provider/view/request/{id}")
    Call<List<ScheduledListResponse>> getSingleAppoinment(@Path("id") String id);


    @GET("api/provider/profile/time/availablity")
    Call<List<AvailabilityListResponse>> getAvailablity(@Query("date") String date);

    @FormUrlEncoded
    @POST("api/provider/profile/time/availablity")
    Call<List<AvailabilityListResponse>> postAvailablity(@FieldMap HashMap<String, Object> params);

    @GET("api/user/city")
    Call<City> getCity();

    @GET("api/provider/doctor")
    Call<DoctorListResponse> getDoctorsList();

    @GET("api/provider/speciality")
    Call<DoctorSpecialityResponse> getDoctorSpecialityList();

    @Multipart
    @POST("api/provider/doctor")
    Call<BaseResponse> addDoctor(@PartMap HashMap<String, RequestBody> params, @Part MultipartBody.Part filePart);

    @Multipart
    @POST("api/provider/doctor/{id}")
    Call<BaseResponse> updateDoctor(@Path("id") Integer id, @PartMap HashMap<String, RequestBody> map, @Part MultipartBody.Part filePart1);

    @POST("api/provider/destroy/doctor/{id}")
    Call<BaseResponse> removeDoctor(@Path("id") Integer id);

    @GET("api/provider/availability/{id}")
    Call<List<GetDoctorAvailablity>> getDoctorAvailablity(@Path("id") Integer id);

    @POST("api/provider/availability/delete/{id}")
    Call<BaseResponse> removeDoctorAvailability(@Path("id") Integer id);

    @FormUrlEncoded
    @POST("api/provider/availability/{id}")
    Call<List<GetDoctorAvailablity>> postDoctorAvailability(@Path("id") Integer id, @Field("available_date") String selectDate);



}