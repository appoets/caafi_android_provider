package com.caafisom.android.provider.model.dto.common;

import com.google.gson.annotations.SerializedName;

public class ServiceItem{

	@SerializedName("image")
	private String image;

	@SerializedName("price")
	private int price;

	@SerializedName("name")
	private String name;

	@SerializedName("available")
	private boolean available;

	@SerializedName("description")
	private Object description;

	@SerializedName("fixed")
	private Double fixed;

	@SerializedName("id")
	private int id;

	@SerializedName("provider_name")
	private String providerName;

	@SerializedName("status")
	private int status;

	public void setImage(String image){
		this.image = image;
	}

	public String getImage(){
		return image;
	}

	public void setPrice(int price){
		this.price = price;
	}

	public int getPrice(){
		return price;
	}

	public void setName(String name){
		this.name = name;
	}

	public String getName(){
		return name;
	}

	public void setAvailable(boolean available){
		this.available = available;
	}

	public boolean isAvailable(){
		return available;
	}

	public void setDescription(Object description){
		this.description = description;
	}

	public Object getDescription(){
		return description;
	}

	public void setFixed(Double fixed){
		this.fixed = fixed;
	}

	public Double getFixed(){
		return fixed;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setProviderName(String providerName){
		this.providerName = providerName;
	}

	public String getProviderName(){
		return providerName;
	}

	public void setStatus(int status){
		this.status = status;
	}

	public int getStatus(){
		return status;
	}

	@Override
	public String toString() {
		return name;
	}
}