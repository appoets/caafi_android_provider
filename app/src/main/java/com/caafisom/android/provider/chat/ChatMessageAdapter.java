package com.caafisom.android.provider.chat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.utils.CodeSnippet;

import java.util.List;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;

/**
 * Created by Tranxit Technology.
 */

public class ChatMessageAdapter extends ArrayAdapter<Chat> {
    private static final int MY_MESSAGE = 0, OTHER_MESSAGE = 1, MY_IMAGE = 2, OTHER_IMAGE = 3;
    private Context context;
    String senderAvatar;

    public ChatMessageAdapter(Context context, List<Chat> data, String senderAvatar) {
        super(context, R.layout.item_mine_message, data);
        this.context = context;
        this.senderAvatar = senderAvatar;
    }

    @Override
    public int getViewTypeCount() {
        // my message, other message, my image, other image
        return 2;
    }

    @Override
    public int getItemViewType(int position) {
        Chat item = getItem(position);

        assert item != null;
        if (item.getReciever() == getApplicationInstance().getUserID() && item.getUsertype().equalsIgnoreCase(context.getString(R.string.type_of_user))) {
            return MY_MESSAGE;
        } else
            return OTHER_MESSAGE;

        /*if (item.getIsMine()) return MY_MESSAGE;
        else return OTHER_MESSAGE;
*/
        /*if (item.isMine() && !item.isImage()) return MY_MESSAGE;
        else if (!item.isMine() && !item.isImage()) return OTHER_MESSAGE;
        else if (item.isMine() && item.isImage()) return MY_IMAGE;
        else return OTHER_IMAGE;*/
    }

    @SuppressLint("ClickableViewAccessibility")
    @NonNull
    @Override
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {
        int viewType = getItemViewType(position);
        final Chat chat = getItem(position);
        if (viewType == MY_MESSAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_message, parent, false);
            if (chat.getType().equals("text")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_message, parent, false);
                TextView textView = (TextView) convertView.findViewById(R.id.text);
                textView.setText(getItem(position).getText());
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
            }/* else if (chat.getType().equals("image")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_image, parent, false);
                ImageView mine_image = (ImageView) convertView.findViewById(R.id.mine_image);
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
                //CodeSnippet.loadImageCircle(chat.getUrl(),mine_image,R.drawable.user);
            }else if (chat.getType().equals("video")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_video, parent, false);
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
                ImageView mine_video = (ImageView) convertView.findViewById(R.id.mine_video);
                final Uri uri = Uri.parse(chat.getUrl());
                CodeSnippet.loadImageCircle(chat.getUrl(),mine_video,R.drawable.user);
                mine_video.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setDataAndType(uri, "video/*");
                        context.startActivity(intent);
                    }
                });
            }*/
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_message, parent, false);
            if (chat.getType().equals("text")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_message, parent, false);
                TextView textView = (TextView) convertView.findViewById(R.id.text);
                ImageView sender_avatar = (ImageView) convertView.findViewById(R.id.sender_avatar);
                textView.setText(getItem(position).getText());
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                if (chat.getTimestamp()!=null)
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
                CodeSnippet.loadImageCircle(senderAvatar,sender_avatar,R.drawable.user);
            } /*else if (chat.getType().equals("image")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_image, parent, false);
                ImageView other_image = (ImageView) convertView.findViewById(R.id.other_image);
                CodeSnippet.loadImageCircle(chat.getUrl(),other_image,R.drawable.user);
                ImageView sender_avatar = (ImageView) convertView.findViewById(R.id.sender_avatar);
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
                CodeSnippet.loadImageCircle(senderAvatar,sender_avatar,R.drawable.user);
            }else if (chat.getType().equals("video")) {
                convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_video, parent, false);
                ImageView sender_avatar = (ImageView) convertView.findViewById(R.id.sender_avatar);
                TextView timestamp = (TextView) convertView.findViewById(R.id.timestamp);
                timestamp.setText(String.valueOf(CodeSnippet.getDisplayableTime(chat.getTimestamp())));
                CodeSnippet.loadImageCircle(senderAvatar,sender_avatar,R.drawable.user);
                ImageView other_video = (ImageView) convertView.findViewById(R.id.other_video);
                final Uri uri = Uri.parse(chat.getUrl());
                Glide.with(context)
                        .load(chat.getUrl())
                        .into(other_video);
                other_video.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
                        intent.setDataAndType(uri, "video/*");
                        context.startActivity(intent);
                    }
                });
            }*/
        }

        /* else if(viewType == OTHER_MESSAGE){
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_message, parent, false);

            TextView textView = (TextView) convertView.findViewById(R.id.text);
            //textView.setText(getItem(position).getContent());
        } else if (viewType == MY_IMAGE) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_mine_image, parent, false);
        } else {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_other_image, parent, false);
        }*/

        /*convertView.findViewById(R.id.chatMessageView).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getContext(), "onClick", Toast.LENGTH_LONG).show();
            }
        });*/


        return convertView;
    }
}
