package com.caafisom.android.provider.view.activity;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.Constants;
import com.caafisom.android.provider.model.dto.request.RatingRequest;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.ScheduledListResponse;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;
import com.caafisom.android.provider.view.widget.CustomProgressbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;
import static com.caafisom.android.provider.view.activity.HomeActivity.currentRequestID;

public class ScheduleDetailActivity extends AppCompatActivity implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    //Flow Layout
    @BindView(R.id.ivBack)
    ImageView ivBack;
    @BindView(R.id.statusLayout)
    LinearLayout statusLayout;
    @BindView(R.id.iv_profile)
    CircleImageView iv_profile;
    @BindView(R.id.tv_doctor_name)
    TextView tv_doctor_name;
    @BindView(R.id.tv_treatment)
    TextView tv_treatment;
    @BindView(R.id.tv_rate_txt)
    TextView tv_rate_txt;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tv_desc_txt)
    TextView tv_desc_txt;
    @BindView(R.id.lblCount)
    TextView lblCount;
    @BindView(R.id.btnTreatment)
    Button btnTreatment;

    //Invoice
    @BindView(R.id.llInvoice)
    LinearLayout llInvoice;
    @BindView(R.id.ivInvoiceProfile)
    CircleImageView ivInvoiceProfile;
    @BindView(R.id.tv_base_rate)
    TextView tv_base_rate;
    @BindView(R.id.tv_pay_doctor)
    TextView tv_pay_doctor;
    @BindView(R.id.tv_ztoid_fee)
    TextView tv_ztoid_fee;
    @BindView(R.id.tv_promotion)
    TextView tv_promotion;
    @BindView(R.id.tv_balance_wallet)
    TextView tv_balance_wallet;
    @BindView(R.id.tv_tax)
    TextView tv_tax;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.tv_amount_paid)
    TextView tv_amount_paid;
    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    @BindView(R.id.llRatingLayout)
    LinearLayout llRatingLayout;
    @BindView(R.id.ivRatingProfile)
    CircleImageView ivRatingProfile;
    @BindView(R.id.rb_doctor)
    RatingBar rb_doctor;
    @BindView(R.id.et_review)
    EditText et_review;
    @BindView(R.id.btn_submit)
    Button btn_submit;

    private String requestID;
    private ScheduledListResponse data;
    private GoogleMap mMap;
    private Marker mCtLocationMarker;
    GoogleApiClient mGoogleApiClient;
    private LocationRequest mLocationRequest;
    boolean setData = false;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_schedule_detail);
        ButterKnife.bind(this);
        if (getIntent().getStringExtra("from").equalsIgnoreCase("video")) {
            callgetSingleAppoinment(currentRequestID + "");
        } else {
            data = getIntent().getParcelableExtra("data");
            setData();
            callgetSingleAppoinment(data.getId() + "");

        }
        buildGoogleApiClient();
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private void setData() {
        if (data.getStatus().equalsIgnoreCase("SCHEDULED") || data.getStatus().equalsIgnoreCase("ARRIVED")) {
            statusLayout.setVisibility(View.VISIBLE);
            llInvoice.setVisibility(View.GONE);
            btnTreatment.setVisibility(View.VISIBLE);
            btnTreatment.setText(getResources().getString(R.string.start));
            setScheduleorStartedData(data);
        } else if (data.getStatus().equalsIgnoreCase("STARTED") ||
                data.getStatus().equalsIgnoreCase("PICKEDUP")) {
            statusLayout.setVisibility(View.VISIBLE);
            llInvoice.setVisibility(View.GONE);
            btnTreatment.setVisibility(View.VISIBLE);
            lblCount.setVisibility(View.GONE);// VISIBLE
            btnTreatment.setText(getResources().getString(R.string.end));
            setScheduleorStartedData(data);
        } else if (data.getStatus().equalsIgnoreCase("DROPPED") ||
                data.getStatus().equalsIgnoreCase("COMPLETED")) {
            setInvoiceData();
        }
    }

    private void setScheduleorStartedData(ScheduledListResponse response) {
        tv_doctor_name.setText(response.getUser().getFirstName() + " " + response.getUser().getLastName());
        Glide.with(this).load(Constants.URL.BASE_URL_STORAGE + response.getUser().getPicture()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(iv_profile);
        tv_treatment.setText(getApplicationInstance().getCategory());
        // String base_rate = getApplicationInstance().getCurrency() + response.getService_type().getPrice() + "/min";
        // tv_rate_txt.setText(base_rate);
        if (response.getService_type() != null) {
            if (response.getService_type().getDescription() != null)
                tv_desc_txt.setText(response.getService_type().getDescription());
            String base_rate = getApplicationInstance().getCurrency() + response.getService_type().getPrice() + "/min";
            tv_rate_txt.setText(base_rate);
        }
        if (response.getScheduleAt() != null && !response.getScheduleAt().equalsIgnoreCase("null")) {
            tvDate.setText(response.getScheduleAt().split(" ")[0]);
            tvTime.setText(response.getScheduleAt().split(" ")[1]);
        }
    }

    public void setInvoiceData() {
        statusLayout.setVisibility(View.GONE);
        llInvoice.setVisibility(View.VISIBLE);
        lblCount.setVisibility(View.GONE); // VISIBLE
        if (data.getPayment() != null) {
            if (data.getService_type() != null)
                tv_desc_txt.setText(data.getService_type().getDescription());
            String base_rate = getApplicationInstance().getCurrency() + data.getPayment().getFixed() + "/min";
            tv_base_rate.setText(base_rate);
            tv_rate_txt.setText(base_rate);
            tv_pay_doctor.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getFixed());
            tv_ztoid_fee.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getCommision());
            tv_promotion.setText(getApplicationInstance().getCurrency() + "" + "0");
            tv_balance_wallet.setText(getApplicationInstance().getCurrency() + "" + "0");
            tv_tax.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTax());
            tv_total.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTotal());
            tv_amount_paid.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTotal());
        }
    }

    @OnClick({R.id.ivBack, R.id.btnTreatment, R.id.btnConfirm, R.id.btn_submit})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;
            case R.id.btnTreatment:
                if (btnTreatment.getText().toString().equalsIgnoreCase("start treatment"))
                    callStartandEnd("PICKEDUP");
                else
                    callStartandEnd("DROPPED");
                break;
            case R.id.btnConfirm:
//                callComlpeteRequest("COMPLETED");
                statusLayout.setVisibility(View.GONE);
                llInvoice.setVisibility(View.GONE);
                llRatingLayout.setVisibility(View.VISIBLE);
                lblCount.setVisibility(View.GONE);
                setRatingData();
                break;
            case R.id.btn_submit:
                if (Math.round(rb_doctor.getRating()) > 0)
                    callRatingAPI();
                break;
            default:
                break;
        }
    }

    private void callRatingAPI() {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();
        RatingRequest request = new RatingRequest();
        request.setCommand(et_review.getText().toString());
        request.setRating("" + Math.round(rb_doctor.getRating()));
        Call<BaseResponse> call = new ApiClient().getClient().create(ApiInterface.class).updateRating("" + data.getId(), request);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull retrofit2.Response<BaseResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        onBackPressed();
                        //   finish();
                        // startActivity(new Intent(ScheduleDetailActivity.this,  ScheduledListActivity.class));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });
    }

    private void setRatingData() {
        Glide.with(this).load(Constants.URL.BASE_URL_STORAGE + data.getUser().getPicture()).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(ivRatingProfile);
    }

    private void callStartandEnd(String status) {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();
        Call<BaseResponse> call = new ApiClient().getClient().create(ApiInterface.class).changeStatus(data.getId(), status);
        call.enqueue(new Callback<BaseResponse>() {
            @Override
            public void onResponse(@NonNull Call<BaseResponse> call, @NonNull retrofit2.Response<BaseResponse> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        if (btnTreatment.getText().toString().equalsIgnoreCase("start treatment")) {
                            btnTreatment.setText("End Treatment");
                        } else {
                            statusLayout.setVisibility(View.GONE);
                            llInvoice.setVisibility(View.VISIBLE);
                            setInvoiceData();
                            callgetSingleAppoinment(data.getId() + "");
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<BaseResponse> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });
    }

    private void callgetSingleAppoinment(String id) {
        Call<List<ScheduledListResponse>> call = new ApiClient().getClient().create(ApiInterface.class).getSingleAppoinment(id);
        call.enqueue(new Callback<List<ScheduledListResponse>>() {
            @Override
            public void onResponse(@NonNull Call<List<ScheduledListResponse>> call, @NonNull retrofit2.Response<List<ScheduledListResponse>> response) {
                if (response.isSuccessful()) {
                    try {
                        Log.d("callgetSingleAppoinment", "onResponse: " + response.body());
                        data = response.body().get(0);
                        if (!setData) {
                            setData();
                            setData = true;
                        }
                        if (data.getPayment() != null) {
                            String base_rate = getApplicationInstance().getCurrency() + data.getPayment().getFixed() + "/min";
                            tv_base_rate.setText(base_rate);
                            tv_pay_doctor.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getFixed());
                            tv_ztoid_fee.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getCommision());
                            tv_promotion.setText(getApplicationInstance().getCurrency() + "" + "0");
                            tv_balance_wallet.setText(getApplicationInstance().getCurrency() + "" + "0");
                            tv_tax.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTax());
                            tv_total.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTotal());
                            tv_amount_paid.setText(getApplicationInstance().getCurrency() + "" + data.getPayment().getTotal());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<List<ScheduledListResponse>> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
            }
        });
    }


    private Handler mCheckStatusHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            mCheckStatusHandler.postDelayed(runnable, 5000);
            Log.e("ScheduleDetailActivity", "running");
            callgetSingleAppoinment(data.getId() + "");
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        mCheckStatusHandler.postDelayed(runnable, 5000);
        Log.e("ScheduleDetailActivity", "started");
    }

    @Override
    protected void onPause() {
        super.onPause();
        mCheckStatusHandler.removeCallbacks(runnable);
        Log.e("ScheduleDetailActivity", "stoped");
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style_json));
        } catch (Exception e) {
            e.printStackTrace();
        }
//        mMap.getUiSettings().setAllGesturesEnabled(false);
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            changeMapLocation(location);
        }
    }

    private void changeMapLocation(Location location) {
        if (mMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            float zoom = 15.0f;
            if (mCtLocationMarker == null) {
                mCtLocationMarker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                        .anchor(0.5f, 0.5f)
                        .position(latLng));
            }
            boolean contains = mMap.getProjection()
                    .getVisibleRegion()
                    .latLngBounds
                    .contains(latLng);
            if (!contains) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            }
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }
}