package com.caafisom.android.provider.presenter.ipresenter;

import com.caafisom.android.provider.model.dto.request.LoginRequest;
import com.caafisom.android.provider.model.dto.request.RegisterRequest;

public interface IRegisterPresenter extends IPresenter {
    void goToLogin();
    void getServiceData();
    void postLogin(LoginRequest request);
    void postRegister(RegisterRequest registerRequest);
}