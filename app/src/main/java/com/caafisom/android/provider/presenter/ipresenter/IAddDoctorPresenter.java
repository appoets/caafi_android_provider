package com.caafisom.android.provider.presenter.ipresenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IAddDoctorPresenter extends IPresenter {
    void addDoctor(HashMap<String, RequestBody> params, MultipartBody.Part filePart);

}
