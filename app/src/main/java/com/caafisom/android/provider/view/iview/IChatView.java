package com.caafisom.android.provider.view.iview;


import com.caafisom.android.provider.presenter.ipresenter.IChatPresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface IChatView extends IView<IChatPresenter> {
        void setUp();
}
