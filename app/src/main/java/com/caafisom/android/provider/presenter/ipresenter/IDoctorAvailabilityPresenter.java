package com.caafisom.android.provider.presenter.ipresenter;

public interface IDoctorAvailabilityPresenter extends IPresenter {
        void getDoctorAvailability(Integer id);
        void removeDoctorAvailability(Integer id);
}