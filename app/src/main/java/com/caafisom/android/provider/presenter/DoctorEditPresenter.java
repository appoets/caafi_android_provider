package com.caafisom.android.provider.presenter;

import android.os.Bundle;
import android.widget.Toast;

import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.DoctorListModel;
import com.caafisom.android.provider.model.DoctorSpecialityModel;
import com.caafisom.android.provider.model.EditDoctorModel;
import com.caafisom.android.provider.model.RemoveDoctorModel;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.common.EditDoctorResponse;
import com.caafisom.android.provider.model.dto.common.RemoveDoctor;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorEditPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorPresenter;
import com.caafisom.android.provider.view.adapter.DoctorListRecyclerAdater;
import com.caafisom.android.provider.view.adapter.listener.IDoctorListRecyclerAdapter;
import com.caafisom.android.provider.view.iview.IDoctorEditView;
import com.caafisom.android.provider.view.iview.IDoctorView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DoctorEditPresenter extends BasePresenter<IDoctorEditView> implements IDoctorEditPresenter {

    public DoctorEditPresenter(IDoctorEditView iView) {
        super(iView);
        updateDoctorData();
        getDoctorSpeciality();
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
        updateDoctorData();
        getDoctorSpeciality();

    }

    private void getDoctorSpeciality() {
        iView.showProgressbar();
        new DoctorSpecialityModel(new IModelListener<DoctorSpecialityResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull DoctorSpecialityResponse response) {
                iView.dismissProgressbar();
                iView.getDoctorSpeciality(response);
            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).getDoctorSpecialityList();
    }

    @Override
    public void updateDoctorData() {

    }

    @Override
    public void postDoctor(Integer doctorID, HashMap<String, RequestBody> map, MultipartBody.Part filePart1) {
        iView.showProgressbar();

        new EditDoctorModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.getResponse();
            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).updateDoctor(doctorID,map,filePart1);

    }

    @Override
    public void removeDoctor(Integer doctorID) {
        iView.showProgressbar();

        new RemoveDoctorModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.getRemoveResponse();

            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).removeDoctor(doctorID);

    }


//  iView.updateDoctor(data);


}
