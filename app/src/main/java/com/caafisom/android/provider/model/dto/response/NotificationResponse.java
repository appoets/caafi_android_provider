
package com.caafisom.android.provider.model.dto.response;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationResponse extends BaseResponse {

    @SerializedName("provider")
    @Expose
    private List<Provider> provider = null;

    public List<Provider> getProvider() {
        return provider;
    }

    public void setProvider(List<Provider> provider) {
        this.provider = provider;
    }

}
