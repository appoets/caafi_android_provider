package com.caafisom.android.provider.presenter.ipresenter;

public interface IOnBoardPresenter extends IPresenter {

    void goToLogin();
}
