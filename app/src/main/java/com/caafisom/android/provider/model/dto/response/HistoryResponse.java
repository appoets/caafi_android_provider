package com.caafisom.android.provider.model.dto.response;

import java.util.List;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.HistoryItem;

public class HistoryResponse extends BaseResponse{

	@SerializedName("history")
	private List<HistoryItem> history;

	public void setHistory(List<HistoryItem> history){
		this.history = history;
	}

	public List<HistoryItem> getHistory(){
		return history;
	}
}