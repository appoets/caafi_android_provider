package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IOneTimePasswordPresenter;

public interface IOneTimePasswordView extends IView<IOneTimePasswordPresenter> {
        void goToForgotChangePassword();
        void setUp();
}
