package com.caafisom.android.provider.presenter.ipresenter;

public interface IForgotPasswordPresenter extends IPresenter {
    void goToOneTimePassword();
    void getOTPDetails(String email);
}