package com.caafisom.android.provider.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.model.dto.common.User;
import com.caafisom.android.provider.view.adapter.listener.IHistoryRecyclerListener;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;

public class HistoryViewHolder extends BaseViewHolder<HistoryItem, IHistoryRecyclerListener> {

    @BindView(R.id.civProfile)
    CircleImageView civProfile;

    @BindView(R.id.tvPatientName)
    TextView tvPatientName;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvbooking_id)
    TextView tvbookingid;
    @BindView(R.id.tvSpeciality)
    TextView tvSpeciality;
    @BindView(R.id.tvDoctorName)
    TextView tvDoctorName;

    @BindView(R.id.ivChat)
    ImageView ivChat;

    public HistoryViewHolder(View itemView, IHistoryRecyclerListener listener) {
        super(itemView, listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    void populateData(HistoryItem data) {
        if (data.getUser() != null) {
            User user = data.getUser();
            tvPatientName.setText(user.getFirstName() + " " + user.getLastName());
        }
        tvDate.setText(CodeSnippet.parseDateToyyyyMMdd(data.getAssignedAt()));
        tvbookingid.setText(data.getBookingId());
        if (data.getDoctor() != null && data.getDoctor().getSpeciality() != null) {
            tvSpeciality.setText(data.getDoctor().getSpeciality().getName());
            tvDoctorName.setText(data.getDoctor().getFirstName() + " " + data.getDoctor().getFirstName());
        }

    }

    @OnClick({R.id.ivChat})
    public void OnClick(View view) {
        switch (view.getId()) {
            case R.id.ivChat:
                listener.onClickItem(getAdapterPosition(), data);
                break;
        }
    }
}
