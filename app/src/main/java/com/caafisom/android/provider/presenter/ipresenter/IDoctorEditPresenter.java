package com.caafisom.android.provider.presenter.ipresenter;

import java.util.HashMap;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IDoctorEditPresenter extends IPresenter {
    void updateDoctorData();

    void postDoctor(Integer doctorID, HashMap<String, RequestBody> map, MultipartBody.Part filePart1);

    void removeDoctor(Integer doctorID);
}