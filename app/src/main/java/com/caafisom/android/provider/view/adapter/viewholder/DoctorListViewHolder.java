package com.caafisom.android.provider.view.adapter.viewholder;

import android.annotation.SuppressLint;
import android.view.View;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.provider.view.adapter.listener.IDoctorListRecyclerAdapter;

import butterknife.BindView;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorListViewHolder extends BaseViewHolder<DoctorList, IDoctorListRecyclerAdapter> {

    @BindView(R.id.tv_name)
    TextView tvname;
    @BindView(R.id.tv_specialise)
    TextView tvspecialise;
    @BindView(R.id.tv_experience)
    TextView tvexperience;

    public DoctorListViewHolder(View itemView, IDoctorListRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @SuppressLint("SetTextI18n")
    @Override
    void populateData(DoctorList data) {
        tvname.setText(data.getFirstName());
        tvspecialise.setText(data.getSpeciality().getName());
        tvexperience.setText(" " + data.getExperience() + " Experience");
    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
