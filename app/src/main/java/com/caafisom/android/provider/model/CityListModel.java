package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.common.City;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import java.util.List;

/**
 * Created by Tranxit Technologies.
 */

public class CityListModel extends BaseModel<City> {

    public CityListModel(IModelListener<City> listener) {
        super(listener);
    }


    @Override
    public void onSuccessfulApi(City response) {
        listener.onSuccessfulApi(response);
    }


    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getCityList() {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getCity());
    }
}
