package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.AvailabilityListModel;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.DoctorAvailabilityModel;
import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.RemoveDoctorAvailablityModel;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListListener;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IAvailabilityListPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.adapter.CityListRecyclerAdater;
import com.caafisom.android.provider.view.adapter.DoctorAvailabilityAdapter;
import com.caafisom.android.provider.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.provider.view.adapter.listener.IDoctorAvailabilityRecyclerListener;
import com.caafisom.android.provider.view.iview.IAvailabityListView;
import com.caafisom.android.provider.view.iview.IDoctorAvailabilityView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class DoctorAvailabilityPresenter extends BasePresenter<IDoctorAvailabilityView> implements IDoctorAvailabilityPresenter {


    public DoctorAvailabilityPresenter(IDoctorAvailabilityView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    IDoctorAvailabilityRecyclerListener iCityListRecyclerAdapter = new IDoctorAvailabilityRecyclerListener() {
        @Override
        public void onClickItem(int pos, GetDoctorAvailablity data) {
            iView.removeDoctorAvaailability(data.getId());
        }
    };


    @Override
    public void getDoctorAvailability(Integer id) {
        iView.showProgressbar();

        new DoctorAvailabilityModel(new IModelListListener<GetDoctorAvailablity>() {
            @Override
            public void onSuccessfulApi(@NotNull GetDoctorAvailablity response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<GetDoctorAvailablity> response) {
                iView.dismissProgressbar();
                iView.setAdapter(new DoctorAvailabilityAdapter(response,iCityListRecyclerAdapter));

            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getDoctorAvailablity(id);

    }

    @Override
    public void removeDoctorAvailability(Integer id) {
        iView.showProgressbar();

        new RemoveDoctorAvailablityModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.getRemoveResponse();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).removeDoctorAvailability(id);
    }
}
