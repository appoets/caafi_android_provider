package com.caafisom.android.provider.model.dto.common;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class DoctorSpecialityResponse extends BaseResponse {
    @SerializedName("data")
    @Expose
    private List<ProviderSpeciality> data = null;

    public List<ProviderSpeciality> getData() {
        return data;
    }

    public void setData(List<ProviderSpeciality> data) {
        this.data = data;
    }


}
