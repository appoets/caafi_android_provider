package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.AvailabilityListModel;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.model.listener.IModelListListener;
import com.caafisom.android.provider.presenter.ipresenter.IAvailabilityListPresenter;
import com.caafisom.android.provider.view.iview.IAvailabityListView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

public class AvailabilityListPresenter extends BasePresenter<IAvailabityListView> implements IAvailabilityListPresenter {


    public AvailabilityListPresenter(IAvailabityListView iView, String date) {
        super(iView);
        getAvailablity(date);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }


    @Override
    public void getAvailablity(String date) {
        iView.showProgressbar();
        new AvailabilityListModel(new IModelListListener<AvailabilityListResponse>() {
        
            @Override
            public void onSuccessfulApi(AvailabilityListResponse response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<AvailabilityListResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getAvailablity(date);
    }

    @Override
    public void postAvailablity(HashMap<String, Object> obj) {

        iView.showProgressbar();
        new AvailabilityListModel(new IModelListListener<AvailabilityListResponse>() {

            @Override
            public void onSuccessfulApi(AvailabilityListResponse response) {
                iView.dismissProgressbar();
            }

            @Override
            public void onSuccessfulApi(@NotNull List<AvailabilityListResponse> response) {
                iView.dismissProgressbar();
                iView.setAdapter(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postAvailablity(obj);

    }
}
