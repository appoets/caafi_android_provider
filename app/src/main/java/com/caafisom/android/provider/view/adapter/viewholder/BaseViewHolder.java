package com.caafisom.android.provider.view.adapter.viewholder;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;

import butterknife.ButterKnife;

public abstract class BaseViewHolder<D, L> extends RecyclerView.ViewHolder implements View.OnClickListener {

    protected D data;
    protected L listener;
    protected String TAG = getClass().getSimpleName();

    public BaseViewHolder(View itemView, L listener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.listener = listener;
    }

    public void setData(D data) {
        this.data = data;
        itemView.setOnClickListener(this);
        populateData(data);
    }

    abstract void populateData(D data);

    @Override
    public void onClick(View view) {
    }

    public void showImage(ImageView imgView,String url) {
        Glide.with(itemView.getContext()).load(url).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(imgView);

    }


}