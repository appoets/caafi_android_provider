package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IForgotPasswordPresenter;

public interface IForgotPasswordView extends IView<IForgotPasswordPresenter> {
        void goToOneTimePassword();
}
