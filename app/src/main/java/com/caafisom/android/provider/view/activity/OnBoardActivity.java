package com.caafisom.android.provider.view.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.WalkThrough;
import com.caafisom.android.provider.presenter.OnBoardPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IOnBoardPresenter;
import com.caafisom.android.provider.view.iview.IOnBoardView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class OnBoardActivity extends BaseActivity<IOnBoardPresenter> implements IOnBoardView,
        ViewPager.OnPageChangeListener {

    private MyViewPagerAdapter adapter;
    @BindView(R.id.vpOnBoard)
    ViewPager vpOnBoard;
    @BindView(R.id.layoutDots)
    LinearLayout layoutDots;
    private int dotsCount;
    private ImageView[] dots;

    @Override
    int attachLayout() {
        return R.layout.activity_onboard;
    }

    @Override
    IOnBoardPresenter initialize() {
        return new OnBoardPresenter(this);
    }

    @Override
    public void initSetUp() {
        ButterKnife.bind(this);
        List<WalkThrough> list = new ArrayList<>();
        list.add(new WalkThrough(R.drawable.slide_1,
                getString(R.string.walk_1), getString(R.string.walk_1_description)));
        list.add(new WalkThrough(R.drawable.slide_2,
                getString(R.string.walk_2), getString(R.string.walk_2_description)));
        list.add(new WalkThrough(R.drawable.slide_3,
                getString(R.string.walk_3), getString(R.string.walk_3_description)));
        adapter = new MyViewPagerAdapter(this, list);
        vpOnBoard.setAdapter(adapter);
        vpOnBoard.setCurrentItem(0);
        vpOnBoard.addOnPageChangeListener(this);
        addBottomDots();
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }

    @Override
    public void onPageSelected(int position) {
        for (int i = 0; i < dotsCount; i++)
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_unselected));
        dots[position].setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_selected));
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }

    private void addBottomDots() {
        dotsCount = adapter.getCount();
        dots = new ImageView[dotsCount];
        if (dotsCount == 0)
            return;
        layoutDots.removeAllViews();
        for (int i = 0; i < dotsCount; i++) {
            dots[i] = new ImageView(this);
            dots[i].setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_unselected));
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(4, 4, 4, 4);
            layoutDots.addView(dots[i], params);
        }
        dots[0].setImageDrawable(getResources().getDrawable(R.drawable.ic_dot_selected));
    }

    @OnClick({R.id.btnGetStarted})
    public void onViewClicked(View view) {
        if (view.getId() == R.id.btnGetStarted) {
            gotoLogin();
        }
    }

    @Override
    public void gotoLogin() {
        navigateTo(LoginActivity.class, true, new Bundle());
    }

    public static class MyViewPagerAdapter extends PagerAdapter {
        List<WalkThrough> list;
        Context context;

        MyViewPagerAdapter(Context context, List<WalkThrough> list) {
            this.list = list;
            this.context = context;
        }

        @NonNull
        @Override
        public Object instantiateItem(@NonNull ViewGroup container, int position) {
            View itemView = LayoutInflater.from(container.getContext()).inflate(R.layout.pager_item, container, false);
            WalkThrough walk = list.get(position);
            TextView title = itemView.findViewById(R.id.title);
            TextView description = itemView.findViewById(R.id.description);
            ImageView imageView = itemView.findViewById(R.id.img_pager_item);
            title.setText(walk.title);
            description.setText(walk.description);
//            Glide.with(context).load(walk.drawable).into(imageView);
            imageView.setImageResource(walk.drawable);
            container.addView(itemView);
            return itemView;
        }

        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public boolean isViewFromObject(@NonNull View view, @NonNull Object obj) {
            return view == obj;
        }

        @Override
        public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
            View view = (View) object;
            container.removeView(view);
        }
    }
}