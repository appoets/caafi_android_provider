package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.DoctorListModel;
import com.caafisom.android.provider.model.DoctorSpecialityModel;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorPresenter;
import com.caafisom.android.provider.view.adapter.CityListRecyclerAdater;
import com.caafisom.android.provider.view.adapter.DoctorListRecyclerAdater;
import com.caafisom.android.provider.view.adapter.listener.ICityListRecyclerAdapter;
import com.caafisom.android.provider.view.adapter.listener.IDoctorListRecyclerAdapter;
import com.caafisom.android.provider.view.iview.IDoctorView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class DoctorPresenter extends BasePresenter<IDoctorView> implements IDoctorPresenter {

    public DoctorPresenter(IDoctorView iView) {
        super(iView);
        getDoctorsList();

    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
        getDoctorsList();
    }

    IDoctorListRecyclerAdapter iDoctorListRecyclerAdapter = new IDoctorListRecyclerAdapter() {
        @Override
        public void onClickItem(int pos, DoctorList data) {
            iView.getDoctor(data);
        }
    };

    @Override
    public void getDoctorsList() {
        iView.showProgressbar();
        new DoctorListModel(new IModelListener<DoctorListResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull DoctorListResponse response) {
                iView.dismissProgressbar();
                iView.setAdapter(new DoctorListRecyclerAdater(response.getData(),iDoctorListRecyclerAdapter));

            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).getDoctorsList();
    }






}
