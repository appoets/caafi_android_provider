package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IForgotChangePasswordPresenter;

public interface IForgotChangePasswordView extends IView<IForgotChangePasswordPresenter> {
                void goToLogin();
}
