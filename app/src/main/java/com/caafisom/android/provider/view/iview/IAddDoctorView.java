package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorPresenter;


public interface IAddDoctorView extends IView<IAddDoctorPresenter> {
    void initSetUp();
    void successs();
    void getDoctorSpeciality(DoctorSpecialityResponse response);
}
