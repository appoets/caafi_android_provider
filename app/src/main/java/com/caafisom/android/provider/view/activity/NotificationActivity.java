package com.caafisom.android.provider.view.activity;

import android.annotation.SuppressLint;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.LinearLayout;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.response.Provider;
import com.caafisom.android.provider.presenter.NotificationPresenter;
import com.caafisom.android.provider.presenter.ipresenter.INotificationPresenter;
import com.caafisom.android.provider.view.adapter.NotificationRecyclerAdapter;
import com.caafisom.android.provider.view.fragment.ScheduleDialogFragment;
import com.caafisom.android.provider.view.iview.INotificationView;

import butterknife.BindView;
import butterknife.OnClick;

public class NotificationActivity extends BaseActivity<INotificationPresenter> implements INotificationView {

    @BindView(R.id.rcvNotification)
    RecyclerView rcvNotification;

    @BindView(R.id.llNoNotification)
    LinearLayout llNoNotification;


    @Override
    int attachLayout() {
        return R.layout.activity_notification;
    }

    @Override
    INotificationPresenter initialize() {
        return new NotificationPresenter(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        iPresenter.getNotification();
    }

    @Override
    public void bindPresenter(INotificationPresenter iPresenter) {
        super.bindPresenter(iPresenter);
    }

    @Override
    public void initSetUp() {
        rcvNotification.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvNotification.setItemAnimator(new DefaultItemAnimator());
    }

    @OnClick({R.id.ibBack})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
        }
    }

    @Override
    public void setAdapter(NotificationRecyclerAdapter adapter) {
        if (adapter.getItemCount() > 0) {
            llNoNotification.setVisibility(View.GONE);
            rcvNotification.setVisibility(View.VISIBLE);
            rcvNotification.setAdapter(adapter);
        } else {
            rcvNotification.setVisibility(View.GONE);
            llNoNotification.setVisibility(View.VISIBLE);
        }
    }

    @SuppressLint("CommitTransaction")
    @Override
    public void showNotificationDialog(int pos, Provider data) {

        Fragment fragment = getSupportFragmentManager().findFragmentByTag("Dialog");
        if (fragment!=null){
            getSupportFragmentManager().beginTransaction().remove(fragment);
        }
        ScheduleDialogFragment.newInstance(data).show(getSupportFragmentManager(),"Dialog");
    }
}
