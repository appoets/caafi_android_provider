package com.caafisom.android.provider.model.dto.response;

import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.Device;
import com.caafisom.android.provider.model.dto.common.Service;

import java.util.List;


public class LoginResponse extends BaseResponse{

	@SerializedName("latitude")
	private double latitude;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("rating")
	private String rating;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("description")
	private String description;

	@SerializedName("otp")
	private int otp;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("rating_count")
	private int ratingCount;

	@SerializedName("access_token")
	private String accessToken;

	@SerializedName("service")
//	private Service service;
	private List<Service> service;

	@SerializedName("currency")
	private String currency;

	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("device")
	private Device device;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	@SerializedName("longitude")
	private double longitude;

	public void setLatitude(int latitude){
		this.latitude = latitude;
	}

	public double getLatitude(){
		return latitude;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setRatingCount(int ratingCount){
		this.ratingCount = ratingCount;
	}

	public int getRatingCount(){
		return ratingCount;
	}

	public void setAccessToken(String accessToken){
		this.accessToken = accessToken;
	}

	public String getAccessToken(){
		return accessToken;
	}

	/*public void setService(Service service){
		this.service = service;
	}

	public Service getService(){
		return service;
	}*/

	public List<Service> getService() {
		return service;
	}

	public void setService(List<Service> service) {
		this.service = service;
	}

	public void setCurrency(String currency){
		this.currency = currency;
	}

	public String getCurrency(){
		return currency;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setDevice(Device device){
		this.device = device;
	}

	public Device getDevice(){
		return device;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setLongitude(int longitude){
		this.longitude = longitude;
	}

	public double getLongitude(){
		return longitude;
	}
}