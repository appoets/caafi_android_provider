package com.caafisom.android.provider.presenter.ipresenter;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationPresenter extends IPresenter {
    void getNotification();
}
