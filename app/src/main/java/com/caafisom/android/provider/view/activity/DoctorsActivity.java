package com.caafisom.android.provider.view.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.presenter.DoctorPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorPresenter;
import com.caafisom.android.provider.view.adapter.DoctorListRecyclerAdater;
import com.caafisom.android.provider.view.iview.IDoctorView;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;

public class DoctorsActivity extends BaseActivity<IDoctorPresenter> implements IDoctorView {

    @BindView(R.id.tv_add)
    TextView tvadd;
    @BindView(R.id.rcvDoctors)
    RecyclerView rcvDoctors;
    Context context;

    @Override
    int attachLayout() {
        return R.layout.activity_doctors;
    }

    @Override
    IDoctorPresenter initialize() {
        return new DoctorPresenter(this);
    }

    @OnClick({R.id.tv_add, R.id.ibBack})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.tv_add:
                Intent intent = new Intent(this, AddDoctorActivity.class);
                startActivity(intent);
                finish();
                break;
            case R.id.ibBack:
                finish();
                break;

        }

    }


    @Override
    public void getDoctor(DoctorList data) {
//        Toast.makeText(this, "Selected " + data.getFirstName(), Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(DoctorsActivity.this, DoctorEditActivity.class);
        intent.putExtra("first_name", data.getFirstName());
        intent.putExtra("last_name", data.getLastName());
        intent.putExtra("email", data.getEmail());
        intent.putExtra("consultation_fee", data.getConsultationFee());
        intent.putExtra("experience", data.getExperience());
        intent.putExtra("doctor_ID", data.getId());
        intent.putExtra("avatar", data.getPicture());
        intent.putExtra("speciality", data.getSpeciality().getName());
        intent.putExtra("speciality_ID", data.getSpeciality().getId());

        startActivity(intent);
        finish();


    }

    @Override
    public void setAdapter(DoctorListRecyclerAdater adapter) {
        if (adapter.getItemCount() > 0) {
            rcvDoctors.setVisibility(View.VISIBLE);
            rcvDoctors.setAdapter(adapter);
        } else {
            rcvDoctors.setVisibility(View.GONE);
        }
    }

    @Override
    public void initSetUp() {
        rcvDoctors.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvDoctors.setItemAnimator(new DefaultItemAnimator());
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }

}