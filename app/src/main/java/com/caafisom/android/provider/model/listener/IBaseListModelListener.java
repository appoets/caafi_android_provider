package com.caafisom.android.provider.model.listener;

import com.caafisom.android.provider.model.CustomException;

import java.util.List;

public interface IBaseListModelListener<BML> {

    void onSuccessfulApi(BML response);

    void onSuccessfulApi(List<BML> response);

    void onFailureApi(CustomException e);

    void onUnauthorizedUser(CustomException e);

    void onNetworkFailure();

}
