package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.adapter.DoctorAvailabilityAdapter;

public interface IDoctorAvailabilityView extends IView<IDoctorAvailabilityPresenter> {
    void setAdapter(DoctorAvailabilityAdapter adapter);
    void initSetUp();
    void removeDoctorAvaailability(Integer doctorId);
    void getRemoveResponse();
}
