package com.caafisom.android.provider.presenter.ipresenter;

import java.util.HashMap;

public interface IAvailabilityListPresenter extends IPresenter {
    void getAvailablity(String date);
    void postAvailablity(HashMap<String, Object> obj);
}
