package com.caafisom.android.provider.presenter.ipresenter;

import com.caafisom.android.provider.model.dto.common.ServiceItem;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IProfilePresenter extends IPresenter {
    void getServiceData();
    void setSpecialitiesName(int specialities_id,List<ServiceItem> serviceItemList);
    void updateProfile(HashMap<String, RequestBody> params, MultipartBody.Part filePart, MultipartBody.Part filePart2);
    void goToChangePassword();
}