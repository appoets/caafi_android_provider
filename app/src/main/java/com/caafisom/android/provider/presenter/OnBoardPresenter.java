package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.presenter.ipresenter.IOnBoardPresenter;
import com.caafisom.android.provider.view.iview.IOnBoardView;

public class OnBoardPresenter extends BasePresenter<IOnBoardView> implements IOnBoardPresenter {

    public OnBoardPresenter(IOnBoardView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }

    @Override
    public void goToLogin() {
        iView.gotoLogin();
    }
}