package com.caafisom.android.provider.presenter;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.caafisom.android.provider.common.Constants;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.LoginModel;
import com.caafisom.android.provider.model.RegisterModel;
import com.caafisom.android.provider.model.ServiceModel;
import com.caafisom.android.provider.model.dto.request.LoginRequest;
import com.caafisom.android.provider.model.dto.request.RegisterRequest;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.LoginResponse;
import com.caafisom.android.provider.model.dto.response.RegisterResponse;
import com.caafisom.android.provider.model.dto.response.ServiceResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IRegisterPresenter;
import com.caafisom.android.provider.view.iview.IRegisterView;

import org.jetbrains.annotations.NotNull;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;

import java.util.List;


public class RegisterPresenter extends BasePresenter<IRegisterView> implements IRegisterPresenter {

    @Override
    public void goToLogin() {
        iView.goToLogin();
    }

    public RegisterPresenter(IRegisterView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getServiceData();
    }

    @Override
    public void getServiceData() {
        new ServiceModel(new IModelListener<ServiceResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull ServiceResponse response) {
                iView.dismissProgressbar();
                iView.setUpData(response.getService());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getServiceDetails();
    }


    @Override
    public void postRegister(RegisterRequest registerRequest) {
        iView.showProgressbar();
        new RegisterModel(new IModelListener<RegisterResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull RegisterResponse response) {
                LoginRequest request = new LoginRequest();
              /*  request.setEmail(String.valueOf(registerRequest.get("email")));
                request.setPassword(String.valueOf(registerRequest.get("password")));
                request.setDevice_id(String.valueOf(registerRequest.get("device_id")));
                request.setDevice_token(String.valueOf(registerRequest.get("device_token")));*/


                request.setEmail(String.valueOf(registerRequest.getEmail()));
                request.setPassword(String.valueOf(registerRequest.getPassword()));
                request.setDevice_id(String.valueOf(registerRequest.getDevice_id()));
                request.setDevice_token(String.valueOf(registerRequest.getDevice_token()));

                request.setDevice_type(Constants.WebConstants.DEVICE_TYPE);
                postLogin(request);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postRegister(registerRequest);
    }

    @Override
    public void postLogin(LoginRequest request) {
        new LoginModel(new IModelListener<LoginResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NonNull LoginResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setAccessToken(response.getAccessToken());
                iView.goToHome();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).postLogin(request);
    }
}
