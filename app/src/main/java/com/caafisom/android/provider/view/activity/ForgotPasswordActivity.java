package com.caafisom.android.provider.view.activity;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.presenter.ForgotPasswordPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IForgotPasswordPresenter;
import com.caafisom.android.provider.view.iview.IForgotPasswordView;

import butterknife.BindView;
import butterknife.OnClick;

public class ForgotPasswordActivity extends BaseActivity<IForgotPasswordPresenter> implements IForgotPasswordView {

    @BindView(R.id.btnConfirm)
    Button btnConfirm;

    @BindView(R.id.etEmail)
    EditText etEmail;

    @Override
    int attachLayout() {
        return R.layout.activity_forget_password;
    }

    @Override
    IForgotPasswordPresenter initialize() {
        return new ForgotPasswordPresenter(this);
    }

    @OnClick({R.id.btnConfirm, R.id.ivBack})
    public void OnClickView(View view) {
        switch (view.getId()) {

            case R.id.btnConfirm:
                validateForgotPassword();
                break;

            case R.id.ivBack:
                onBackPressed();
                break;

        }
    }

    private void validateForgotPassword() {
        String email = etEmail.getText().toString().trim();
        if (TextUtils.isEmpty(email)) {
            showSnackBar(getString(R.string.please_enter_email));
        } else if (!getCodeSnippet().isEmailValid(email)) {
            showSnackBar(getString(R.string.please_enter_valid_email));
        } else {
            iPresenter.getOTPDetails(email);
        }
    }

    @Override
    public void goToOneTimePassword() {
       navigateTo(OneTimePasswordActivity.class,true,new Bundle());
    }
}
