package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.response.ServiceResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

public class ServiceModel extends BaseModel<ServiceResponse> {

    public ServiceModel(IModelListener<ServiceResponse> listener) {
        super(listener);
    }

    public void getServiceDetails() {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getServiceDetails());
    }

    @Override
    public void onSuccessfulApi(ServiceResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
