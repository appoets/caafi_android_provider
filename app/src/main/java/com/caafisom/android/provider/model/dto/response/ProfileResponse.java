package com.caafisom.android.provider.model.dto.response;

import com.caafisom.android.provider.model.dto.common.ServiceType;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfileResponse extends BaseResponse {

    @SerializedName("latitude")
    private double latitude;

    @SerializedName("mobile")
    private String mobile;

    @SerializedName("rating")
    private String rating;

    @SerializedName("last_name")
    private String lastName;

    @SerializedName("description")
    private String description;

    @SerializedName("otp")
    private int otp;

    @SerializedName("avatar")
    private String avatar;

    @SerializedName("rating_count")
    private int ratingCount;

    @SerializedName("service")
    private Service service;

    @SerializedName("currency")
    private String currency;

    @SerializedName("id")
    private int id;

    @SerializedName("first_name")
    private String firstName;

    @SerializedName("email")
    private String email;

    @SerializedName("status")
    private String status;

    @SerializedName("longitude")
    private double longitude;

    @SerializedName("experience")
    private double experience;

    @SerializedName("qualification")
    private String qualification;

    @SerializedName("certificate")
    private String certificate;

    public double getExperience() {
        return experience;
    }

    public void setExperience(double experience) {
        this.experience = experience;
    }

    public String getQualification() {
        return qualification;
    }

    public void setQualification(String qualification) {
        this.qualification = qualification;
    }

    public void setLatitude(int latitude) {
        this.latitude = latitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getMobile() {
        return mobile;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public String getRating() {
        return rating;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public void setOtp(int otp) {
        this.otp = otp;
    }

    public int getOtp() {
        return otp;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setRatingCount(int ratingCount) {
        this.ratingCount = ratingCount;
    }

    public int getRatingCount() {
        return ratingCount;
    }

    public void setService(Service service) {
        this.service = service;
    }

    public Service getService() {
        return service;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public String getCurrency() {
        return currency;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getStatus() {
        return status;
    }

    public void setLongitude(int longitude) {
        this.longitude = longitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public String getCertificate() {
        return certificate;
    }

    public void setCertificate(String certificate) {
        this.certificate = certificate;
    }

    public class Service {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("provider_id")
        @Expose
        private Integer providerId;
        @SerializedName("service_type_id")
        @Expose
        private Integer serviceTypeId;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("service_number")
        @Expose
        private String serviceNumber;
        @SerializedName("service_model")
        @Expose
        private String serviceModel;
        @SerializedName("provider_price")
        @Expose
        private Integer providerPrice;
        @SerializedName("slot_time")
        @Expose
        private String slotTime;
        @SerializedName("service_type")
        @Expose
        private ServiceType serviceType;

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getProviderId() {
            return providerId;
        }

        public void setProviderId(Integer providerId) {
            this.providerId = providerId;
        }

        public Integer getServiceTypeId() {
            return serviceTypeId;
        }

        public void setServiceTypeId(Integer serviceTypeId) {
            this.serviceTypeId = serviceTypeId;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getServiceNumber() {
            return serviceNumber;
        }

        public void setServiceNumber(String serviceNumber) {
            this.serviceNumber = serviceNumber;
        }

        public String getServiceModel() {
            return serviceModel;
        }

        public void setServiceModel(String serviceModel) {
            this.serviceModel = serviceModel;
        }

        public Integer getProviderPrice() {
            return providerPrice;
        }

        public void setProviderPrice(Integer providerPrice) {
            this.providerPrice = providerPrice;
        }

        public String getSlotTime() {
            return slotTime;
        }

        public void setSlotTime(String slotTime) {
            this.slotTime = slotTime;
        }

        public ServiceType getServiceType() {
            return serviceType;
        }

        public void setServiceType(ServiceType serviceType) {
            this.serviceType = serviceType;
        }

    }
}