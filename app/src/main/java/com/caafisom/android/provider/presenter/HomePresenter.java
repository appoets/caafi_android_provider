package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.google.firebase.iid.FirebaseInstanceId;
import com.caafisom.android.provider.model.AvailabilityModel;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.DeviceTokenModel;
import com.caafisom.android.provider.model.InvoiceModel;
import com.caafisom.android.provider.model.LocationUpdateModel;
import com.caafisom.android.provider.model.NotificationModel;
import com.caafisom.android.provider.model.ProfileModel;
import com.caafisom.android.provider.model.RatingModel;
import com.caafisom.android.provider.model.dto.request.AvailabilityRequest;
import com.caafisom.android.provider.model.dto.request.LocationRequest;
import com.caafisom.android.provider.model.dto.request.RatingRequest;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.InvoiceResponse;
import com.caafisom.android.provider.model.dto.response.NotificationResponse;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IHomePresenter;
import com.caafisom.android.provider.view.iview.IHomeView;

import org.jetbrains.annotations.NotNull;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;

import java.util.List;


public class HomePresenter extends BasePresenter<IHomeView> implements IHomePresenter {

    public HomePresenter(IHomeView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.setUp();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void getUserDetails() {
        iView.showProgressbar();
        new ProfileModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setCurrency(response.getCurrency());
                getApplicationInstance().setUserID(response.getId());
                getApplicationInstance().setProviderPrice(response.getService().getProviderPrice()+"");
                iView.updateUserDetails(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).getUserDetails();
    }


    @Override
    public void updateDeviceToken() {
        String deviceToken = FirebaseInstanceId.getInstance().getToken();
        getApplicationInstance().setFCMToken(deviceToken);

        new DeviceTokenModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                //iView.showSnackBar("Device Token Updated");
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
                iView.showNetworkMessage();
            }
        }).updateToken(deviceToken);
    }

    @Override
    public void goToHelp() {
        iView.goToHelp();
    }

    @Override
    public void changeAvailability(AvailabilityRequest request) {
        iView.showProgressbar();
        new AvailabilityModel(new IModelListener<ProfileResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull ProfileResponse response) {
                iView.dismissProgressbar();
                getApplicationInstance().setCurrency(response.getCurrency());
                iView.updateUserDetails(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
                iView.dismissProgressbar();
            }
        }).postAvailability(request);
    }

    @Override
    public void goToHistory() {
        iView.goToHistory();
    }

    @Override
    public void gotoDoctors() {
        iView.goToDoctors();
    }


    @Override
    public void goToSchedule() {
        iView.goToSchedule();
    }

    @Override
    public void goToAvailability() {
        iView.goToAvailability();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        iView.onViewDestroy();
    }

    @Override
    public void updateLocation(LocationRequest request) {
        new LocationUpdateModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {

            }


            @Override
            public void onFailureApi(CustomException e) {
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
            }
        }).updateLocation(request);
    }

    @Override
    public void getInvoiceDetails(String id) {
        iView.showProgressbar();
        new InvoiceModel(new IModelListener<InvoiceResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull InvoiceResponse response) {
                iView.showInvoice(response);
                iView.dismissProgressbar();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
            }
        }).getInvoice(id);
    }

    @Override
    public void postRating(String id, RatingRequest request) {
        new RatingModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                    iView.successRating(response.getMessage());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.dismissProgressbar();
            }
        }).updateRating(id,request);
    }

    @Override
    public void moveToNotification() {
        iView.goToNotification();
    }

    @Override
    public void getNotificationCount() {
        new NotificationModel(new IModelListener<NotificationResponse>() {

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull NotificationResponse response) {
                iView.dismissProgressbar();
                iView.updateNotification(response.getProvider().size());
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.updateNotification(0);
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.updateNotification(0);
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getMissedDetails();

    }
}
