package com.caafisom.android.provider.service;

import android.app.Service;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.Log;
import android.widget.Toast;

import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.response.CheckStatusResponse;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import timber.log.Timber;

public class CheckStatusService extends Service {

    private Handler mCheckStatusHandler = new Handler();
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            checkStatusCall();
            mCheckStatusHandler.postDelayed(runnable, 5000);
        }
    };

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        Timber.e("onStartCommand");
        mCheckStatusHandler.postDelayed(runnable, 5000);
        return START_STICKY;
    }

    private void checkStatusCall() {
        if (new CodeSnippet().hasNetwork()) {
            Call<CheckStatusResponse> call;
            call = new ApiClient().getClient().create(ApiInterface.class).getRequest();

            call.enqueue(new Callback<CheckStatusResponse>() {
                @Override
                public void onResponse(@NonNull Call<CheckStatusResponse> call, @NonNull Response<CheckStatusResponse> response) {
                    if (response.isSuccessful()) {
                        CheckStatusResponse tripResponse = response.body();
                        if (tripResponse != null) {
                            Intent intent = new Intent();
                            intent.setAction(CodeSnippet.USER_ACTION);
                            Bundle bundle = new Bundle();
                            bundle.putParcelable("response", tripResponse);
                            intent.putExtra("bundle", bundle);
                            sendBroadcast(intent.putExtra("response", tripResponse));
                        }
                    } else {
                        Toast.makeText(getApplicationContext(), response.message(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(@NonNull Call<CheckStatusResponse> call, @NonNull Throwable t) {
                    Toast.makeText(getApplicationContext(), t.toString(), Toast.LENGTH_SHORT).show();
                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "No Internet Connection", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mCheckStatusHandler.removeCallbacks(runnable);
        Log.e("TripService", "stopped");
    }


}
