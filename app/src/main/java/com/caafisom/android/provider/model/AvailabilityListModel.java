package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.model.listener.IModelListListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import java.util.HashMap;
import java.util.List;

public class AvailabilityListModel extends BaseListModel<AvailabilityListResponse> {

    public AvailabilityListModel(IModelListListener<AvailabilityListResponse> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(AvailabilityListResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<AvailabilityListResponse> response) {
        listener.onSuccessfulApi(response);
    }


    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getAvailablity(String date) {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getAvailablity(date));
    }

    public void postAvailablity(HashMap<String, Object> obj) {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).postAvailablity(obj));
    }
}
