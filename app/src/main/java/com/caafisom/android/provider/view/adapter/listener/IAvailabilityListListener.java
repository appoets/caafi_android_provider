package com.caafisom.android.provider.view.adapter.listener;


import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;

public interface IAvailabilityListListener extends BaseRecyclerListener<AvailabilityListResponse> {
}
