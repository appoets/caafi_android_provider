package com.caafisom.android.provider.view.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.CardView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Interpolator;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.daimajia.androidanimations.library.Techniques;
import com.daimajia.androidanimations.library.YoYo;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.Constants;
import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.request.AvailabilityRequest;
import com.caafisom.android.provider.model.dto.request.RatingRequest;
import com.caafisom.android.provider.model.dto.response.CheckStatusResponse;
import com.caafisom.android.provider.model.dto.response.InvoiceResponse;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;
import com.caafisom.android.provider.presenter.HomePresenter;
import com.caafisom.android.provider.presenter.ipresenter.IHomePresenter;
import com.caafisom.android.provider.service.CheckStatusService;
import com.caafisom.android.provider.view.iview.IHomeView;
import com.caafisom.android.provider.view.widget.CustomProgressbar;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResult;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.MessageFormat;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import retrofit2.Call;
import retrofit2.Callback;
import timber.log.Timber;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;
import static com.caafisom.android.provider.common.Constants.DoctorStatus.OFFLINE;
import static com.caafisom.android.provider.common.Constants.DoctorStatus.ONLINE;
import static com.caafisom.android.provider.common.Constants.Requests.REQUEST_LOCATION;
import static com.caafisom.android.provider.common.Constants.StoragePath.WEB_PATH;

public class HomeActivity extends BaseActivity<IHomePresenter> implements IHomeView,
        NavigationView.OnNavigationItemSelectedListener, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, OnMapReadyCallback, LocationListener {

    @BindView(R.id.nav_view)
    NavigationView navigationView;

    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;

    @BindView(R.id.lblOffline)
    TextView lblOffline;
    @BindView(R.id.lblOnline)
    TextView lblOnline;

    @BindView(R.id.crdOnboarding)
    CardView crdOnboarding;

    @BindView(R.id.viewLock)
    View viewLock;

    @BindView(R.id.imgNotifcation)
    ImageButton imgNotifcation;
    @BindView(R.id.tvNotificationCount)
    TextView tvNotificationCount;

    TextView tvName;
    CircleImageView civProfile;
    RelativeLayout rlHeaderMain;

    private GoogleMap mMap;
    private Marker mCtLocationMarker;
    GoogleApiClient mGoogleApiClient;

    private String CURRENT_MODE = ONLINE;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    private LocationRequest mLocationRequest;
    private Location ctLocation;

    @BindView(R.id.llBottomSheetInvoice)
    LinearLayout llBottomSheetInvoice;
    @BindView(R.id.rlBottomSheetRating)
    RelativeLayout rlBottomSheetRating;

    BottomSheetBehavior invoiceSheet, ratingSheet;

    @BindView(R.id.btn_submit)
    Button btn_submit;
    @BindView(R.id.btn_confirm)
    Button btn_confirm;
    @BindView(R.id.et_review)
    EditText et_review;
    @BindView(R.id.rb_doctor)
    RatingBar rb_doctor;

    @BindView(R.id.tv_invoice_id)
    TextView tv_invoice_id;
    @BindView(R.id.tv_base_rate)
    TextView tv_base_rate;
    @BindView(R.id.tv_pay_doctor)
    TextView tv_pay_doctor;
    @BindView(R.id.tv_ztoid_fee)
    TextView tv_ztoid_fee;
    @BindView(R.id.tv_promotion)
    TextView tv_promotion;
    @BindView(R.id.tv_balance_wallet)
    TextView tv_balance_wallet;
    @BindView(R.id.tv_tax)
    TextView tv_tax;
    @BindView(R.id.tv_total)
    TextView tv_total;
    @BindView(R.id.tv_amount_paid)
    TextView tv_amount_paid;
    @BindView(R.id.ivOffCircle)
    AppCompatTextView ivOffCircle;

    @BindView(R.id.statusLayout)
    LinearLayout statusLayout;
    @BindView(R.id.iv_profile)
    CircleImageView iv_profile;
    @BindView(R.id.tv_doctor_name)
    TextView tv_doctor_name;
    @BindView(R.id.tv_treatment)
    TextView tv_treatment;
    @BindView(R.id.tv_rate_txt)
    TextView tv_rate_txt;
    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvTime)
    TextView tvTime;
    @BindView(R.id.tv_desc_txt)
    TextView tv_desc_txt;

    @BindView(R.id.civInvoiceProfile)
    CircleImageView civInvoiceProfile;

    BroadcastReceiver mBroadcastReceiver;
    IntentFilter mIntentFilter;
    private MyReceiver myReceiver;
    boolean isFirstTime = true;
    public static Integer currentRequestID;

    public static String show_layout = "";

    @Override
    int attachLayout() {
        return R.layout.activity_home;
    }

    @Override
    IHomePresenter initialize() {
        return new HomePresenter(this);
    }

    String request_id = "", call_duration = "";

    @Override
    public void setUp() {
        setSupportActionBar(toolbar);

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);
        View header = navigationView.getHeaderView(0);
        tvName = header.findViewById(R.id.tvName);
        civProfile = header.findViewById(R.id.civProfile);
        rlHeaderMain = header.findViewById(R.id.rlHeaderMain);

        invoiceSheet = BottomSheetBehavior.from(llBottomSheetInvoice);
        ratingSheet = BottomSheetBehavior.from(rlBottomSheetRating);

        rlHeaderMain.setOnClickListener(v -> navigateTo(ProfileActivity.class, false, new Bundle()));

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        prepareLocation();
        mIntentFilter = new IntentFilter(Constants.MessageConstant.INVOICE);
        mBroadcastReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                request_id = intent.getExtras().getString(Constants.BundleConstants.REQUEST_ID);
                call_duration = intent.getExtras().getString(Constants.BundleConstants.CALL_DURATION);
                iPresenter.getInvoiceDetails(request_id);
            }
        };
        registerReceiver(mBroadcastReceiver, mIntentFilter);
    }

    private void playAnimation() {
        YoYo.with(Techniques.Tada)
                .duration(700)
                .repeat(3)
                .playOn(imgNotifcation);
    }

    private void prepareLocation() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (getPermissionUtils().hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Location Permission already granted
                buildGoogleApiClient();
                checkLocationEnableGPS();
            } else {
                //Request Location Permission
                checkLocationPermission();
            }
        } else {
            buildGoogleApiClient();
            checkLocationEnableGPS();
        }
    }

    private void checkLocationEnableGPS() {
        if (!checkLocationStatus()) {
            LocationRequest locationRequest = LocationRequest.create();
            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
            locationRequest.setInterval(30 * 1000);
            locationRequest.setFastestInterval(5 * 1000);
            LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest);
            builder.setAlwaysShow(true);
            PendingResult<LocationSettingsResult> result =
                    LocationServices.SettingsApi.checkLocationSettings(mGoogleApiClient, builder.build());
            result.setResultCallback(new ResultCallback<LocationSettingsResult>() {
                @Override
                public void onResult(LocationSettingsResult result) {
                    final Status status = result.getStatus();
                    if (status.getStatusCode() == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                        try {
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            status.startResolutionForResult(getActivity(), REQUEST_LOCATION);
                        } catch (IntentSender.SendIntentException e) {
                            e.printStackTrace();
                        }
                    }
                }
            });
        }
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
    }

    private void checkLocationPermission() {
        if (!getPermissionUtils().hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {
                new AlertDialog.Builder(this)
                        .setTitle(R.string.location_permission_needed)
                        .setMessage(R.string.location_permission_content)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                getPermissionUtils().requestPermissions(HomeActivity.this,
                                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                                        MY_PERMISSIONS_REQUEST_LOCATION);
                            }
                        })
                        .create()
                        .show();
            } else {
                getPermissionUtils().requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == MY_PERMISSIONS_REQUEST_LOCATION) {
            // If request is cancelled, the result arrays are empty.
            if (grantResults.length > 0
                    && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // permission was granted, yay! Do the
                // location-related task you need to do.
                if (getPermissionUtils().hasPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)) {

                    if (mGoogleApiClient == null) {
                        buildGoogleApiClient();
                    }
                    checkLocationEnableGPS();
                }
            } else {
                showSnackBar("Permission Denied");
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        HomeActivity.show_layout = "";
        iPresenter.getUserDetails();
        iPresenter.updateDeviceToken();
        iPresenter.getNotificationCount();
        if (!isMyServiceRunning(CheckStatusService.class)) {
            Intent intent = new Intent(this, CheckStatusService.class);
            startService(intent);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.getUiSettings().setMapToolbarEnabled(false);
        try {
            googleMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(getApplicationContext(), R.raw.style_json));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        switch (id) {
            case R.id.nav_home:
                break;
            case R.id.nav_doctors:
                iPresenter.gotoDoctors();
                break;
            case R.id.nav_history:
                iPresenter.goToHistory();
                break;
            case R.id.nav_schedule:
                iPresenter.goToSchedule();
                break;
            case R.id.nav_availability:
                iPresenter.goToAvailability();
                break;
            case R.id.nav_help:
                iPresenter.goToHelp();
                break;
            case R.id.nav_logout:
                iPresenter.onLogout();
                break;
        }
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @OnClick({R.id.statusLayout, R.id.lblOnline, R.id.lblOffline, R.id.viewLock, R.id.btn_confirm, R.id.imgNotifcation, R.id.btn_submit, R.id.btnAccept, R.id.btnReject})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.statusLayout:
                break;
            case R.id.lblOnline:
                statusChanger(ONLINE);
                callAvailability(ONLINE);
                if (!isMyServiceRunning(CheckStatusService.class)) {
                    Intent intent = new Intent(this, CheckStatusService.class);
                    startService(intent);
                }
                break;
            case R.id.lblOffline:
                statusChanger(OFFLINE);
                callAvailability(OFFLINE);
                if (isMyServiceRunning(CheckStatusService.class)) {
                    stopService(new Intent(HomeActivity.this, CheckStatusService.class));
                }
                break;
            case R.id.btn_confirm:
                invoiceSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
                ratingSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                break;

            case R.id.imgNotifcation:
                iPresenter.moveToNotification();
                break;
            case R.id.btn_submit:
                String comment = et_review.getText().toString().trim();
                if (TextUtils.isEmpty(comment)) {
                    showSnackBar("Please enter your review");
                } else {
                    String rating = (int) rb_doctor.getRating() + "";

                    RatingRequest request = new RatingRequest();
                    request.setCommand(comment);
                    request.setRating(rating);
                    iPresenter.postRating(request_id, request);
                }
                break;
            case R.id.btnAccept:
                acceptORrejectAPI("accept");
                break;
            case R.id.btnReject:
                acceptORrejectAPI("destroy");
                break;
        }
    }

    private void acceptORrejectAPI(String status) {
        CustomProgressbar mCustomProgressbar = new CustomProgressbar(this);
        mCustomProgressbar.setCancelable(false);
        mCustomProgressbar.show();

        Call<Object> call = new ApiClient().getClient().create(ApiInterface.class).acceptORreject(currentRequestID, status);
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(@NonNull Call<Object> call, @NonNull retrofit2.Response<Object> response) {
                mCustomProgressbar.dismiss();
                if (response.isSuccessful()) {
                    try {
                        if (status.equalsIgnoreCase("accept"))
                            Toast.makeText(HomeActivity.this, "Request accepted sucessfully, Go to schedule and proceed further!", Toast.LENGTH_LONG).show();
                        else if (status.equalsIgnoreCase("destroy"))
                            Toast.makeText(HomeActivity.this, "Request Cancelled!", Toast.LENGTH_LONG).show();
                        Timber.e("Retrofit response" + response.body());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onFailure(@NonNull Call<Object> call, @NonNull Throwable t) {
                Timber.e("Retrofit NetworkError" + t.getMessage());
                mCustomProgressbar.dismiss();
            }
        });
    }

    private void callAvailability(String status) {
        AvailabilityRequest request = new AvailabilityRequest();
        request.setService_status(status);
        iPresenter.changeAvailability(request);
    }

    @Override
    public void updateUserDetails(ProfileResponse response) {
        if (response != null) {
            tvName.setText(response.getFirstName());
            String image_url = WEB_PATH + response.getAvatar();
            Glide.with(HomeActivity.this).load(image_url).apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user)).into(civProfile);
            if (response.getService() != null) {
                if (response.getService().getStatus().equals(ONLINE)) {
                    statusChanger(ONLINE);
                } else {
                    statusChanger(OFFLINE);
                }
            }
        }
    }

    private void statusChanger(String status) {
        if (status.equals(ONLINE)) {
            CURRENT_MODE = ONLINE;
            lblOnline.setBackground(getResources().getDrawable(R.drawable.status_button_bg));
            lblOffline.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            viewLock.setVisibility(View.GONE);
        } else {
            CURRENT_MODE = OFFLINE;
            lblOffline.setBackground(getResources().getDrawable(R.drawable.status_button_bg));
            lblOnline.setBackgroundColor(getResources().getColor(R.color.colorWhite));
            viewLock.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void goToHelp() {
        navigateTo(HelpActivity.class, false, new Bundle());
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        if (mGoogleApiClient != null) {
            mGoogleApiClient.reconnect();
        }
    }

    @Override
    public void goToHistory() {
        navigateTo(HistoryActivity.class, false, new Bundle());
    }

    @Override
    public void goToDoctors() {
        navigateTo(DoctorsActivity.class, false, new Bundle());

    }

    @Override
    public void goToSchedule() {
        navigateTo(ScheduledListActivity.class, false, new Bundle());
    }

    @Override
    public void goToAvailability() {
        navigateTo(AvailabilityActivity.class, false, new Bundle());
    }

    @Override
    public void onLocationChanged(Location location) {
        if (location != null) {
            ctLocation = location;
            changeMapLocation(location);
            if (isFirstTime) {
                isFirstTime = false;
                com.caafisom.android.provider.model.dto.request.LocationRequest request = new com.caafisom.android.provider.model.dto.request.LocationRequest();
                request.setLatitude(location.getLatitude() + "");
                request.setLongitude(location.getLongitude() + "");
                iPresenter.updateLocation(request);
            }
        }
    }

    private void changeMapLocation(Location location) {
        if (mMap != null) {
            LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
            float zoom = 15.0f;
            if (mCtLocationMarker == null) {
                mCtLocationMarker = mMap.addMarker(new MarkerOptions()
                        .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_location))
                        .anchor(0.5f, 0.5f)
                        .position(latLng));
            }
            animateMarker(mCtLocationMarker, location);
            boolean contains = mMap.getProjection()
                    .getVisibleRegion()
                    .latLngBounds
                    .contains(latLng);
            if (!contains) {
                mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(latLng, zoom));
            }
        }
    }

    public void animateMarker(final Marker marker, final Location location) {
        final Handler handler = new Handler();
        final long start = SystemClock.uptimeMillis();
        final LatLng startLatLng = marker.getPosition();
        final double startRotation = marker.getRotation();
        final long duration = 500;

        final Interpolator interpolator = new LinearInterpolator();

        handler.post(new Runnable() {
            @Override
            public void run() {
                long elapsed = SystemClock.uptimeMillis() - start;
                float t = interpolator.getInterpolation((float) elapsed
                        / duration);

                double lng = t * location.getLongitude() + (1 - t)
                        * startLatLng.longitude;
                double lat = t * location.getLatitude() + (1 - t)
                        * startLatLng.latitude;

                float rotation = (float) (t * location.getBearing() + (1 - t)
                        * startRotation);

                marker.setPosition(new LatLng(lat, lng));
                marker.setRotation(rotation);

                if (t < 1.0) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16);
                }
            }
        });
    }

    @Override
    public void showInvoice(InvoiceResponse response) {
        String timeFormat = new CodeSnippet().secToDisplayFormat(Long.parseLong(call_duration));

        ivOffCircle.setText(timeFormat);
        String base_rate = getApplicationInstance().getCurrency() + getApplicationInstance().getProviderPrice() + "/min";
        tv_base_rate.setText(base_rate);
        String to_doctor = getApplicationInstance().getCurrency() + response.getTimePrice();
        tv_pay_doctor.setText(to_doctor);
        String ztoid_fee = getApplicationInstance().getCurrency() + response.getCommision();
        tv_ztoid_fee.setText(ztoid_fee);
        String promotion = getApplicationInstance().getCurrency() + response.getDiscount();
        tv_promotion.setText(promotion);
        String wallet = getApplicationInstance().getCurrency() + response.getWallet();
        tv_balance_wallet.setText(wallet);
        String tax = getApplicationInstance().getCurrency() + response.getTax();
        tv_tax.setText(tax);
        String total = getApplicationInstance().getCurrency() + response.getTotal();
        tv_total.setText(total);
        tv_amount_paid.setText(total);

        tv_invoice_id.setText(MessageFormat.format("{0}{1}", getString(R.string.invoice_id), response.getId()));

        invoiceSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        ratingSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void onViewDestroy() {
        if (mGoogleApiClient != null)
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
        unregisterReceiver(mBroadcastReceiver);
    }

    @Override
    public void successRating(String message) {
        invoiceSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
        ratingSheet.setState(BottomSheetBehavior.STATE_COLLAPSED);
    }

    @Override
    public void goToNotification() {
        navigateTo(NotificationActivity.class, false, new Bundle());
    }

    @Override
    public void updateNotification(int count) {
        if (count > 0) {
            if (count > 99)
                tvNotificationCount.setText("99+");
            else
                tvNotificationCount.setText(count + "");
            tvNotificationCount.setVisibility(View.VISIBLE);
            playAnimation();
        } else {
            tvNotificationCount.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        registerReceiver(myReceiver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(myReceiver);
        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(HomeActivity.this, CheckStatusService.class));
        }
    }

    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {
                if (intent.getParcelableExtra("response") != null) {
                    response = intent.getParcelableExtra("response");
                    Log.e("onReceive HOME=>", response.toString());
                    if (response.getAccountStatus().equalsIgnoreCase("onboarding")) {
                        crdOnboarding.setVisibility(View.VISIBLE);
                    } else {
                        crdOnboarding.setVisibility(View.GONE);
                    }
                    if (response.getServiceStatus().equalsIgnoreCase("active") && response.getRequests() != null && response.getRequests().size() > 0) {
                        currentRequestID = response.getRequests().get(0).getRequestId();
                        if (!statusLayout.isShown()) {
                            if (show_layout.equalsIgnoreCase(""))
                                statusLayout.setVisibility(View.VISIBLE);
                            else
                                statusLayout.setVisibility(View.GONE);
                            Glide.with(HomeActivity.this).load(WEB_PATH + response.getRequests().get(0).getRequest().getUser().getPicture())
                                    .apply(new RequestOptions().centerCrop().placeholder(R.drawable.ic_dummy_user).error(R.drawable.ic_dummy_user))
                                    .into(iv_profile);
                            tv_doctor_name.setText(response.getRequests().get(0).getRequest().getUser().getFirstName() + " " + response.getRequests().get(0).getRequest().getUser().getLastName());
                            tv_treatment.setText(response.getRequests().get(0).getRequest().getServiceType().getName());
                            tv_rate_txt.setText("" + response.getRequests().get(0).getRequest().getServiceType().getFixed());
                            tvDate.setText(response.getRequests().get(0).getRequest().getScheduleAt().split(" ")[0]);
                            tvTime.setText(response.getRequests().get(0).getRequest().getScheduleAt().split(" ")[1]);
                            if (response.getRequests().get(0).getRequest().getServiceType().getDescription() != null) {
                                tv_desc_txt.setText(response.getRequests().get(0).getRequest().getServiceType().getDescription());
                            } else {
                                tv_desc_txt.setText("No comments");
                            }
                        }
                    } else {
                        statusLayout.setVisibility(View.GONE);
                    }
                    if (response.getRequests().size() <= 0) {
                        statusLayout.setVisibility(View.GONE);
                    }
                }
            }
        }
    }

    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }
}