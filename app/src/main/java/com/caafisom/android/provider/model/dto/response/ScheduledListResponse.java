package com.caafisom.android.provider.model.dto.response;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.ServiceType;

public class ScheduledListResponse extends BaseResponse implements Parcelable {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("booking_id")
    @Expose
    private String bookingId;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("provider_id")
    @Expose
    private Integer providerId;
    @SerializedName("current_provider_id")
    @Expose
    private Integer currentProviderId;
    @SerializedName("service_type_id")
    @Expose
    private Integer serviceTypeId;
    @SerializedName("before_image")
    @Expose
    private String beforeImage;
    @SerializedName("before_comment")
    @Expose
    private String beforeComment;
    @SerializedName("after_image")
    @Expose
    private String afterImage;
    @SerializedName("after_comment")
    @Expose
    private String afterComment;
    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("cancelled_by")
    @Expose
    private String cancelledBy;
    @SerializedName("payment_mode")
    @Expose
    private String paymentMode;
    @SerializedName("paid")
    @Expose
    private Integer paid;
    @SerializedName("distance")
    @Expose
    private Integer distance;
    @SerializedName("s_address")
    @Expose
    private String sAddress;
    @SerializedName("s_latitude")
    @Expose
    private Double sLatitude;
    @SerializedName("s_longitude")
    @Expose
    private Double sLongitude;
    @SerializedName("d_address")
    @Expose
    private String dAddress;
    @SerializedName("d_latitude")
    @Expose
    private Double dLatitude;
    @SerializedName("d_longitude")
    @Expose
    private Double dLongitude;
    @SerializedName("otp")
    @Expose
    private String otp;
    @SerializedName("assigned_at")
    @Expose
    private String assignedAt;
    @SerializedName("schedule_at")
    @Expose
    private String scheduleAt;
    @SerializedName("started_at")
    @Expose
    private String startedAt;
    @SerializedName("finished_at")
    @Expose
    private String finishedAt;
    @SerializedName("user_rated")
    @Expose
    private Integer userRated;
    @SerializedName("provider_rated")
    @Expose
    private Integer providerRated;
    @SerializedName("use_wallet")
    @Expose
    private Integer useWallet;
    @SerializedName("call_seconds")
    @Expose
    private Integer callSeconds;
    @SerializedName("broadcast")
    @Expose
    private Integer broadcast;
    @SerializedName("static_map")
    @Expose
    private String staticMap;
    @SerializedName("service_type")
    @Expose
    private ServiceType service_type;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("payment")
    @Expose
    private Payment payment;

    protected ScheduledListResponse(Parcel in) {
        if (in.readByte() == 0) {
            id = null;
        } else {
            id = in.readInt();
        }
        bookingId = in.readString();
        if (in.readByte() == 0) {
            userId = null;
        } else {
            userId = in.readInt();
        }
        if (in.readByte() == 0) {
            providerId = null;
        } else {
            providerId = in.readInt();
        }
        if (in.readByte() == 0) {
            currentProviderId = null;
        } else {
            currentProviderId = in.readInt();
        }
        if (in.readByte() == 0) {
            serviceTypeId = null;
        } else {
            serviceTypeId = in.readInt();
        }
        beforeImage = in.readString();
        beforeComment = in.readString();
        afterImage = in.readString();
        afterComment = in.readString();
        status = in.readString();
        cancelledBy = in.readString();
        paymentMode = in.readString();
        if (in.readByte() == 0) {
            paid = null;
        } else {
            paid = in.readInt();
        }
        if (in.readByte() == 0) {
            distance = null;
        } else {
            distance = in.readInt();
        }
        sAddress = in.readString();
        if (in.readByte() == 0) {
            sLatitude = null;
        } else {
            sLatitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            sLongitude = null;
        } else {
            sLongitude = in.readDouble();
        }
        dAddress = in.readString();
        if (in.readByte() == 0) {
            dLatitude = null;
        } else {
            dLatitude = in.readDouble();
        }
        if (in.readByte() == 0) {
            dLongitude = null;
        } else {
            dLongitude = in.readDouble();
        }
        otp = in.readString();
        assignedAt = in.readString();
        scheduleAt = in.readString();
        startedAt = in.readString();
        finishedAt = in.readString();
        if (in.readByte() == 0) {
            userRated = null;
        } else {
            userRated = in.readInt();
        }
        if (in.readByte() == 0) {
            providerRated = null;
        } else {
            providerRated = in.readInt();
        }
        if (in.readByte() == 0) {
            useWallet = null;
        } else {
            useWallet = in.readInt();
        }
        if (in.readByte() == 0) {
            callSeconds = null;
        } else {
            callSeconds = in.readInt();
        }
        if (in.readByte() == 0) {
            broadcast = null;
        } else {
            broadcast = in.readInt();
        }
        staticMap = in.readString();
        user = in.readParcelable(User.class.getClassLoader());
        payment = in.readParcelable(Payment.class.getClassLoader());
    }

    public static final Creator<ScheduledListResponse> CREATOR = new Creator<ScheduledListResponse>() {
        @Override
        public ScheduledListResponse createFromParcel(Parcel in) {
            return new ScheduledListResponse(in);
        }

        @Override
        public ScheduledListResponse[] newArray(int size) {
            return new ScheduledListResponse[size];
        }
    };

    public Payment getPayment() {
        return payment;
    }

    public void setPayment(Payment payment) {
        this.payment = payment;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getBookingId() {
        return bookingId;
    }

    public void setBookingId(String bookingId) {
        this.bookingId = bookingId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public Integer getProviderId() {
        return providerId;
    }

    public void setProviderId(Integer providerId) {
        this.providerId = providerId;
    }

    public Integer getCurrentProviderId() {
        return currentProviderId;
    }

    public void setCurrentProviderId(Integer currentProviderId) {
        this.currentProviderId = currentProviderId;
    }

    public Integer getServiceTypeId() {
        return serviceTypeId;
    }

    public void setServiceTypeId(Integer serviceTypeId) {
        this.serviceTypeId = serviceTypeId;
    }

    public String getBeforeImage() {
        return beforeImage;
    }

    public void setBeforeImage(String beforeImage) {
        this.beforeImage = beforeImage;
    }

    public String getBeforeComment() {
        return beforeComment;
    }

    public void setBeforeComment(String beforeComment) {
        this.beforeComment = beforeComment;
    }

    public String getAfterImage() {
        return afterImage;
    }

    public void setAfterImage(String afterImage) {
        this.afterImage = afterImage;
    }

    public String getAfterComment() {
        return afterComment;
    }

    public void setAfterComment(String afterComment) {
        this.afterComment = afterComment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCancelledBy() {
        return cancelledBy;
    }

    public void setCancelledBy(String cancelledBy) {
        this.cancelledBy = cancelledBy;
    }

    public String getPaymentMode() {
        return paymentMode;
    }

    public void setPaymentMode(String paymentMode) {
        this.paymentMode = paymentMode;
    }

    public Integer getPaid() {
        return paid;
    }

    public void setPaid(Integer paid) {
        this.paid = paid;
    }

    public Integer getDistance() {
        return distance;
    }

    public void setDistance(Integer distance) {
        this.distance = distance;
    }

    public String getSAddress() {
        return sAddress;
    }

    public void setSAddress(String sAddress) {
        this.sAddress = sAddress;
    }

    public Double getSLatitude() {
        return sLatitude;
    }

    public void setSLatitude(Double sLatitude) {
        this.sLatitude = sLatitude;
    }

    public Double getSLongitude() {
        return sLongitude;
    }

    public void setSLongitude(Double sLongitude) {
        this.sLongitude = sLongitude;
    }

    public String getDAddress() {
        return dAddress;
    }

    public void setDAddress(String dAddress) {
        this.dAddress = dAddress;
    }

    public Double getDLatitude() {
        return dLatitude;
    }

    public void setDLatitude(Double dLatitude) {
        this.dLatitude = dLatitude;
    }

    public Double getDLongitude() {
        return dLongitude;
    }

    public void setDLongitude(Double dLongitude) {
        this.dLongitude = dLongitude;
    }

    public String getOtp() {
        return otp;
    }

    public void setOtp(String otp) {
        this.otp = otp;
    }

    public String getAssignedAt() {
        return assignedAt;
    }

    public void setAssignedAt(String assignedAt) {
        this.assignedAt = assignedAt;
    }

    public String getScheduleAt() {
        return scheduleAt;
    }

    public void setScheduleAt(String scheduleAt) {
        this.scheduleAt = scheduleAt;
    }

    public String getStartedAt() {
        return startedAt;
    }

    public void setStartedAt(String startedAt) {
        this.startedAt = startedAt;
    }

    public String getFinishedAt() {
        return finishedAt;
    }

    public void setFinishedAt(String finishedAt) {
        this.finishedAt = finishedAt;
    }

    public Integer getUserRated() {
        return userRated;
    }

    public void setUserRated(Integer userRated) {
        this.userRated = userRated;
    }

    public Integer getProviderRated() {
        return providerRated;
    }

    public void setProviderRated(Integer providerRated) {
        this.providerRated = providerRated;
    }

    public Integer getUseWallet() {
        return useWallet;
    }

    public void setUseWallet(Integer useWallet) {
        this.useWallet = useWallet;
    }

    public Integer getCallSeconds() {
        return callSeconds;
    }

    public void setCallSeconds(Integer callSeconds) {
        this.callSeconds = callSeconds;
    }

    public Integer getBroadcast() {
        return broadcast;
    }

    public void setBroadcast(Integer broadcast) {
        this.broadcast = broadcast;
    }

    public String getStaticMap() {
        return staticMap;
    }

    public void setStaticMap(String staticMap) {
        this.staticMap = staticMap;
    }

    public ServiceType getService_type() {
        return service_type;
    }

    public void setService_type(ServiceType service_type) {
        this.service_type = service_type;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(id);
        }
        dest.writeString(bookingId);
        if (userId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userId);
        }
        if (providerId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(providerId);
        }
        if (currentProviderId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(currentProviderId);
        }
        if (serviceTypeId == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(serviceTypeId);
        }
        dest.writeString(beforeImage);
        dest.writeString(beforeComment);
        dest.writeString(afterImage);
        dest.writeString(afterComment);
        dest.writeString(status);
        dest.writeString(cancelledBy);
        dest.writeString(paymentMode);
        if (paid == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(paid);
        }
        if (distance == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(distance);
        }
        dest.writeString(sAddress);
        if (sLatitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(sLatitude);
        }
        if (sLongitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(sLongitude);
        }
        dest.writeString(dAddress);
        if (dLatitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(dLatitude);
        }
        if (dLongitude == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeDouble(dLongitude);
        }
        dest.writeString(otp);
        dest.writeString(assignedAt);
        dest.writeString(scheduleAt);
        dest.writeString(startedAt);
        dest.writeString(finishedAt);
        if (userRated == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(userRated);
        }
        if (providerRated == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(providerRated);
        }
        if (useWallet == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(useWallet);
        }
        if (callSeconds == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(callSeconds);
        }
        if (broadcast == null) {
            dest.writeByte((byte) 0);
        } else {
            dest.writeByte((byte) 1);
            dest.writeInt(broadcast);
        }
        dest.writeString(staticMap);
        dest.writeParcelable(user, flags);
        dest.writeParcelable(payment, flags);
    }

    public static class User implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("first_name")
        @Expose
        private String firstName;
        @SerializedName("last_name")
        @Expose
        private String lastName;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("picture")
        @Expose
        private String picture;
        @SerializedName("device_token")
        @Expose
        private String deviceToken;
        @SerializedName("device_id")
        @Expose
        private String deviceId;
        @SerializedName("device_type")
        @Expose
        private String deviceType;
        @SerializedName("login_by")
        @Expose
        private String loginBy;
        @SerializedName("social_unique_id")
        @Expose
        private String socialUniqueId;
        @SerializedName("mobile")
        @Expose
        private String mobile;
        @SerializedName("latitude")
        @Expose
        private String latitude;
        @SerializedName("longitude")
        @Expose
        private String longitude;
        @SerializedName("stripe_cust_id")
        @Expose
        private String stripeCustId;
        @SerializedName("wallet_balance")
        @Expose
        private Integer walletBalance;
        @SerializedName("rating")
        @Expose
        private String rating;
        @SerializedName("rating_count")
        @Expose
        private Integer ratingCount;
        @SerializedName("otp")
        @Expose
        private Integer otp;

        protected User(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            firstName = in.readString();
            lastName = in.readString();
            paymentMode = in.readString();
            email = in.readString();
            picture = in.readString();
            deviceToken = in.readString();
            deviceId = in.readString();
            deviceType = in.readString();
            loginBy = in.readString();
            socialUniqueId = in.readString();
            mobile = in.readString();
            latitude = in.readString();
            longitude = in.readString();
            stripeCustId = in.readString();
            if (in.readByte() == 0) {
                walletBalance = null;
            } else {
                walletBalance = in.readInt();
            }
            rating = in.readString();
            if (in.readByte() == 0) {
                ratingCount = null;
            } else {
                ratingCount = in.readInt();
            }
            if (in.readByte() == 0) {
                otp = null;
            } else {
                otp = in.readInt();
            }
        }

        public static final Creator<User> CREATOR = new Creator<User>() {
            @Override
            public User createFromParcel(Parcel in) {
                return new User(in);
            }

            @Override
            public User[] newArray(int size) {
                return new User[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPicture() {
            return picture;
        }

        public void setPicture(String picture) {
            this.picture = picture;
        }

        public String getDeviceToken() {
            return deviceToken;
        }

        public void setDeviceToken(String deviceToken) {
            this.deviceToken = deviceToken;
        }

        public String getDeviceId() {
            return deviceId;
        }

        public void setDeviceId(String deviceId) {
            this.deviceId = deviceId;
        }

        public String getDeviceType() {
            return deviceType;
        }

        public void setDeviceType(String deviceType) {
            this.deviceType = deviceType;
        }

        public String getLoginBy() {
            return loginBy;
        }

        public void setLoginBy(String loginBy) {
            this.loginBy = loginBy;
        }

        public String getSocialUniqueId() {
            return socialUniqueId;
        }

        public void setSocialUniqueId(String socialUniqueId) {
            this.socialUniqueId = socialUniqueId;
        }

        public String getMobile() {
            return mobile;
        }

        public void setMobile(String mobile) {
            this.mobile = mobile;
        }

        public String getLatitude() {
            return latitude;
        }

        public void setLatitude(String latitude) {
            this.latitude = latitude;
        }

        public String getLongitude() {
            return longitude;
        }

        public void setLongitude(String longitude) {
            this.longitude = longitude;
        }

        public String getStripeCustId() {
            return stripeCustId;
        }

        public void setStripeCustId(String stripeCustId) {
            this.stripeCustId = stripeCustId;
        }

        public Integer getWalletBalance() {
            return walletBalance;
        }

        public void setWalletBalance(Integer walletBalance) {
            this.walletBalance = walletBalance;
        }

        public String getRating() {
            return rating;
        }

        public void setRating(String rating) {
            this.rating = rating;
        }

        public Integer getRatingCount() {
            return ratingCount;
        }

        public void setRatingCount(Integer ratingCount) {
            this.ratingCount = ratingCount;
        }

        public Integer getOtp() {
            return otp;
        }

        public void setOtp(Integer otp) {
            this.otp = otp;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            dest.writeString(firstName);
            dest.writeString(lastName);
            dest.writeString(paymentMode);
            dest.writeString(email);
            dest.writeString(picture);
            dest.writeString(deviceToken);
            dest.writeString(deviceId);
            dest.writeString(deviceType);
            dest.writeString(loginBy);
            dest.writeString(socialUniqueId);
            dest.writeString(mobile);
            dest.writeString(latitude);
            dest.writeString(longitude);
            dest.writeString(stripeCustId);
            if (walletBalance == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(walletBalance);
            }
            dest.writeString(rating);
            if (ratingCount == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(ratingCount);
            }
            if (otp == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(otp);
            }
        }
    }

    public static class Payment implements Parcelable {

        @SerializedName("id")
        @Expose
        private Integer id;
        @SerializedName("request_id")
        @Expose
        private Integer requestId;
        @SerializedName("promocode_id")
        @Expose
        private Object promocodeId;
        @SerializedName("payment_id")
        @Expose
        private String paymentId;
        @SerializedName("payment_mode")
        @Expose
        private String paymentMode;
        @SerializedName("fixed")
        @Expose
        private Double fixed;
        @SerializedName("distance")
        @Expose
        private Integer distance;
        @SerializedName("commision")
        @Expose
        private Double commision;
        @SerializedName("time_price")
        @Expose
        private Double timePrice;
        @SerializedName("discount")
        @Expose
        private Double discount;
        @SerializedName("tax")
        @Expose
        private Double tax;
        @SerializedName("wallet")
        @Expose
        private Double wallet;
        @SerializedName("total")
        @Expose
        private Double total;

        protected Payment(Parcel in) {
            if (in.readByte() == 0) {
                id = null;
            } else {
                id = in.readInt();
            }
            if (in.readByte() == 0) {
                requestId = null;
            } else {
                requestId = in.readInt();
            }
            paymentId = in.readString();
            paymentMode = in.readString();
            if (in.readByte() == 0) {
                fixed = null;
            } else {
                fixed = in.readDouble();
            }
            if (in.readByte() == 0) {
                distance = null;
            } else {
                distance = in.readInt();
            }
            if (in.readByte() == 0) {
                commision = null;
            } else {
                commision = in.readDouble();
            }
            if (in.readByte() == 0) {
                timePrice = null;
            } else {
                timePrice = in.readDouble();
            }
            if (in.readByte() == 0) {
                discount = null;
            } else {
                discount = in.readDouble();
            }
            if (in.readByte() == 0) {
                tax = null;
            } else {
                tax = in.readDouble();
            }
            if (in.readByte() == 0) {
                wallet = null;
            } else {
                wallet = in.readDouble();
            }
            if (in.readByte() == 0) {
                total = null;
            } else {
                total = in.readDouble();
            }
        }

        public static final Creator<Payment> CREATOR = new Creator<Payment>() {
            @Override
            public Payment createFromParcel(Parcel in) {
                return new Payment(in);
            }

            @Override
            public Payment[] newArray(int size) {
                return new Payment[size];
            }
        };

        public Integer getId() {
            return id;
        }

        public void setId(Integer id) {
            this.id = id;
        }

        public Integer getRequestId() {
            return requestId;
        }

        public void setRequestId(Integer requestId) {
            this.requestId = requestId;
        }

        public Object getPromocodeId() {
            return promocodeId;
        }

        public void setPromocodeId(Object promocodeId) {
            this.promocodeId = promocodeId;
        }

        public String getPaymentId() {
            return paymentId;
        }

        public void setPaymentId(String paymentId) {
            this.paymentId = paymentId;
        }

        public String getPaymentMode() {
            return paymentMode;
        }

        public void setPaymentMode(String paymentMode) {
            this.paymentMode = paymentMode;
        }

        public Double getFixed() {
            return fixed;
        }

        public void setFixed(Double fixed) {
            this.fixed = fixed;
        }

        public Integer getDistance() {
            return distance;
        }

        public void setDistance(Integer distance) {
            this.distance = distance;
        }

        public Double getCommision() {
            return commision;
        }

        public void setCommision(Double commision) {
            this.commision = commision;
        }

        public Double getTimePrice() {
            return timePrice;
        }

        public void setTimePrice(Double timePrice) {
            this.timePrice = timePrice;
        }

        public Double getDiscount() {
            return discount;
        }

        public void setDiscount(Double discount) {
            this.discount = discount;
        }

        public Double getTax() {
            return tax;
        }

        public void setTax(Double tax) {
            this.tax = tax;
        }

        public Double getWallet() {
            return wallet;
        }

        public void setWallet(Double wallet) {
            this.wallet = wallet;
        }

        public Double getTotal() {
            return total;
        }

        public void setTotal(Double total) {
            this.total = total;
        }

        @Override
        public int describeContents() {
            return 0;
        }

        @Override
        public void writeToParcel(Parcel dest, int flags) {
            if (id == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(id);
            }
            if (requestId == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(requestId);
            }
            dest.writeString(paymentId);
            dest.writeString(paymentMode);
            if (fixed == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(fixed);
            }
            if (distance == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeInt(distance);
            }
            if (commision == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(commision);
            }
            if (timePrice == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(timePrice);
            }
            if (discount == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(discount);
            }
            if (tax == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(tax);
            }
            if (wallet == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(wallet);
            }
            if (total == null) {
                dest.writeByte((byte) 0);
            } else {
                dest.writeByte((byte) 1);
                dest.writeDouble(total);
            }
        }
    }

}
