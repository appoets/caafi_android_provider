package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.ISplashPresenter;

public interface ISplashView extends IView<ISplashPresenter> {

    void startTimer(int splashTimer);

    void gotoLogin();

    void gotoHome();

}
