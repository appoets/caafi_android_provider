package com.caafisom.android.provider.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.view.adapter.listener.IHistoryRecyclerListener;
import com.caafisom.android.provider.view.adapter.viewholder.HistoryViewHolder;

import java.util.List;

public class HistoryAdapter extends BaseRecyclerAdapter<IHistoryRecyclerListener,HistoryItem,HistoryViewHolder> {


    IHistoryRecyclerListener iHistoryRecyclerListener;

    public HistoryAdapter(List<HistoryItem> data, IHistoryRecyclerListener iHistoryRecyclerListener) {
        super(data, iHistoryRecyclerListener);
        this.iHistoryRecyclerListener = iHistoryRecyclerListener;
    }

    @NonNull
    @Override
    public HistoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_history, parent, false);
        return new HistoryViewHolder(view, iHistoryRecyclerListener);
    }
}
