package com.caafisom.android.provider.view.adapter.listener;


import com.caafisom.android.provider.model.dto.common.CityList;

/**
 * Created by Tranxit Technologies.
 */

public interface ICityListRecyclerAdapter extends BaseRecyclerListener<CityList> {
}
