package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.AddDoctorAvailabilityModel;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.listener.IModelListListener;
import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.iview.IAddDoctorAvailabilityView;

import org.jetbrains.annotations.NotNull;

import java.util.List;


public class DoctorAvailabilityAddPresenter extends BasePresenter<IAddDoctorAvailabilityView> implements IAddDoctorAvailabilityPresenter {

    public DoctorAvailabilityAddPresenter(IAddDoctorAvailabilityView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
    }


    @Override
    public void addDoctorAvailability(Integer id, String selectDate) {
        iView.showProgressbar();

       new AddDoctorAvailabilityModel(new IModelListListener<GetDoctorAvailablity>() {
           @Override
           public void onSuccessfulApi(@NotNull GetDoctorAvailablity response) {
               iView.dismissProgressbar();
           }

           @Override
           public void onSuccessfulApi(@NotNull List<GetDoctorAvailablity> response) {
               iView.dismissProgressbar();
               iView.getSuccessResponse();
           }

           @Override
           public void onFailureApi(CustomException e) {
               iView.dismissProgressbar();
               iView.showSnackBar(e.getMessage());
           }

           @Override
           public void onUnauthorizedUser(CustomException e) {
               iView.dismissProgressbar();
               iView.showSnackBar(e.getMessage());
           }

           @Override
           public void onNetworkFailure() {
               iView.showNetworkMessage();
           }
       }).postDoctorAvailability(id,selectDate);

    }
}
