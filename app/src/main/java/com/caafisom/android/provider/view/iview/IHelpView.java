package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.response.HelpResponse;
import com.caafisom.android.provider.presenter.ipresenter.IHelpPresenter;

public interface IHelpView extends IView<IHelpPresenter> {
        void updateHelpDetails(HelpResponse response);
}
