package com.caafisom.android.provider.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.response.Provider;
import com.caafisom.android.provider.view.adapter.listener.INotificationRecyclerAdapter;


import butterknife.BindView;

/**
 * Created by Tranxit Technologies.
 */

public class NotificationViewHolder extends BaseViewHolder<Provider,INotificationRecyclerAdapter> {

    @BindView(R.id.tvDate)
    TextView tvDate;
    @BindView(R.id.tvCallStatus)
    TextView tvCallStatus;
    @BindView(R.id.tvMissedCallStatus)
    TextView tvMissedCallStatus;


    public NotificationViewHolder(View itemView, INotificationRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(Provider data) {

        String date = data.getMissedDate();
        String dateNotification = data.getMissedDate();
        dateNotification = CodeSnippet.parseDateFormatTo12hoursFormat(dateNotification);
        date = CodeSnippet.parseDateTimeFormatToDate(date);

        tvDate.setText(date);
        String missedCallType = "Video Missed Call";


        tvCallStatus.setText(missedCallType);

        String callStatus = "You got a video missed call from patient "+data.getUser().getFirstName()+" "+data.getUser().getLastName() +" by "+dateNotification;

        tvMissedCallStatus.setText(callStatus);

    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
