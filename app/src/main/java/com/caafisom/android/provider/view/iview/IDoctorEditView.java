package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.common.RemoveDoctor;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorEditPresenter;


public interface IDoctorEditView extends IView<IDoctorEditPresenter> {
    void updateDoctor(DoctorListResponse data);
    void initSetUp();
    void getResponse();
    void getRemoveResponse();
    void getDoctorSpeciality(DoctorSpecialityResponse response);
}
