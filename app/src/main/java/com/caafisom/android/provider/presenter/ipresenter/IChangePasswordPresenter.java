package com.caafisom.android.provider.presenter.ipresenter;

import com.caafisom.android.provider.model.dto.request.PasswordRequest;

public interface IChangePasswordPresenter extends IPresenter {
    void updatePassword(PasswordRequest request);
    void goToLogin();
}