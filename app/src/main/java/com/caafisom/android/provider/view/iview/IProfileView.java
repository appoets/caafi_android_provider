package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.common.ServiceItem;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.presenter.ipresenter.IProfilePresenter;

import java.util.List;

public interface IProfileView extends IView<IProfilePresenter> {
    void updateUserDetails(ProfileResponse response);
    void setSpecialitiesList(List<ServiceItem> itemList);
    void setSpecialityName(String name);
    void goToChangePassword();
}
