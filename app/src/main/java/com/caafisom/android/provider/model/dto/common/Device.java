package com.caafisom.android.provider.model.dto.common;

import com.google.gson.annotations.SerializedName;

public class Device{

	@SerializedName("sns_arn")
	private String snsArn;

	@SerializedName("provider_id")
	private int providerId;

	@SerializedName("id")
	private int id;

	@SerializedName("udid")
	private String udid;

	@SerializedName("type")
	private String type;

	@SerializedName("token")
	private String token;

	public void setSnsArn(String snsArn){
		this.snsArn = snsArn;
	}

	public String getSnsArn(){
		return snsArn;
	}

	public void setProviderId(int providerId){
		this.providerId = providerId;
	}

	public int getProviderId(){
		return providerId;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setUdid(String udid){
		this.udid = udid;
	}

	public String getUdid(){
		return udid;
	}

	public void setType(String type){
		this.type = type;
	}

	public String getType(){
		return type;
	}

	public void setToken(String token){
		this.token = token;
	}

	public String getToken(){
		return token;
	}
}