package com.caafisom.android.provider.model.dto.common.videocalldata;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payload {

    @SerializedName("type")
    @Expose
    private String type;

    public String getType() {
        return type;
    }

    public void setType(String team) {
        this.type = team;
    }

}
