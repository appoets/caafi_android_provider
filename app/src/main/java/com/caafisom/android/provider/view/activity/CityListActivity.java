package com.caafisom.android.provider.view.activity;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.presenter.CityListPresenter;
import com.caafisom.android.provider.presenter.ipresenter.ICityListPresenter;
import com.caafisom.android.provider.view.adapter.CityListRecyclerAdater;
import com.caafisom.android.provider.view.iview.ICityListView;
import com.google.firebase.iid.FirebaseInstanceId;

import butterknife.BindView;
import butterknife.OnClick;

public class CityListActivity extends BaseActivity<ICityListPresenter> implements ICityListView {

    @BindView(R.id.rv_city)
    RecyclerView rvcity;
    Context context;


    @Override
    int attachLayout() {
        return R.layout.activity_city_list;
    }

    @Override
    ICityListPresenter initialize() {
        return new CityListPresenter(this);
    }


    @Override
    public void setAdapter(CityListRecyclerAdater adapter) {
        if (adapter.getItemCount() > 0) {
            rvcity.setVisibility(View.VISIBLE);
            rvcity.setAdapter(adapter);
        } else {
            rvcity.setVisibility(View.GONE);
        }
    }

    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
        }
    }

    @Override
    public void getCity(CityList data) {
        Intent output = new Intent();
        output.putExtra("selectedCityID", data.getId());
        output.putExtra("selectedCityName", data.getName());
        setResult(RESULT_OK, output);
        finish();
    }

    @Override
    public void initSetUp() {
        rvcity.setLayoutManager(new LinearLayoutManager(getActivity()));
        rvcity.setItemAnimator(new DefaultItemAnimator());
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }
}