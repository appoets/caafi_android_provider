package com.caafisom.android.provider.presenter;

import android.content.Intent;
import android.os.Bundle;

import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.presenter.ipresenter.IPresenter;
import com.caafisom.android.provider.view.activity.LoginActivity;
import com.caafisom.android.provider.view.iview.IView;

import static com.caafisom.android.provider.MyApplication.getApplicationInstance;

public abstract class BasePresenter<V extends IView> implements IPresenter {
    public final String TAG = getClass().getSimpleName();

    protected Bundle bundle;

    protected V iView;

    public BasePresenter(V iView) {
        this.iView = iView;
    }

    @Override
    public void onCreate(Bundle bundle) {
        this.bundle = bundle;
    }

    @Override
    public void onStart() {

    }

    @Override
    public void onResume() {

    }

    @Override
    public void onStop() {

    }

    @Override
    public void onPause() {

    }

    @Override
    public void onDestroy() {

    }

    @Override
    public void onActivityForResult(int requestCode, int resultCode, Intent data) {
    }

    @Override
    public String getStringRes(int resId) {
        return iView.getActivity().getString(resId);
    }


    @Override
    public void onLogout() {
        getApplicationInstance().logout();
        iView.navigateTo(LoginActivity.class, false, new Bundle());
        iView.getActivity().finishAffinity();
    }

    @Override
    public void onLogout(CustomException e) {

    }
}
