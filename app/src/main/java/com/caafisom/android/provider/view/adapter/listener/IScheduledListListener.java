package com.caafisom.android.provider.view.adapter.listener;


import com.caafisom.android.provider.model.dto.response.ScheduledListResponse;

public interface IScheduledListListener extends BaseRecyclerListener<ScheduledListResponse> {
}
