package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.AddDoctorModel;
import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.DoctorSpecialityModel;
import com.caafisom.android.provider.model.dto.common.AddDoctorResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorPresenter;
import com.caafisom.android.provider.view.iview.IAddDoctorView;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class AddDoctorPresenter extends BasePresenter<IAddDoctorView> implements IAddDoctorPresenter {


    public AddDoctorPresenter(IAddDoctorView iView) {
        super(iView);
        addDoctor();
        getDoctorSpeciality();

    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();
        getDoctorSpeciality();
    }

    private void addDoctor() {

    }

    @Override
    public void addDoctor(HashMap<String, RequestBody> params, MultipartBody.Part filePart) {
        iView.showProgressbar();

        new AddDoctorModel(new IModelListener<BaseResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull BaseResponse response) {
                iView.dismissProgressbar();
                iView.successs();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();

            }
        }).postAdd(params,filePart);
    }

    private void getDoctorSpeciality() {
        iView.showProgressbar();
        new DoctorSpecialityModel(new IModelListener<DoctorSpecialityResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull DoctorSpecialityResponse response) {
                iView.dismissProgressbar();
                iView.getDoctorSpeciality(response);

            }

            @Override
            public void onFailureApi(CustomException e) {

            }

            @Override
            public void onUnauthorizedUser(CustomException e) {

            }

            @Override
            public void onNetworkFailure() {

            }
        }).getDoctorSpecialityList();
    }


}
