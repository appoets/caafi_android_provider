package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.response.Provider;
import com.caafisom.android.provider.presenter.ipresenter.INotificationPresenter;
import com.caafisom.android.provider.view.adapter.NotificationRecyclerAdapter;

public interface INotificationView extends IView<INotificationPresenter> {
    void initSetUp();
    void setAdapter(NotificationRecyclerAdapter adapter);
    void showNotificationDialog(int pos, Provider data);
}
