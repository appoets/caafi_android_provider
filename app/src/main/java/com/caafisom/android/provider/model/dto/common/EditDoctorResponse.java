package com.caafisom.android.provider.model.dto.common;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class EditDoctorResponse extends BaseResponse {
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("data")
    @Expose
    private EditDoctor data;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EditDoctor getData() {
        return data;
    }

    public void setData(EditDoctor data) {
        this.data = data;
    }
}
