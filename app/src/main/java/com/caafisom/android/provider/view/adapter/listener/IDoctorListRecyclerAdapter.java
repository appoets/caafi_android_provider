package com.caafisom.android.provider.view.adapter.listener;


import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;

/**
 * Created by Tranxit Technologies.
 */

public interface IDoctorListRecyclerAdapter extends BaseRecyclerListener<DoctorList> {
}
