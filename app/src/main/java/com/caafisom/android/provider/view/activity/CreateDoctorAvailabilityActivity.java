package com.caafisom.android.provider.view.activity;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.content.Intent;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.presenter.DoctorAvailabilityAddPresenter;
import com.caafisom.android.provider.presenter.DoctorAvailabilityPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorAvailabilityPresenter;
import com.caafisom.android.provider.view.iview.IAddDoctorAvailabilityView;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.Calendar;

import butterknife.BindView;
import butterknife.OnClick;

public class CreateDoctorAvailabilityActivity extends BaseActivity<IAddDoctorAvailabilityPresenter> implements IAddDoctorAvailabilityView {

    @BindView(R.id.civProfile)
    ImageView civProfile;
    @BindView(R.id.tvName)
    TextView tvname;
    @BindView(R.id.btn_update)
    Button btnupdate;
    @BindView(R.id.tv_selectDate)
    TextView tvselectDate;
    @BindView(R.id.dateLayout)
    LinearLayout dateLayout;
    @BindView(R.id.selectedDate)
    TextView selectedDate;
    @BindView(R.id.ibBack)
    ImageView ibBack;


    Integer doctorID;
    String doctorname, doctorimage, selectDate;

    @Override
    int attachLayout() {
        return R.layout.activity_create_doctor_availability;
    }

    @Override
    IAddDoctorAvailabilityPresenter initialize() {
        Intent intent = getIntent();
        if (intent != null) {
            doctorname = intent.getStringExtra("doctor_name");
            doctorID = intent.getIntExtra("doctor_id", 0);
            doctorimage = intent.getStringExtra("doctor_image");

            tvname.setText(doctorname);
            Glide.with(CreateDoctorAvailabilityActivity.this)
                    .load(doctorimage)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user).dontAnimate())
                    .into(civProfile);
        }

        return new DoctorAvailabilityAddPresenter(this);
    }

    @OnClick({R.id.ibBack, R.id.btn_update, R.id.dateLayout})
    public void OnViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                onBackPressed();
                break;
            case R.id.btn_update:
                iPresenter.addDoctorAvailability(doctorID, selectDate);
                break;

            case R.id.dateLayout:
                final Calendar c = Calendar.getInstance();
                int mYear = c.get(Calendar.YEAR); // current year
                int mMonth = c.get(Calendar.MONTH); // current month
                int mDay = c.get(Calendar.DAY_OF_MONTH); // current day
                // date picker dialog
                DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                        new DatePickerDialog.OnDateSetListener() {

                            @Override
                            public void onDateSet(DatePicker view, int year,
                                                  int monthOfYear, int dayOfMonth) {

                                // set day of month , month and year value in the edit text
                                String choosedMonth = "";
                                String choosedDate = "";

                                if (dayOfMonth < 10) {
                                    choosedDate = "0" + dayOfMonth;
                                } else {
                                    choosedDate = "" + dayOfMonth;
                                }

                                if ((monthOfYear + 1) < 10) {
                                    choosedMonth = "0" + (monthOfYear + 1);
                                } else {
                                    choosedMonth = "" + (monthOfYear + 1);
                                }
//                                selectDate = choosedDate + "-" + choosedMonth + "-" + year;
                                selectDate = year + "-" + choosedMonth + "-" + choosedDate;
                                tvselectDate.setVisibility(View.GONE);
                                selectedDate.setVisibility(View.VISIBLE);
                                if (isAfterToday(year, (monthOfYear + 1), dayOfMonth)) {
                                    selectedDate.setText(choosedDate + "-" + choosedMonth + "-" + year);
                                    selectedDate.setText(selectDate);
                                } else {
                                    //scheduleDate.setText("");
                                    showToast("Select Date After Today's Date");
                                }
                            }
                        }, mYear, mMonth, mDay);
                datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis() - 1000);
                datePickerDialog.getDatePicker().setMaxDate((System.currentTimeMillis() - 1000) + (1000 * 60 * 60 * 24 * 7));
                datePickerDialog.show();
                break;

        }
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void initSetUp() {
        String token = FirebaseInstanceId.getInstance().getToken();
        Log.v(TAG, "Token Received ==>> " + token);
    }

    @Override
    public void getSuccessResponse() {
        Toast.makeText(this, "Availability Added Successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(CreateDoctorAvailabilityActivity.this, HomeActivity.class);
        startActivity(intent);
        finish();
    }

    public static boolean isAfterToday(int year, int month, int day) {
        Calendar today = Calendar.getInstance();
        Calendar myDate = Calendar.getInstance();

        myDate.set(year, month, day);
        return !myDate.before(today);
    }


}
