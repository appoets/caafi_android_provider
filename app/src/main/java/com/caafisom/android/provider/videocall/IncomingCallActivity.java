package com.caafisom.android.provider.videocall;

import android.app.ActivityManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.response.CheckStatusResponse;
import com.caafisom.android.provider.service.CheckStatusService;
import com.caafisom.android.provider.view.activity.HomeActivity;
import com.caafisom.android.provider.view.activity.twilio.TwilloVideoActivity;

import com.caafisom.android.provider.R;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import static com.caafisom.android.provider.view.activity.HomeActivity.currentRequestID;

public class IncomingCallActivity extends AppCompatActivity {

    @BindView(R.id.lblName)
    TextView lblName;
    @BindView(R.id.imgEndCall)
    ImageView imgEndCall;
    @BindView(R.id.imgAcceptCall)
    ImageView imgAcceptCall;

    String chatPath, name,mRequestId;
    Ringtone ringtone;
    private MyReceiver myReceiver;

    @Override
    protected void onStart() {
        super.onStart();

        HomeActivity.show_layout = "true";
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(CodeSnippet.USER_ACTION);
        registerReceiver(myReceiver, intentFilter);

    }

    @Override
    protected void onStop() {
        super.onStop();

        unregisterReceiver(myReceiver);
        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(this, CheckStatusService.class));
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON |
                WindowManager.LayoutParams.FLAG_DISMISS_KEYGUARD |
                WindowManager.LayoutParams.FLAG_SHOW_WHEN_LOCKED |
                WindowManager.LayoutParams.FLAG_TURN_SCREEN_ON);

        setContentView(R.layout.activity_incoming_call);
        ButterKnife.bind(this);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            return;
        }

        name = extras.getString("name");
        chatPath = extras.getString("chat_path");
        mRequestId = extras.getString("request_id");
        currentRequestID = Integer.valueOf(mRequestId);
        lblName.setText(name);

        playRingtone();
    }


    @Override
    public void onBackPressed() {

    }

    @OnClick({R.id.imgEndCall, R.id.imgAcceptCall})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.imgEndCall:

                stopRingtone();
                finish();

                break;
            case R.id.imgAcceptCall:

                stopRingtone();
                Intent i = new Intent(getApplicationContext(), TwilloVideoActivity.class);
                i.putExtra("chat_path", chatPath);
                i.putExtra("request_id", mRequestId);
                startActivity(i);

                break;
        }
    }

    private void playRingtone() {
        Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_RINGTONE);

        if (notification != null) {
            ringtone = RingtoneManager.getRingtone(getApplicationContext(), notification);
            ringtone.play();
        }
    }


    private void stopRingtone() {
        if (ringtone != null && ringtone.isPlaying())
            ringtone.stop();
    }

    @Override
    protected void onResume() {
        super.onResume();


        if (!isMyServiceRunning(CheckStatusService.class)) {
            Intent intent = new Intent(this, CheckStatusService.class);
            startService(intent);
        }
    }

    @Override
    protected void onPause() {
        super.onPause();


        if (isMyServiceRunning(CheckStatusService.class)) {
            stopService(new Intent(this, CheckStatusService.class));
        }

    }

    @Override
    public void finish() {
        super.finish();
    }


    private boolean isMyServiceRunning(Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) getSystemService(Context.ACTIVITY_SERVICE);
        if (manager != null) {
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    return true;
                }
            }
        }
        return false;
    }


    private class MyReceiver extends BroadcastReceiver {

        @Override
        public void onReceive(Context context, Intent intent) {
            CheckStatusResponse response;
            if (intent != null && intent.getExtras() != null) {

                if (intent.getParcelableExtra("response") != null) {
                    response = intent.getParcelableExtra("response");
                    Log.e("onReceive Call=>", response.toString());

                    if (response.getServiceStatus().equalsIgnoreCase("active") && response.getRequests() != null && response.getRequests().size() > 0) {
                        currentRequestID = response.getRequests().get(0).getRequestId();
                    }
                }
            }
        }
    }


}
