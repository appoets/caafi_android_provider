package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.HistoryModel;
import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.HistoryResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IHistoryPresenter;
import com.caafisom.android.provider.view.adapter.HistoryAdapter;
import com.caafisom.android.provider.view.iview.IHistoryView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HistoryPresenter extends BasePresenter<IHistoryView> implements IHistoryPresenter {


    public HistoryPresenter(IHistoryView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        iView.initSetUp();

    }

    @Override
    public void onResume() {
        super.onResume();
        getHistory();
    }

    @Override
    public void getHistory() {
        new HistoryModel(new IModelListener<HistoryResponse>() {

            public void onClickItem(int pos, HistoryItem data) {
                iView.moveToChat(data);
            }

            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull HistoryResponse response) {
                iView.setAdapter(new HistoryAdapter(response.getHistory(), this::onClickItem));
                iView.dismissProgressbar();
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHistoryDetails();
    }
}
