package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.model.listener.IModelListListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import java.util.HashMap;
import java.util.List;

public class DoctorAvailabilityModel extends BaseListModel<GetDoctorAvailablity> {

    public DoctorAvailabilityModel(IModelListListener<GetDoctorAvailablity> listener) {
        super(listener);
    }

    @Override
    public void onSuccessfulApi(GetDoctorAvailablity response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onSuccessfulApi(List<GetDoctorAvailablity> response) {
        listener.onSuccessfulApi(response);
    }


    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getDoctorAvailablity(Integer id) {
        enQueueListTask(new ApiClient().getClient().create(ApiInterface.class).getDoctorAvailablity(id));
    }

}
