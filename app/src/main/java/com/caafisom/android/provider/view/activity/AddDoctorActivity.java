package com.caafisom.android.provider.view.activity;

import static com.caafisom.android.provider.common.Constants.Requests.REQUEST_IMAGE;
import static com.caafisom.android.provider.common.Constants.Requests.REQUEST_PERMISSION;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.presenter.AddDoctorPresenter;
import com.caafisom.android.provider.presenter.RegisterPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IAddDoctorPresenter;
import com.caafisom.android.provider.view.iview.IAddDoctorView;
import com.caafisom.android.provider.view.iview.IView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.EasyPermissions;

public class AddDoctorActivity extends BaseActivity<IAddDoctorPresenter> implements IAddDoctorView, EasyPermissions.PermissionCallbacks {

    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etExperience)
    EditText etExperience;
    @BindView(R.id.etPrice)
    EditText etPrice;
   /* @BindView(R.id.etSpecialise)
    EditText etSpecialise;*/

    @BindView(R.id.spinner_specialise)
    Spinner spinnerspecialise;

    @BindView(R.id.btnCreate)
    Button btnCreate;
    @BindView(R.id.civProfile)
    CircleImageView civProfile;
    private File profileImageFile;

    ArrayList<String> specialityList = new ArrayList<>();
    private DoctorSpecialityResponse doctorSpecialityResponse;


    @Override
    int attachLayout() {
        return R.layout.activity_add_doctor;
    }

    @Override
    IAddDoctorPresenter initialize() {
        return new AddDoctorPresenter(this);

    }

    private void checkStoragePermission() {
        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            EasyImage.openChooserWithDocuments(AddDoctorActivity.this, "Select", REQUEST_IMAGE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.image_selection_rationale),
                    REQUEST_PERMISSION, perms);
        }
    }

    private void validateCreateDoctor() {
        String email = etEmail.getText().toString().trim();
        String first_name = etName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String experience = etExperience.getText().toString().trim();
        String price_min = etPrice.getText().toString().trim();
//        String specialise = etSpecialise.getText().toString().trim();

        /*else if (specialise.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_specialities));
        }*/

        if (TextUtils.isEmpty(email)) {
            showSnackBar(getString(R.string.please_enter_email));
        } else if (!getCodeSnippet().isEmailValid(email)) {
            showSnackBar(getString(R.string.please_enter_valid_email));
        } else if (first_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_name));
        } else if (last_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_last_name));
        } else if (experience.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_experience));
        } else if (price_min.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_price_min));
        } else if (spinnerspecialise.getAdapter() == null || spinnerspecialise.getSelectedItem() == null) {
            showSnackBar(getString(R.string.please_enter_specialities));
        } else {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), first_name));
            map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), last_name));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
            map.put("consultation_fee", RequestBody.create(MediaType.parse("text/plain"), price_min));
            map.put("experience", RequestBody.create(MediaType.parse("text/plain"), experience));
//            map.put("specialise", RequestBody.create(MediaType.parse("text/plain"), specialise));

            if (spinnerspecialise.getAdapter() != null) {
                doctorSpecialityResponse.getData().get(spinnerspecialise.getSelectedItemPosition());
                map.put("speciality_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(doctorSpecialityResponse.getData().get(spinnerspecialise.getSelectedItemPosition()).getId())));
            }

            MultipartBody.Part filePart1 = null;
            if (profileImageFile != null && profileImageFile.exists()) {
                filePart1 = MultipartBody.Part.createFormData("picture", profileImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageFile));
            }
            iPresenter.addDoctor(map, filePart1);

        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if (type == REQUEST_IMAGE) {
                    profileImageFile = imageFiles.get(0);
                    Glide.with(AddDoctorActivity.this)
                            .load(profileImageFile)
                            .apply(new RequestOptions()
                                    .placeholder(R.drawable.ic_dummy_user)
                                    .error(R.drawable.ic_dummy_user).dontAnimate())
                            .into(civProfile);
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }

    @Override
    public void initSetUp() {

        civProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkStoragePermission();
            }
        });

        btnCreate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                validateCreateDoctor();
            }
        });

    }

    @Override
    public void successs() {
        Toast.makeText(this, "Created Successfully!", Toast.LENGTH_SHORT).show();
    }

    @SuppressLint("LogNotTimber")
    @Override
    public void getDoctorSpeciality(DoctorSpecialityResponse response) {
        doctorSpecialityResponse = response;
        specialityList.clear();

        for (int i = 0; i < response.getData().size(); i++) {
            specialityList.add(response.getData().get(i).getName());
        }

        ArrayAdapter mspecialAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, specialityList);
        spinnerspecialise.setAdapter(mspecialAdapter);
        spinnerspecialise.setSelection(0);

    }

    @Override
    public void onPermissionsGranted(int i, @NonNull List<String> list) {
        EasyImage.openChooserWithDocuments(AddDoctorActivity.this, "Select", REQUEST_IMAGE);
    }

    @Override
    public void onPermissionsDenied(int i, @NonNull List<String> list) {

    }
}