package com.caafisom.android.provider.view.adapter.listener;

import com.caafisom.android.provider.model.dto.response.Provider;

/**
 * Created by Tranxit Technologies.
 */

public interface INotificationRecyclerAdapter extends BaseRecyclerListener<Provider> {
}
