package com.caafisom.android.provider.model;

import com.caafisom.android.provider.model.dto.request.RegisterRequest;
import com.caafisom.android.provider.model.dto.response.RegisterResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

import java.util.HashMap;

public class RegisterModel extends BaseModel<RegisterResponse> {

    public RegisterModel(IModelListener<RegisterResponse> listener) {
        super(listener);
    }

    public void postRegister(RegisterRequest request) {
       enQueueTask(new ApiClient().getClient().create(ApiInterface.class).postRegister(request));
    }

    @Override
    public void onSuccessfulApi(RegisterResponse response) {
        listener.onSuccessfulApi(response);
    }

    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }
}
