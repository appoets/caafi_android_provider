package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorPresenter;
import com.caafisom.android.provider.view.adapter.DoctorListRecyclerAdater;


import java.util.List;

public interface IDoctorView extends IView<IDoctorPresenter> {
    void getDoctor(DoctorList data);
    void setAdapter(DoctorListRecyclerAdater adapter);
    void initSetUp();
}
