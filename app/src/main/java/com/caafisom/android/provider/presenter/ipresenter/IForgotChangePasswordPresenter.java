package com.caafisom.android.provider.presenter.ipresenter;

import com.caafisom.android.provider.model.dto.request.ResetPasswordRequest;

public interface IForgotChangePasswordPresenter extends IPresenter {
    void goToLogin();
    void resetPassword(ResetPasswordRequest resetPasswordRequest);
}