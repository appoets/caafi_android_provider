package com.caafisom.android.provider.model.dto.response;

import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.Provider;

public class ForgotPasswordResponse extends BaseResponse{

	@SerializedName("provider")
	private Provider provider;

	public void setProvider(Provider provider){
		this.provider = provider;
	}

	public Provider getProvider(){
		return provider;
	}

}