package com.caafisom.android.provider.model.dto.request;


import java.util.List;

public class RegisterRequest {

    private String first_name;
    private String last_name;
    private String email;
    private String mobile;
    private String password;
    private String password_confirmation;
    private String device_id;
    private String device_type;
    private String device_token;
    private List<Integer> service_type;


    private String experience;
    private String provider_price;
    private String city_id;


    public RegisterRequest() {

    }

    public String getFirst_name() {
        return first_name;
    }

    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    public String getLast_name() {
        return last_name;
    }

    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPassword_confirmation() {
        return password_confirmation;
    }

    public void setPassword_confirmation(String password_confirmation) {
        this.password_confirmation = password_confirmation;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_token() {
        return device_token;
    }

    public void setDevice_token(String device_token) {
        this.device_token = device_token;
    }

    public String getExperience() {
        return experience;
    }

    public void setExperience(String experience) {
        this.experience = experience;
    }

    public String getProvider_price() {
        return provider_price;
    }

    public void setProvider_price(String provider_price) {
        this.provider_price = provider_price;
    }

    public String getCity_id() {
        return city_id;
    }

    public void setCity_id(String city_id) {
        this.city_id = city_id;
    }


    public List<Integer> getService_type() {
        return service_type;
    }

    public void setService_type(String s, List<Integer> service_type) {
        this.service_type = service_type;
    }
}
