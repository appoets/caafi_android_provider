package com.caafisom.android.provider.presenter.ipresenter;

public interface ISplashPresenter extends IPresenter {

    boolean onCheckUserStatus();

    boolean hasInternet();

    void goToHome();

    void goToLogin();

}