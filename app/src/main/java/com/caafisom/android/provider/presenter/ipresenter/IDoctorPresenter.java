package com.caafisom.android.provider.presenter.ipresenter;

import com.caafisom.android.provider.model.dto.common.ServiceItem;
import com.caafisom.android.provider.view.iview.IView;

import java.util.HashMap;
import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public interface IDoctorPresenter extends IPresenter {
    void getDoctorsList();

}