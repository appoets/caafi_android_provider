package com.caafisom.android.provider.model.dto.response;

import com.caafisom.android.provider.model.dto.common.Service;
import com.google.gson.annotations.SerializedName;
import com.caafisom.android.provider.model.dto.common.RegisterService;

import java.util.List;

public class RegisterResponse extends  BaseResponse{

	@SerializedName("latitude")
	private String latitude;

	@SerializedName("mobile")
	private String mobile;

	@SerializedName("rating")
	private String rating;

	@SerializedName("last_name")
	private String lastName;

	@SerializedName("description")
	private String description;

	@SerializedName("otp")
	private int otp;

	@SerializedName("avatar")
	private String avatar;

	@SerializedName("experience")
	private String experience;

	@SerializedName("rating_count")
	private int ratingCount;

	@SerializedName("service")
//	private RegisterService service;
	private List<RegisterService> service;


	@SerializedName("id")
	private int id;

	@SerializedName("first_name")
	private String firstName;

	@SerializedName("email")
	private String email;

	@SerializedName("status")
	private String status;

	@SerializedName("longitude")
	private String longitude;

	public void setLatitude(String latitude){
		this.latitude = latitude;
	}

	public String getLatitude(){
		return latitude;
	}

	public void setMobile(String mobile){
		this.mobile = mobile;
	}

	public String getMobile(){
		return mobile;
	}

	public void setRating(String rating){
		this.rating = rating;
	}

	public String getRating(){
		return rating;
	}

	public void setLastName(String lastName){
		this.lastName = lastName;
	}

	public String getLastName(){
		return lastName;
	}

	public void setDescription(String description){
		this.description = description;
	}

	public String getDescription(){
		return description;
	}

	public void setOtp(int otp){
		this.otp = otp;
	}

	public int getOtp(){
		return otp;
	}

	public void setAvatar(String avatar){
		this.avatar = avatar;
	}

	public String getAvatar(){
		return avatar;
	}

	public void setExperience(String experience){
		this.experience = experience;
	}

	public String getExperience(){
		return experience;
	}

	public void setRatingCount(int ratingCount){
		this.ratingCount = ratingCount;
	}

	public int getRatingCount(){
		return ratingCount;
	}

	/*public void setService(RegisterService service){
		this.service = service;
	}

	public RegisterService getService(){
		return service;
	}*/

	public List<RegisterService> getService() {
		return service;
	}

	public void setService(List<RegisterService> service) {
		this.service = service;
	}

	public void setId(int id){
		this.id = id;
	}

	public int getId(){
		return id;
	}

	public void setFirstName(String firstName){
		this.firstName = firstName;
	}

	public String getFirstName(){
		return firstName;
	}

	public void setEmail(String email){
		this.email = email;
	}

	public String getEmail(){
		return email;
	}

	public void setStatus(String status){
		this.status = status;
	}

	public String getStatus(){
		return status;
	}

	public void setLongitude(String longitude){
		this.longitude = longitude;
	}

	public String getLongitude(){
		return longitude;
	}
}