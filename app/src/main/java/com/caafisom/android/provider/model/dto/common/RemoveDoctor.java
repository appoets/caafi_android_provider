package com.caafisom.android.provider.model.dto.common;

import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RemoveDoctor extends BaseResponse {
    @SerializedName("message")
    @Expose
    private String message;

    @Override
    public String getMessage() {
        return message;
    }

    @Override
    public void setMessage(String message) {
        this.message = message;
    }
}
