package com.caafisom.android.provider.view.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.ServiceItem;
import com.caafisom.android.provider.model.dto.response.ProfileResponse;
import com.caafisom.android.provider.presenter.ProfilePresenter;
import com.caafisom.android.provider.presenter.ipresenter.IProfilePresenter;
import com.caafisom.android.provider.view.iview.IProfileView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import butterknife.BindView;
import butterknife.OnClick;
import de.hdodenhof.circleimageview.CircleImageView;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import pub.devrel.easypermissions.EasyPermissions;

import static com.caafisom.android.provider.common.Constants.Requests.REQUEST_IMAGE;
import static com.caafisom.android.provider.common.Constants.Requests.REQUEST_PERMISSION;
import static com.caafisom.android.provider.common.Constants.StoragePath.WEB_PATH;

public class ProfileActivity extends BaseActivity<IProfilePresenter> implements IProfileView, EasyPermissions.PermissionCallbacks {


    @BindView(R.id.etFirstName)
    EditText etFirstName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etMobileNumber)
    EditText etMobileNumber;
    @BindView(R.id.etFarePerMin)
    EditText etFarePerMin;
    @BindView(R.id.etSlotTime)
    EditText etSlotTime;
    @BindView(R.id.etQualification)
    EditText etQualification;
    @BindView(R.id.etExperience)
    EditText etExperience;

    @BindView(R.id.civProfile)
    CircleImageView civProfile;
    @BindView(R.id.ivCertificate)
    ImageView ivCertificate;

    @BindView(R.id.tvSaveEdit)
    TextView tvSaveEdit;
    @BindView(R.id.tvEmail)
    TextView tvEmail;
    @BindView(R.id.tvSpecialities)
    TextView tvSpecialities;

    @BindView(R.id.viewFirstName)
    View viewFirstName;
    @BindView(R.id.viewLastName)
    View viewLastName;
    @BindView(R.id.viewMobileNumber)
    View viewMobileNumber;


    int SpecialitiesId;
    List<ServiceItem> specialitiesList = new ArrayList<>();

    private File profileImageFile, certificateFile;
    private boolean isProfile = false;

    @Override
    int attachLayout() {
        return R.layout.activity_profile;
    }

    @Override
    IProfilePresenter initialize() {
        return new ProfilePresenter(this);
    }

    @OnClick({R.id.ivBack, R.id.tvSaveEdit, R.id.civProfile, R.id.lblChangePassword, R.id.tvUpload})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.ivBack:
                onBackPressed();
                break;

            case R.id.tvSaveEdit:
                if (!etFirstName.isEnabled()) {
                    tvSaveEdit.setText(R.string.save);
                    etFirstName.setEnabled(true);
                    etLastName.setEnabled(true);
                    etMobileNumber.setEnabled(true);
                    etFarePerMin.setEnabled(true);
                    etSlotTime.setEnabled(true);
                    etQualification.setEnabled(true);
                    etExperience.setEnabled(true);

                    etFirstName.requestFocus();
                } else {
                    tvSaveEdit.setText(R.string.edit);
                    etFirstName.setEnabled(false);
                    etLastName.setEnabled(false);
                    etMobileNumber.setEnabled(false);
                    etFarePerMin.setEnabled(false);
                    etSlotTime.setEnabled(false);
                    etQualification.setEnabled(false);
                    etExperience.setEnabled(false);
                    updateProfile();
                }
                break;

            case R.id.civProfile:
                if (etFirstName.isEnabled()) {
                    isProfile = true;
                    checkStoragePermission();
                }
                break;

            case R.id.lblChangePassword:
                iPresenter.goToChangePassword();
                break;

            case R.id.tvUpload:
                if (etFirstName.isEnabled()) {
                    isProfile = false;
                    checkStoragePermission();
                }
                break;

        }
    }

    private void updateProfile() {
        String firstname = etFirstName.getText().toString().trim();
        String lastname = etLastName.getText().toString().trim();
        String phonenumber = etMobileNumber.getText().toString().trim();
        String timeSlot = etSlotTime.getText().toString().trim();

        if (TextUtils.isEmpty(firstname)) {
            showSnackBar(getString(R.string.please_enter_name));
        } else if (TextUtils.isEmpty(lastname)) {
            showSnackBar(getString(R.string.please_enter_last_name));
        } else if (TextUtils.isEmpty(phonenumber)) {
            showSnackBar(getString(R.string.please_enter_mobile_number));
        } else if (TextUtils.isEmpty(timeSlot) || timeSlot.trim().equals("-")) {
            showSnackBar(getString(R.string.please_enter_slot_time));
        } else {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), firstname));
            map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), lastname));
            map.put("mobile", RequestBody.create(MediaType.parse("text/plain"), phonenumber));
            map.put("basePrice", RequestBody.create(MediaType.parse("text/plain"), etFarePerMin.getText().toString()));
            map.put("provider_slot", RequestBody.create(MediaType.parse("text/plain"), timeSlot));
            map.put("qualification", RequestBody.create(MediaType.parse("text/plain"), etQualification.getText().toString()));
            map.put("experience", RequestBody.create(MediaType.parse("text/plain"), etExperience.getText().toString()));

            MultipartBody.Part filePart1 = null;
            if (profileImageFile != null && profileImageFile.exists()) {
                filePart1 = MultipartBody.Part.createFormData("avatar", profileImageFile.getName(), RequestBody.create(MediaType.parse("image/*"), profileImageFile));
            }
            MultipartBody.Part filePart2 = null;
            if (certificateFile != null && certificateFile.exists()) {
                filePart2 = MultipartBody.Part.createFormData("certificate", certificateFile.getName(), RequestBody.create(MediaType.parse("image/*"), certificateFile));
            }

            iPresenter.updateProfile(map, filePart1, filePart2);
        }
    }

    private void checkStoragePermission() {
        String[] perms = {Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (EasyPermissions.hasPermissions(this, perms)) {
            EasyImage.openChooserWithDocuments(ProfileActivity.this, "Select", REQUEST_IMAGE);
        } else {
            // Do not have permissions, request them now
            EasyPermissions.requestPermissions(this, getString(R.string.image_selection_rationale),
                    REQUEST_PERMISSION, perms);
        }
    }


    @Override
    public void updateUserDetails(ProfileResponse response) {
        if (response != null) {
            etFirstName.setText(response.getFirstName());
            etLastName.setText(response.getLastName());
            String image_url = WEB_PATH + response.getAvatar();
            Glide.with(ProfileActivity.this)
                    .load(WEB_PATH + response.getCertificate())
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user).dontAnimate())
                    .into(ivCertificate);
            etMobileNumber.setText(response.getMobile());
            tvEmail.setText(response.getEmail());
            SpecialitiesId = response.getService().getServiceTypeId();

            if (!specialitiesList.isEmpty()) {
                iPresenter.setSpecialitiesName(SpecialitiesId, specialitiesList);
            }

            etFarePerMin.setText(response.getService().getProviderPrice() != null ?
                    "" + response.getService().getProviderPrice() : " - ");
            etSlotTime.setText(response.getService().getSlotTime() != null && !response.getService().getSlotTime().equalsIgnoreCase(null) ?
                    response.getService().getSlotTime() : " - ");
            etQualification.setText(response.getQualification() != null && !response.getQualification().equalsIgnoreCase(null) ?
                    response.getQualification() : " - ");
            etExperience.setText("" + response.getExperience());
        }
    }

    @Override
    public void setSpecialityName(String name) {
        tvSpecialities.setText(name);
    }

    @Override
    public void setSpecialitiesList(List<ServiceItem> itemList) {
        specialitiesList = itemList;
        if (SpecialitiesId != 0) {
            iPresenter.setSpecialitiesName(SpecialitiesId, specialitiesList);
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, @NonNull List<String> perms) {
        EasyImage.openChooserWithDocuments(ProfileActivity.this, "Select", REQUEST_IMAGE);
    }

    @Override
    public void onPermissionsDenied(int requestCode, @NonNull List<String> perms) {

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                e.printStackTrace();
            }

            @Override
            public void onImagesPicked(@NonNull List<File> imageFiles, EasyImage.ImageSource source, int type) {
                if (type == REQUEST_IMAGE) {
                    if (isProfile) {
                        profileImageFile = imageFiles.get(0);
                        Glide.with(ProfileActivity.this)
                                .load(profileImageFile)
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.ic_dummy_user)
                                        .error(R.drawable.ic_dummy_user).dontAnimate())
                                .into(civProfile);
                        isProfile = false;
                    } else {
                        certificateFile = imageFiles.get(0);
                        Glide.with(ProfileActivity.this)
                                .load(certificateFile)
                                .apply(new RequestOptions()
                                        .placeholder(R.drawable.ic_dummy_user)
                                        .error(R.drawable.ic_dummy_user).dontAnimate())
                                .into(ivCertificate);
                        isProfile = false;
                    }
                }
            }

            @Override
            public void onCanceled(EasyImage.ImageSource source, int type) {

            }
        });
    }

    @Override
    public void goToChangePassword() {
        navigateTo(ChangePasswordActivity.class, false, new Bundle());
    }
}
