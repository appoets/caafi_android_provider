package com.caafisom.android.provider.model.dto.response;

import com.google.gson.annotations.SerializedName;


public class AccessToken extends BaseResponse{


	@SerializedName("accessToken")
	private String accessToken;


	public String getAccessToken() {
		return accessToken;
	}

	public void setAccessToken(String accessToken) {
		this.accessToken = accessToken;
	}

}