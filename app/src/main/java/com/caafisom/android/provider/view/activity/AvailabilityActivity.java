package com.caafisom.android.provider.view.activity;

import android.app.TimePickerDialog;
import android.support.annotation.NonNull;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SwitchCompat;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.View;
import android.widget.CalendarView;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.common.utils.CodeSnippet;
import com.caafisom.android.provider.model.dto.response.AvailabilityListResponse;
import com.caafisom.android.provider.presenter.AvailabilityListPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IAvailabilityListPresenter;
import com.caafisom.android.provider.view.adapter.ScheduledListAdapter;
import com.caafisom.android.provider.view.adapter.listener.IScheduledListListener;
import com.caafisom.android.provider.view.iview.IAvailabityListView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.OnClick;

public class AvailabilityActivity extends BaseActivity<IAvailabilityListPresenter> implements IAvailabityListView {

    @BindView(R.id.ibBack)
    ImageView ibBack;
    @BindView(R.id.rcvScheduledList)
    RecyclerView rcvScheduledList;
    @BindView(R.id.calendarView)
    CalendarView calendarView;
    @BindView(R.id.noData)
    TextView noData;
    @BindView(R.id.switchMorning)
    SwitchCompat switchMorning;
    @BindView(R.id.lblMorningStart)
    TextView lblMorningStart;
    @BindView(R.id.lblMorningTo)
    TextView lblMorningTo;
    @BindView(R.id.switchAfternoon)
    SwitchCompat switchAfternoon;
    @BindView(R.id.switchNight)
    SwitchCompat switchNight;
    @BindView(R.id.lblNightFrom)
    TextView lblNightFrom;
    @BindView(R.id.lblNightTo)
    TextView lblNightTo;
    @BindView(R.id.switchOnsite)
    SwitchCompat switchOnsite;
    @BindView(R.id.switchOnsiteVideo)
    SwitchCompat switchOnsiteVideo;
    @BindView(R.id.lblDone)
    TextView lblDone;

    private String selectedDate;
    private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
    private ScheduledListAdapter adapter;
    private List<AvailabilityListResponse> availabilityResponse = new ArrayList<>();
    private List<AvailabilityListResponse> selectedDateList;
    private String tempDate;
    private IScheduledListListener iScheduledListListener;
    private String MorningfromTime = "00:00", MorningtoTime = "00:00", NightfromTime = "00:00", NighttoTime = "00:00";


    int switchmorning =0, switchafternoon =0, switchnight = 0, switchonsite = 0, switchonsiteVideo = 0;

    @Override
    int attachLayout() {
        return R.layout.activity_availability;
    }

    @Override
    IAvailabilityListPresenter initialize() {
        selectedDate = dateFormat.format(Calendar.getInstance().getTime());
        return new AvailabilityListPresenter(this, selectedDate);
    }


    @OnClick({R.id.ibBack})
    void onClick(View view) {
        switch (view.getId()) {
            case R.id.ibBack:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void setAdapter(List<AvailabilityListResponse> list) {
        if (list.size() > 0) {
            availabilityResponse = list;

            String[] dividegetMorningStartTime= availabilityResponse.get(0).getMorningStartTime().split("\\s");
            String[] dividegetMorningEndTime= availabilityResponse.get(0).getMorningEndTime().split("\\s");
            String[] dividegetNightStartTime= availabilityResponse.get(0).getNightStartTime().split("\\s");
            String[] dividegetNightEndTime= availabilityResponse.get(0).getNightEndTime().split("\\s");

            try {
                MorningfromTime = dividegetMorningStartTime[0];
                MorningtoTime = dividegetMorningEndTime[0];
                NightfromTime = dividegetNightStartTime[0];
                NighttoTime = dividegetNightEndTime[0];

                lblMorningStart.setText(dividegetMorningStartTime[0]);
                lblMorningTo.setText(dividegetMorningEndTime[0]);
                lblNightFrom.setText(dividegetNightStartTime[0]);
                lblNightTo.setText(dividegetNightEndTime[0]);
            } catch (Exception ce) {
                ce.printStackTrace();
            }

            switchMorning.setChecked(availabilityResponse.get(0).getMorning() == 1);
            switchAfternoon.setChecked(availabilityResponse.get(0).getAfternoon() == 1);
            switchNight.setChecked(availabilityResponse.get(0).getNight() == 1);
            switchOnsite.setChecked(availabilityResponse.get(0).getOnsite() == 1);
            switchOnsiteVideo.setChecked(availabilityResponse.get(0).getVideoConsultancy() == 1);

        } else {


            MorningfromTime = "00:00";
            MorningtoTime = "00:00";
            NightfromTime = "00:00";
            NighttoTime = "00:00";


            lblMorningStart.setText("00:00");
            lblMorningTo.setText("00:00");
            lblNightFrom.setText("00:00");
            lblNightTo.setText("00:00");

            switchMorning.setChecked(false);
            switchAfternoon.setChecked(false);
            switchNight.setChecked(false);
            switchOnsite.setChecked(false);
            switchOnsiteVideo.setChecked(false);

        }
    }

    @Override
    public void moveToDetail(AvailabilityListResponse data) {

    }

    @Override
    public void initSetUp() {
        rcvScheduledList.setLayoutManager(new LinearLayoutManager(getActivity()));
        rcvScheduledList.setItemAnimator(new DefaultItemAnimator());

        switchMorning.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    switchmorning =1;
                }
                else
                {
                    switchmorning =0;
                }

            }
        });


        switchAfternoon.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    switchafternoon =1;
                }
                else
                {
                    switchafternoon =0;
                }

            }
        });


        switchNight.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    switchnight =1;
                }
                else
                {
                    switchnight =0;
                }

            }
        });


        switchOnsite.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    switchonsite =1;
                }
                else
                {
                    switchonsite =0;
                }

            }
        });


        switchOnsiteVideo.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean checked) {

                if(checked)
                {
                    switchonsiteVideo =1;
                }
                else
                {
                    switchonsiteVideo =0;
                }

            }
        });

    }

    @Override
    protected void onResume() {
        super.onResume();

        calendarView.setDate(Calendar.getInstance().getTimeInMillis(), false, true);
        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {
            @Override
            public void onSelectedDayChange(@NonNull CalendarView view, int year, int month, int dayOfMonth) {
                try {
                    //selectedDate = year + "-" + (month + 1) + "-" + dayOfMonth;
                    Date enddateObj = dateFormat.parse(year + "-" + (month + 1) + "-" + dayOfMonth);
                    selectedDate = dateFormat.format(enddateObj);
                    Log.d(TAG, "onSelectedDayChange: " + selectedDate);
                    call(selectedDate);
                    //selectedDate = dateFormat.format(selectedDate);
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }
        });


        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        selectedDate = sdf.format(new Date(calendarView.getDate()));
        Log.d(TAG, "onSelectedDayChange: " + selectedDate);

        new AvailabilityListPresenter(this, selectedDate);

    }

    void call(String selectedDate) {
        new AvailabilityListPresenter(this, selectedDate);
    }


    @OnClick({R.id.lblMorningStart, R.id.lblMorningTo, R.id.lblNightFrom, R.id.lblNightTo, R.id.lblDone})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.lblMorningStart: {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String choosedHour = "";
                        String choosedMinute = "";
                        if (selectedHour < 10) {
                            choosedHour = "0" + selectedHour;
                        } else {
                            choosedHour = "" + selectedHour;
                        }

                        if (selectedMinute < 10) {
                            choosedMinute = "0" + selectedMinute;
                        } else {
                            choosedMinute = "" + selectedMinute;
                        }
                        MorningfromTime = choosedHour + ":" + choosedMinute;
                        //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                        if (!MorningfromTime.equals("")) {
                            Date date = null;
                            try {
                                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long milliseconds = date.getTime();
                            if (!DateUtils.isToday(milliseconds)) {
                                lblMorningStart.setText(MorningfromTime);
                            } else {
                                if (CodeSnippet.checktimings(MorningfromTime)) {
                                    lblMorningStart.setText(MorningfromTime);
                                } else {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
            break;
            case R.id.lblMorningTo: {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String choosedHour = "";
                        String choosedMinute = "";
                        if (selectedHour < 10) {
                            choosedHour = "0" + selectedHour;
                        } else {
                            choosedHour = "" + selectedHour;
                        }

                        if (selectedMinute < 10) {
                            choosedMinute = "0" + selectedMinute;
                        } else {
                            choosedMinute = "" + selectedMinute;
                        }
                        MorningtoTime = choosedHour + ":" + choosedMinute;
                        //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                        if (!MorningtoTime.equals("")) {
                            Date date = null;
                            try {
                                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long milliseconds = date.getTime();
                            if (!DateUtils.isToday(milliseconds)) {
                                lblMorningTo.setText(MorningtoTime);
                            } else {
                                if (CodeSnippet.checktimings(MorningtoTime)) {
                                    lblMorningTo.setText(MorningtoTime);
                                } else {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }

            break;
            case R.id.lblNightFrom: {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String choosedHour = "";
                        String choosedMinute = "";
                        if (selectedHour < 10) {
                            choosedHour = "0" + selectedHour;
                        } else {
                            choosedHour = "" + selectedHour;
                        }

                        if (selectedMinute < 10) {
                            choosedMinute = "0" + selectedMinute;
                        } else {
                            choosedMinute = "" + selectedMinute;
                        }
                        NightfromTime = choosedHour + ":" + choosedMinute;
                        //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                        if (!NightfromTime.equals("")) {
                            Date date = null;
                            try {
                                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long milliseconds = date.getTime();
                            if (!DateUtils.isToday(milliseconds)) {
                                lblNightFrom.setText(NightfromTime);
                            } else {
                                if (CodeSnippet.checktimings(NightfromTime)) {
                                    lblNightFrom.setText(NightfromTime);
                                } else {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }


            break;
            case R.id.lblNightTo: {

                Calendar mcurrentTime = Calendar.getInstance();
                int hour = mcurrentTime.get(Calendar.HOUR_OF_DAY);
                int minute = mcurrentTime.get(Calendar.MINUTE);
                TimePickerDialog mTimePicker = new TimePickerDialog(getActivity(), new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int selectedHour, int selectedMinute) {
                        String choosedHour = "";
                        String choosedMinute = "";
                        if (selectedHour < 10) {
                            choosedHour = "0" + selectedHour;
                        } else {
                            choosedHour = "" + selectedHour;
                        }

                        if (selectedMinute < 10) {
                            choosedMinute = "0" + selectedMinute;
                        } else {
                            choosedMinute = "" + selectedMinute;
                        }
                        NighttoTime = choosedHour + ":" + choosedMinute;
                        //choosedTime = choosedHour + ":" + choosedMinute + " " + choosedTimeZone;
                        if (!NighttoTime.equals("")) {
                            Date date = null;
                            try {
                                date = new SimpleDateFormat("dd-MM-yyyy", Locale.ENGLISH).parse(selectedDate);
                            } catch (ParseException e) {
                                e.printStackTrace();
                            }
                            long milliseconds = date.getTime();
                            if (!DateUtils.isToday(milliseconds)) {
                                lblNightTo.setText(NighttoTime);
                            } else {
                                if (CodeSnippet.checktimings(NighttoTime)) {
                                    lblNightTo.setText(NighttoTime);
                                } else {
                                    Toast.makeText(getActivity(), getActivity().getResources().getString(R.string.different_time), Toast.LENGTH_SHORT).show();
                                }
                            }
                        }
                    }
                }, hour, minute, true);//Yes 24 hour time
                mTimePicker.setTitle("Select Time");
                mTimePicker.show();
            }
            break;
            case R.id.lblDone:
                HashMap<String, Object> map = new HashMap<>();



                map.put("available_date", selectedDate);
                map.put("morning", switchmorning);
                map.put("morning_start_time", selectedDate + " " +MorningfromTime);
                map.put("morning_end_time", selectedDate + " " +MorningtoTime);
                map.put("afternoon", switchafternoon);
                map.put("afternoon_start_time", "2019-07-04 12:30:00");
                map.put("afternoon_end_time", "2019-07-04 02:30:00");
                map.put("night", switchnight);
                map.put("night_start_time", selectedDate + " " + NightfromTime);
                map.put("night_end_time", selectedDate + " " + NighttoTime);
                map.put("onsite", switchonsite);
                map.put("video_consultancy", switchonsiteVideo);

                iPresenter.postAvailablity(map);
                break;
        }
    }
}
