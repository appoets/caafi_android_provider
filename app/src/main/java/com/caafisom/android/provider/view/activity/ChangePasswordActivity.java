package com.caafisom.android.provider.view.activity;

import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.EditText;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.request.PasswordRequest;
import com.caafisom.android.provider.presenter.ChangePasswordPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IChangePasswordPresenter;
import com.caafisom.android.provider.view.iview.IChangePasswordView;

import butterknife.BindView;
import butterknife.OnClick;

public class ChangePasswordActivity extends BaseActivity<IChangePasswordPresenter> implements IChangePasswordView {

    @BindView(R.id.etOldPassword)
    EditText etOldPassword;
    @BindView(R.id.etNewPassword)
    EditText etNewPassword;
    @BindView(R.id.etConfirmPassword)
    EditText etConfirmPassword;

    @Override
    int attachLayout() {
        return R.layout.activity_change_password;
    }

    @Override
    IChangePasswordPresenter initialize() {
        return new ChangePasswordPresenter(this);
    }


    @OnClick({R.id.btnChangePassword,R.id.imgBack})
    public void OnClickView(View view) {
        switch (view.getId()) {

            case R.id.btnChangePassword:
                String oldPassword = etOldPassword.getText().toString().trim();
                String newPassword = etNewPassword.getText().toString().trim();
                String confirmNewPassword = etConfirmPassword.getText().toString().trim();

                if (TextUtils.isEmpty(oldPassword)){
                    showSnackBar(getString(R.string.error_please_enter_old_password));
                }else if(TextUtils.isEmpty(newPassword)){
                    showSnackBar(getString(R.string.error_please_enter_new_password));
                }else if(TextUtils.isEmpty(confirmNewPassword)){
                    showSnackBar(getString(R.string.error_please_enter_confirm_password));
                }else{
                    PasswordRequest request = new PasswordRequest();
                    request.setPassword_old(oldPassword);
                    request.setPassword(newPassword);
                    request.setPassword_confirmation(confirmNewPassword);
                    iPresenter.updatePassword(request);
                }

                break;

            case R.id.imgBack:
                getActivity().onBackPressed();
                break;

        }
    }


    @Override
    public void goToLogin() {
        startActivity(new Intent(this, LoginActivity.class).setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK));
    }


}
