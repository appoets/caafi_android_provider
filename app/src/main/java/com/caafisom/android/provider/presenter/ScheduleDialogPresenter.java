package com.caafisom.android.provider.presenter;

import com.caafisom.android.provider.presenter.ipresenter.IScheduleDialogPresenter;
import com.caafisom.android.provider.view.iview.IScheduleDialogView;


public class ScheduleDialogPresenter extends BasePresenter<IScheduleDialogView> implements IScheduleDialogPresenter {


    public ScheduleDialogPresenter(IScheduleDialogView iView) {
        super(iView);
    }

}
