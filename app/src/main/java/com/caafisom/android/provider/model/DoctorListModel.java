package com.caafisom.android.provider.model;


import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.model.webservice.ApiClient;
import com.caafisom.android.provider.model.webservice.ApiInterface;

/**
 * Created by Tranxit Technologies.
 */

public class DoctorListModel extends BaseModel<DoctorListResponse> {

    public DoctorListModel(IModelListener<DoctorListResponse> listener) {
        super(listener);
    }


    @Override
    public void onSuccessfulApi(DoctorListResponse response) {
        listener.onSuccessfulApi(response);
    }


    @Override
    public void onFailureApi(CustomException e) {
        listener.onFailureApi(e);
    }

    @Override
    public void onUnauthorizedUser(CustomException e) {
        listener.onUnauthorizedUser(e);
    }

    @Override
    public void onNetworkFailure() {
        listener.onNetworkFailure();
    }

    public void getDoctorsList() {
        enQueueTask(new ApiClient().getClient().create(ApiInterface.class).getDoctorsList());
    }
}
