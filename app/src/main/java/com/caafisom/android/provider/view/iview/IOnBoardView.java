package com.caafisom.android.provider.view.iview;

import com.caafisom.android.provider.presenter.ipresenter.IOnBoardPresenter;

public interface IOnBoardView extends IView<IOnBoardPresenter> {

    void initSetUp();

    void gotoLogin();
}