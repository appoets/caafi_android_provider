package com.caafisom.android.provider.view.activity;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.text.TextUtils;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.DoctorListResponse;
import com.caafisom.android.provider.model.dto.common.DoctorSpecialityResponse;
import com.caafisom.android.provider.model.dto.common.RemoveDoctor;
import com.caafisom.android.provider.presenter.DoctorEditPresenter;
import com.caafisom.android.provider.presenter.ipresenter.IDoctorEditPresenter;
import com.caafisom.android.provider.view.iview.IDoctorEditView;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;

public class DoctorEditActivity extends BaseActivity<IDoctorEditPresenter> implements IDoctorEditView {

    @BindView(R.id.etName)
    EditText etName;
    @BindView(R.id.etLastName)
    EditText etLastName;
    @BindView(R.id.etEmail)
    EditText etEmail;
    @BindView(R.id.etExperience)
    EditText etExperience;
    @BindView(R.id.etPrice)
    EditText etPrice;
    /*  @BindView(R.id.etSpecialise)
      EditText etSpecialise;*/
    @BindView(R.id.spinner_specialise)
    Spinner spinnerspecialise;
    @BindView(R.id.btnCreate)
    Button btnCreate;
    @BindView(R.id.btnRemove)
    Button btnRemove;
    @BindView(R.id.btnAvialability)
    Button btnAvialability;
    @BindView(R.id.ibBack)
    ImageButton ibBack;
    @BindView(R.id.civProfile)
    ImageView civProfile;
    private File profileImageFile;

    private String first_name, last_name, email, avatar, speciality;
    private Integer experience, doctorID, consultationfee, speciality_ID;
    ArrayList<String> specialityList = new ArrayList<>();
    ArrayList<Integer> specialityListID = new ArrayList<>();
    private DoctorSpecialityResponse doctorSpecialityResponse;

    @Override
    int attachLayout() {
        return R.layout.activity_doctor_edit;
    }

    @SuppressLint("SetTextI18n")
    @Override
    IDoctorEditPresenter initialize() {

        Intent intent = getIntent();

        if (intent != null) {
            first_name = intent.getStringExtra("first_name");
            last_name = intent.getStringExtra("last_name");
            email = intent.getStringExtra("email");
            consultationfee = intent.getIntExtra("consultation_fee", 0);
            experience = intent.getIntExtra("experience", 0);
            doctorID = intent.getIntExtra("doctor_ID", 0);
            speciality_ID = intent.getIntExtra("speciality_ID", 0);
            speciality = intent.getStringExtra("speciality");
            avatar = intent.getStringExtra("avatar");

            etName.setText(first_name);
            etLastName.setText(last_name);
            etEmail.setText(email);
            etPrice.setText("" + consultationfee);
            etExperience.setText(" " + experience);
            Glide.with(DoctorEditActivity.this)
                    .load(avatar)
                    .apply(new RequestOptions()
                            .placeholder(R.drawable.ic_dummy_user)
                            .error(R.drawable.ic_dummy_user).dontAnimate())
                    .into(civProfile);
        }

        return new DoctorEditPresenter(this);
    }

    @OnClick({R.id.btnCreate, R.id.btnRemove,R.id.btnAvialability})
    public void OnViewClick(View view) {

        switch (view.getId()) {
            case R.id.btnCreate:
                updateDoctorData();
                break;
            case R.id.btnRemove:
                removeDoctorData();
                break;
            case R.id.btnAvialability:
                gotoDoctorAvailability();
                break;
        }

    }

    private void gotoDoctorAvailability() {
        Intent intent = new Intent(DoctorEditActivity.this,DoctorAvailableActivity.class);
        intent.putExtra("doctor_id",doctorID);
        intent.putExtra("doctor_name",first_name + " " + last_name);
        intent.putExtra("doctor_image",avatar);
        startActivity(intent);
        finish();
    }

    private void removeDoctorData() {
        if (doctorID != null) {
            iPresenter.removeDoctor(doctorID);
        }
    }

    private void updateDoctorData() {
        String email = etEmail.getText().toString().trim();
        String first_name = etName.getText().toString().trim();
        String last_name = etLastName.getText().toString().trim();
        String experience = etExperience.getText().toString().trim();
        String price_min = etPrice.getText().toString().trim();

        if (TextUtils.isEmpty(email)) {
            showSnackBar(getString(R.string.please_enter_email));
        } else if (!getCodeSnippet().isEmailValid(email)) {
            showSnackBar(getString(R.string.please_enter_valid_email));
        } else if (first_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_name));
        } else if (last_name.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_last_name));
        } else if (experience.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_experience));
        } else if (price_min.isEmpty()) {
            showSnackBar(getString(R.string.please_enter_price_min));
        } else {
            HashMap<String, RequestBody> map = new HashMap<>();
            map.put("first_name", RequestBody.create(MediaType.parse("text/plain"), first_name));
            map.put("last_name", RequestBody.create(MediaType.parse("text/plain"), last_name));
            map.put("email", RequestBody.create(MediaType.parse("text/plain"), email));
            map.put("consultation_fee", RequestBody.create(MediaType.parse("text/plain"), price_min));
            map.put("experience", RequestBody.create(MediaType.parse("text/plain"), experience));

            if (spinnerspecialise.getAdapter() != null) {
                doctorSpecialityResponse.getData().get(spinnerspecialise.getSelectedItemPosition());
                map.put("speciality_id", RequestBody.create(MediaType.parse("text/plain"), String.valueOf(doctorSpecialityResponse.getData().get(spinnerspecialise.getSelectedItemPosition()).getId())));
            }

            MultipartBody.Part filePart1 = null;
            if (avatar != null) {
                filePart1 = MultipartBody.Part.createFormData("picture", avatar, RequestBody.create(MediaType.parse("image/*"), avatar));
            }

            iPresenter.postDoctor(doctorID, map, filePart1);
        }
    }


    @Override
    public void updateDoctor(DoctorListResponse data) {

    }

    @Override
    public void initSetUp() {

    }

    @Override
    public void getResponse() {
        Toast.makeText(this, "Successfully Updated", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DoctorsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void getRemoveResponse() {
        Toast.makeText(this, "Doctor deleted successfully", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, DoctorsActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void getDoctorSpeciality(DoctorSpecialityResponse response) {
//        Toast.makeText(this, "Response:" + response.getData().get(0).getName(), Toast.LENGTH_SHORT).show();

        doctorSpecialityResponse = response;
        specialityList.clear();
        specialityListID.clear();

        for (int i = 0; i < response.getData().size(); i++) {
            specialityList.add(response.getData().get(i).getName());
            specialityListID.add(response.getData().get(i).getId());
        }

        ArrayAdapter mspecialAdapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, specialityList);
        spinnerspecialise.setAdapter(mspecialAdapter);
        for (int j = 0; j < specialityListID.size(); j++) {
            if (speciality_ID.equals(specialityListID.get(j))) {
                spinnerspecialise.setSelection(j);
            }
        }

    }

}