package com.caafisom.android.provider.view.adapter;

import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.GetDoctorAvailablity;
import com.caafisom.android.provider.model.dto.common.DoctorList;
import com.caafisom.android.provider.model.dto.common.HistoryItem;
import com.caafisom.android.provider.view.adapter.listener.IDoctorAvailabilityRecyclerListener;
import com.caafisom.android.provider.view.adapter.listener.IHistoryRecyclerListener;
import com.caafisom.android.provider.view.adapter.viewholder.DoctorAvailabilityViewHolder;
import com.caafisom.android.provider.view.adapter.viewholder.HistoryViewHolder;

import java.util.List;

public class DoctorAvailabilityAdapter extends BaseRecyclerAdapter<IDoctorAvailabilityRecyclerListener, GetDoctorAvailablity, DoctorAvailabilityViewHolder> {

    IDoctorAvailabilityRecyclerListener iHistoryRecyclerListener;
    List<GetDoctorAvailablity> getDoctorAvailablities;

    public DoctorAvailabilityAdapter(List<GetDoctorAvailablity> getDoctorAvailablities, IDoctorAvailabilityRecyclerListener iHistoryRecyclerListener) {
        super(getDoctorAvailablities, iHistoryRecyclerListener);
        this.getDoctorAvailablities = getDoctorAvailablities;
        this.iHistoryRecyclerListener = iHistoryRecyclerListener;
    }

    @NonNull
    @Override
    public DoctorAvailabilityViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_doctor_availability, parent, false);
        return new DoctorAvailabilityViewHolder(view, iHistoryRecyclerListener);
    }
}
