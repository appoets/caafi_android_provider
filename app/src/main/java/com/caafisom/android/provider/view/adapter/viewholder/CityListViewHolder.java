package com.caafisom.android.provider.view.adapter.viewholder;

import android.view.View;
import android.widget.TextView;

import com.caafisom.android.provider.R;
import com.caafisom.android.provider.model.dto.common.CityList;
import com.caafisom.android.provider.view.adapter.listener.ICityListRecyclerAdapter;

import butterknife.BindView;

/**
 * Created by Tranxit Technologies.
 */

public class CityListViewHolder extends BaseViewHolder<CityList, ICityListRecyclerAdapter> {

    @BindView(R.id.tv_city)
    TextView tvcity;

    public CityListViewHolder(View itemView, ICityListRecyclerAdapter listener) {
        super(itemView, listener);
    }

    @Override
    void populateData(CityList data) {
        tvcity.setText(data.getName());

    }

    @Override
    public void onClick(View view) {
        listener.onClickItem(getAdapterPosition(), data);
    }
}
