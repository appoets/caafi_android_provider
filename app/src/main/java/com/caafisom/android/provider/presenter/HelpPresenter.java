package com.caafisom.android.provider.presenter;

import android.os.Bundle;

import com.caafisom.android.provider.model.CustomException;
import com.caafisom.android.provider.model.HelpModel;
import com.caafisom.android.provider.model.dto.response.BaseResponse;
import com.caafisom.android.provider.model.dto.response.HelpResponse;
import com.caafisom.android.provider.model.listener.IModelListener;
import com.caafisom.android.provider.presenter.ipresenter.IHelpPresenter;
import com.caafisom.android.provider.view.iview.IHelpView;

import org.jetbrains.annotations.NotNull;

import java.util.List;

public class HelpPresenter extends BasePresenter<IHelpView> implements IHelpPresenter {

    public HelpPresenter(IHelpView iView) {
        super(iView);
    }

    @Override
    public void onCreate(Bundle bundle) {
        super.onCreate(bundle);
        getHelpDetails();
    }

    @Override
    public void getHelpDetails() {
        new HelpModel(new IModelListener<HelpResponse>() {
            @Override
            public void onSuccessfulApi(@NotNull List<BaseResponse> response) {

            }

            @Override
            public void onSuccessfulApi(@NotNull HelpResponse response) {
                iView.updateHelpDetails(response);
            }

            @Override
            public void onFailureApi(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
            }

            @Override
            public void onUnauthorizedUser(CustomException e) {
                iView.dismissProgressbar();
                iView.showSnackBar(e.getMessage());
                iView.makeLogout();
            }

            @Override
            public void onNetworkFailure() {
                iView.showNetworkMessage();
            }
        }).getHelpDetails();
    }
}
